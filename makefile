all: clean
	@mkdir -p ./bin
	@cd ./src && javac -cp "../lib/antlr-4.5.2-complete.jar" \
		./CtyCompiler2016/*/*/*/*/*/*.java \
		./CtyCompiler2016/*/*/*/*/*.java \
		./CtyCompiler2016/*/*/*/*.java \
		./CtyCompiler2016/*/*/*.java \
		./CtyCompiler2016/*/*.java \
		./CtyCompiler2016/*.java \
		-d ../bin
	@cp ./lib/antlr-4.5.2-complete.jar ./bin
	@cd ./bin && jar xf ./antlr-4.5.2-complete.jar \
			 && rm -rf ./META-INF \
			 && jar cef CtyCompiler2016/Main Mning.jar ./ \
			 && rm -rf ./antlr-4.5.2-complete.jar ./CtyCompiler2016  ./org  ./st4hidden

clean:
	@rm -rf ./bin
