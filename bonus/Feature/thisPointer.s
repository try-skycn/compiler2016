.data
	.word 1
static0:
	.asciiz "\n"
	.align 2

static1:
	.space 1024
	.align 2

.text
# Build in method: ParseInt
Entry0:
	li $v0 0
	lb $fp ($a0)
	seq $v1 $fp 45
	add $a0 $a0 $v1
Label0_Cond:
	lb $fp ($a0)
	blt $fp 48 Label0_Next
	bgt $fp 57 Label0_Next
	add $fp $fp -48
	beqz $v1 Label0_NotNeg
	neg $fp $fp
Label0_NotNeg:
	mul $v0 $v0 10
	add $v0 $v0 $fp
	add $a0 $a0 1
	j Label0_Cond
Label0_Next:
	jr $ra

# Build in method: Substring
Entry1:
	add $a2 $a2 1
	add $a1 $a0 $a1
	add $a2 $a0 $a2
	sub $v1 $a2 $a1
	add $a0 $v1 5
	li $v0 9
	syscall
	sw $v1 ($v0)
	add $v0 $v0 4
	move $a0 $v0
Label1_Loop:
	beq $a1 $a2 Label1_End
	lb $v1 ($a1)
	sb $v1 ($a0)
	add $a0 $a0 1
	add $a1 $a1 1
	j Label1_Loop
Label1_End:
	sb $0 ($a0)
	jr $ra

# Build in method: GetString
Entry2:
	la $a0 static1
	li $a1 1024
	li $v0 8
	syscall
	move $v1 $a0
	li $a1 0
Label2_Cond:
	lb $v0 ($a0)
	beqz $v0 Label2_StrSpace
	add $a1 $a1 1
	add $a0 $a0 1
	j Label2_Cond
Label2_StrSpace:
	add $a0 $a1 5
	li $v0 9
	syscall
	sw $a1 ($v0)
	add $v0 $v0 4
	move $a0 $v0
	la $a1 static1
Label2_CopyCond:
	lb $v1 ($a1)
	beqz $v1 Label2_End
	sb $v1 ($a0)
	add $a0 $a0 1
	add $a1 $a1 1
	j Label2_CopyCond
Label2_End:
	sb $0 ($a0)
	jr $ra

# Build in method: Println
Entry3:
	li $v0 4
	syscall
	la $a0 static0
	syscall
	jr $ra

# Build in method: Println
Entry4:
	li $v0 1
	syscall
	la $a0 static0
	li $v0 4
	syscall
	jr $ra

# Build in method: StringConcatenate
Entry5:
	move $v1 $a0
	lw $v0 -4($a0)
	lw $a0 -4($a1)
	add $a0 $a0 $v0
	move $a2 $a0
	add $a0 $a0 5
	li $v0 9
	syscall
	sw $a2 ($v0)
	add $v0 $v0 4
	move $a0 $v0
Label5_CopyFirst:
	lb $a2 ($v1)
	beqz $a2 Label5_CopySecond
	sb $a2 ($a0)
	add $a0 $a0 1
	add $v1 $v1 1
	j Label5_CopyFirst
Label5_CopySecond:
	lb $v1 ($a1)
	beqz $v1 Label5_End
	sb $v1 ($a0)
	add $a0 $a0 1
	add $a1 $a1 1
	j Label5_CopySecond
Label5_End:
	sb $0 ($a0)
	jr $ra

# Build in method: StringEqual
Entry6:
Label6_Begin:
	lb $v0 ($a0)
	lb $v1 ($a1)
	beqz $v0 Label6_End
	beqz $v1 Label6_End
	add $a0 $a0 1
	add $a1 $a1 1
	j Label6_Begin
Label6_End:
	seq $v0 $v0 $v1
	jr $ra

# Build in method: StringGreater
Entry7:
Label7_Begin:
	lb $v0 ($a0)
	lb $v1 ($a1)
	beqz $v0 Label7_End
	beqz $v1 Label7_End
	add $a0 $a0 1
	add $a1 $a1 1
	j Label7_Begin
Label7_End:
	sgt $v0 $v0 $v1
	jr $ra

# Build in method: StringGreaterEqual
Entry8:
Label8_Begin:
	lb $v0 ($a0)
	lb $v1 ($a1)
	beqz $v0 Label8_End
	beqz $v1 Label8_End
	add $a0 $a0 1
	add $a1 $a1 1
	j Label8_Begin
Label8_End:
	sge $v0 $v0 $v1
	jr $ra

# Build in method: StringLess
Entry9:
Label9_Begin:
	lb $v0 ($a0)
	lb $v1 ($a1)
	beqz $v0 Label9_End
	beqz $v1 Label9_End
	add $a0 $a0 1
	add $a1 $a1 1
	j Label9_Begin
Label9_End:
	slt $v0 $v0 $v1
	jr $ra

# Build in method: StringLessEqual
Entry10:
Label10_Begin:
	lb $v0 ($a0)
	lb $v1 ($a1)
	beqz $v0 Label10_End
	beqz $v1 Label10_End
	add $a0 $a0 1
	add $a1 $a1 1
	j Label10_Begin
Label10_End:
	sle $v0 $v0 $v1
	jr $ra

# Build in method: StringNotEqual
Entry11:
Label11_Begin:
	lb $v0 ($a0)
	lb $v1 ($a1)
	beqz $v0 Label11_End
	beqz $v1 Label11_End
	add $a0 $a0 1
	add $a1 $a1 1
	j Label11_Begin
Label11_End:
	sne $v0 $v0 $v1
	jr $ra

# Build in method: ToString
Entry12:
Label12_ToString:
	move $a1 $a0
	li $a0 16
	li $v0 9
	syscall
	bnez $a1 Label12_NotZero
	lb $0 5($v0)
	li $a0 48
	sb $a0 4($v0)
	li $a0 1
	sb $a0 ($v0)
	j Label12_EndToString
Label12_NotZero:
	slt $v1 $a1 0
	add $fp $v0 15
	sb $0 ($fp)
Label12_Cond:
	beqz $a1 Label12_EndLoop
	rem $a0 $a1 10
	div $a1 $a1 10
	beqz $v1 Label12_LoadChar
	ble $a0 0 Label12_ToNeg
	add $a0 $a0 -10
Label12_ToNeg:
	neg $a0 $a0
Label12_LoadChar:
	add $a0 $a0 48
	add $fp $fp -1
	sb $a0 ($fp)
	j Label12_Cond
Label12_EndLoop:
	beqz $v1 Label12_Copy
	li $a0 45
	add $fp $fp -1
	sb $a0 ($fp)
Label12_Copy:
	sub $v1 $v0 $fp
	add $v1 $v1 15
	sw $v1 ($v0)
	add $v1 $v0 4
Label12_CopyCond:
	lb $a0 ($fp)
	sb $a0 ($v1)
	add $fp $fp 1
	add $v1 $v1 1
	bnez $a0 Label12_CopyCond
Label12_EndToString:
	add $v0 $v0 4
	jr $ra

main:
# Method name: #Mning:constructor
Entry13:
	add $sp $sp -8
	sw $ra 4($sp)
	sw $t9 0($sp)
# Entry#13
Label13_0:
# [Call] $t0 <- method#15
	jal Entry15
	move $t9 $v0
# [Jump]Label#13:1
# Label#13:1
Label13_1:
# [MethodEnd]
	lw $ra 4($sp)
	lw $t9 0($sp)
	add $sp $sp 8
	move $a0 $v0
	li $v0 17
	syscall

# Method name: #Mning.A:foo
Entry14:
	add $sp $sp -12
	sw $ra 8($sp)
# Entry#14
Label14_0:
# [Store]$a1 -> i32 {0}($a0)
	sw $a1 0($a0)
# [Return]$a0 [Jump]Label#14:1
	move $v0 $a0
# Label#14:1
Label14_1:
# [MethodEnd]
	lw $ra 8($sp)
	add $sp $sp 12
	jr $ra

# Method name: main
Entry15:
	add $sp $sp -12
	sw $ra 8($sp)
	sw $t9 0($sp)
	sw $t8 4($sp)
# Entry#15
Label15_0:
# [New] $t1 <- malloc {4}
	li $a0 4
	li $v0 9
	syscall
	move $t9 $v0
# [Move] $t0 <- $t1
	move $t8 $t9
# [Call] $t2 <- method#14($t0, {3})
	move $a0 $t8
	li $a1 3
	jal Entry14
	move $t9 $v0
# [Call] $t3 <- method#14($t2, {4})
	move $a0 $t9
	li $a1 4
	jal Entry14
	move $t9 $v0
# [Load] $t4 <- i32 {0}($t3)
	lw $t9 0($t9)
# [Call] $t5 <- method#4($t4)
	move $a0 $t9
	jal Entry4
	move $t9 $v0
# [Load] $t6 <- i32 {0}($t0)
	lw $t9 0($t8)
# [Call] $t7 <- method#4($t6)
	move $a0 $t9
	jal Entry4
	move $t9 $v0
# [Return]{0} [Jump]Label#15:1
	li $v0 0
# Label#15:1
Label15_1:
# [MethodEnd]
	lw $ra 8($sp)
	lw $t9 0($sp)
	lw $t8 4($sp)
	add $sp $sp 12
	jr $ra

