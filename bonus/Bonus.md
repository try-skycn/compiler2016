# Compiler 2016 - Bonus

## 开源内建方法

<https://github.com/try-skycn/Compiler2016-BuildIn>

## 实现功能

### 成员方法、this指针

见文件`./bonus/Feature/innerFunctions.mx`以及`./bonus/Feature/thisPointer.mx`

### 函数重载

见文件`./bonus/Feature/overloading.mx`

## 全局寄存器分配

编译时默认开启全局寄存器分配

```
$java -jar Mning.jar --source %s.mx --out %s.s
```

若要关闭全局寄存器分配，加参数`--naivemips`即可

```
$java -jar Mning.jar --source %s.mx --out %s.s --naivemips
```

使用全局寄存器分配之前与之后的效率的对比见文件`./bonus/RegisterAllocation/Compare.png`

