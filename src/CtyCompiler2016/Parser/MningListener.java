// Generated from /Users/SkytimChen/WorkSpace/Compiler/compiler2016/src/CtyCompiler2016/Parser/Mning.g4 by ANTLR 4.5.1
package CtyCompiler2016.Parser;

import java.util.*;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MningParser}.
 */
public interface MningListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MningParser#classBody}.
	 * @param ctx the parse tree
	 */
	void enterClassBody(MningParser.ClassBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link MningParser#classBody}.
	 * @param ctx the parse tree
	 */
	void exitClassBody(MningParser.ClassBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link MningParser#classBodyDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterClassBodyDeclaration(MningParser.ClassBodyDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MningParser#classBodyDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitClassBodyDeclaration(MningParser.ClassBodyDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MningParser#classFieldDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterClassFieldDeclaration(MningParser.ClassFieldDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MningParser#classFieldDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitClassFieldDeclaration(MningParser.ClassFieldDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MningParser#classDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterClassDeclaration(MningParser.ClassDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MningParser#classDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitClassDeclaration(MningParser.ClassDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MningParser#functionDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDeclaration(MningParser.FunctionDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MningParser#functionDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDeclaration(MningParser.FunctionDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MningParser#paras}.
	 * @param ctx the parse tree
	 */
	void enterParas(MningParser.ParasContext ctx);
	/**
	 * Exit a parse tree produced by {@link MningParser#paras}.
	 * @param ctx the parse tree
	 */
	void exitParas(MningParser.ParasContext ctx);
	/**
	 * Enter a parse tree produced by {@link MningParser#parameter}.
	 * @param ctx the parse tree
	 */
	void enterParameter(MningParser.ParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link MningParser#parameter}.
	 * @param ctx the parse tree
	 */
	void exitParameter(MningParser.ParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link MningParser#bodyStatement}.
	 * @param ctx the parse tree
	 */
	void enterBodyStatement(MningParser.BodyStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MningParser#bodyStatement}.
	 * @param ctx the parse tree
	 */
	void exitBodyStatement(MningParser.BodyStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code finalCompoundStatement}
	 * labeled alternative in {@link MningParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterFinalCompoundStatement(MningParser.FinalCompoundStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code finalCompoundStatement}
	 * labeled alternative in {@link MningParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitFinalCompoundStatement(MningParser.FinalCompoundStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code finalSelectStatement}
	 * labeled alternative in {@link MningParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterFinalSelectStatement(MningParser.FinalSelectStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code finalSelectStatement}
	 * labeled alternative in {@link MningParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitFinalSelectStatement(MningParser.FinalSelectStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code finalJumpStatement}
	 * labeled alternative in {@link MningParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterFinalJumpStatement(MningParser.FinalJumpStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code finalJumpStatement}
	 * labeled alternative in {@link MningParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitFinalJumpStatement(MningParser.FinalJumpStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code finalForStatement}
	 * labeled alternative in {@link MningParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterFinalForStatement(MningParser.FinalForStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code finalForStatement}
	 * labeled alternative in {@link MningParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitFinalForStatement(MningParser.FinalForStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code finalWhileStatement}
	 * labeled alternative in {@link MningParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterFinalWhileStatement(MningParser.FinalWhileStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code finalWhileStatement}
	 * labeled alternative in {@link MningParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitFinalWhileStatement(MningParser.FinalWhileStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code finalSuperStatement}
	 * labeled alternative in {@link MningParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterFinalSuperStatement(MningParser.FinalSuperStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code finalSuperStatement}
	 * labeled alternative in {@link MningParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitFinalSuperStatement(MningParser.FinalSuperStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MningParser#compoundStatement}.
	 * @param ctx the parse tree
	 */
	void enterCompoundStatement(MningParser.CompoundStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MningParser#compoundStatement}.
	 * @param ctx the parse tree
	 */
	void exitCompoundStatement(MningParser.CompoundStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MningParser#selectStatement}.
	 * @param ctx the parse tree
	 */
	void enterSelectStatement(MningParser.SelectStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MningParser#selectStatement}.
	 * @param ctx the parse tree
	 */
	void exitSelectStatement(MningParser.SelectStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code continueStatement}
	 * labeled alternative in {@link MningParser#jumpStatement}.
	 * @param ctx the parse tree
	 */
	void enterContinueStatement(MningParser.ContinueStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code continueStatement}
	 * labeled alternative in {@link MningParser#jumpStatement}.
	 * @param ctx the parse tree
	 */
	void exitContinueStatement(MningParser.ContinueStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code breakStatement}
	 * labeled alternative in {@link MningParser#jumpStatement}.
	 * @param ctx the parse tree
	 */
	void enterBreakStatement(MningParser.BreakStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code breakStatement}
	 * labeled alternative in {@link MningParser#jumpStatement}.
	 * @param ctx the parse tree
	 */
	void exitBreakStatement(MningParser.BreakStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code returnStatement}
	 * labeled alternative in {@link MningParser#jumpStatement}.
	 * @param ctx the parse tree
	 */
	void enterReturnStatement(MningParser.ReturnStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code returnStatement}
	 * labeled alternative in {@link MningParser#jumpStatement}.
	 * @param ctx the parse tree
	 */
	void exitReturnStatement(MningParser.ReturnStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MningParser#forStatement}.
	 * @param ctx the parse tree
	 */
	void enterForStatement(MningParser.ForStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MningParser#forStatement}.
	 * @param ctx the parse tree
	 */
	void exitForStatement(MningParser.ForStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MningParser#whileStatement}.
	 * @param ctx the parse tree
	 */
	void enterWhileStatement(MningParser.WhileStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MningParser#whileStatement}.
	 * @param ctx the parse tree
	 */
	void exitWhileStatement(MningParser.WhileStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code superExpressionList}
	 * labeled alternative in {@link MningParser#superStatement}.
	 * @param ctx the parse tree
	 */
	void enterSuperExpressionList(MningParser.SuperExpressionListContext ctx);
	/**
	 * Exit a parse tree produced by the {@code superExpressionList}
	 * labeled alternative in {@link MningParser#superStatement}.
	 * @param ctx the parse tree
	 */
	void exitSuperExpressionList(MningParser.SuperExpressionListContext ctx);
	/**
	 * Enter a parse tree produced by the {@code superInitExpression}
	 * labeled alternative in {@link MningParser#superStatement}.
	 * @param ctx the parse tree
	 */
	void enterSuperInitExpression(MningParser.SuperInitExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code superInitExpression}
	 * labeled alternative in {@link MningParser#superStatement}.
	 * @param ctx the parse tree
	 */
	void exitSuperInitExpression(MningParser.SuperInitExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code superEmpty}
	 * labeled alternative in {@link MningParser#superStatement}.
	 * @param ctx the parse tree
	 */
	void enterSuperEmpty(MningParser.SuperEmptyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code superEmpty}
	 * labeled alternative in {@link MningParser#superStatement}.
	 * @param ctx the parse tree
	 */
	void exitSuperEmpty(MningParser.SuperEmptyContext ctx);
	/**
	 * Enter a parse tree produced by {@link MningParser#initExpression}.
	 * @param ctx the parse tree
	 */
	void enterInitExpression(MningParser.InitExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MningParser#initExpression}.
	 * @param ctx the parse tree
	 */
	void exitInitExpression(MningParser.InitExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MningParser#varInit}.
	 * @param ctx the parse tree
	 */
	void enterVarInit(MningParser.VarInitContext ctx);
	/**
	 * Exit a parse tree produced by {@link MningParser#varInit}.
	 * @param ctx the parse tree
	 */
	void exitVarInit(MningParser.VarInitContext ctx);
	/**
	 * Enter a parse tree produced by {@link MningParser#typeSpecifier}.
	 * @param ctx the parse tree
	 */
	void enterTypeSpecifier(MningParser.TypeSpecifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link MningParser#typeSpecifier}.
	 * @param ctx the parse tree
	 */
	void exitTypeSpecifier(MningParser.TypeSpecifierContext ctx);
	/**
	 * Enter a parse tree produced by the {@code classTypeSpecifier}
	 * labeled alternative in {@link MningParser#plainTypeSpecifier}.
	 * @param ctx the parse tree
	 */
	void enterClassTypeSpecifier(MningParser.ClassTypeSpecifierContext ctx);
	/**
	 * Exit a parse tree produced by the {@code classTypeSpecifier}
	 * labeled alternative in {@link MningParser#plainTypeSpecifier}.
	 * @param ctx the parse tree
	 */
	void exitClassTypeSpecifier(MningParser.ClassTypeSpecifierContext ctx);
	/**
	 * Enter a parse tree produced by the {@code basicTypeSpecifier}
	 * labeled alternative in {@link MningParser#plainTypeSpecifier}.
	 * @param ctx the parse tree
	 */
	void enterBasicTypeSpecifier(MningParser.BasicTypeSpecifierContext ctx);
	/**
	 * Exit a parse tree produced by the {@code basicTypeSpecifier}
	 * labeled alternative in {@link MningParser#plainTypeSpecifier}.
	 * @param ctx the parse tree
	 */
	void exitBasicTypeSpecifier(MningParser.BasicTypeSpecifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link MningParser#classType}.
	 * @param ctx the parse tree
	 */
	void enterClassType(MningParser.ClassTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link MningParser#classType}.
	 * @param ctx the parse tree
	 */
	void exitClassType(MningParser.ClassTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code AndExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAndExpr(MningParser.AndExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AndExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAndExpr(MningParser.AndExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code thisExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterThisExpr(MningParser.ThisExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code thisExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitThisExpr(MningParser.ThisExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code equivExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterEquivExpr(MningParser.EquivExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code equivExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitEquivExpr(MningParser.EquivExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code plainNewExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPlainNewExpr(MningParser.PlainNewExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code plainNewExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPlainNewExpr(MningParser.PlainNewExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code subscriptExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterSubscriptExpr(MningParser.SubscriptExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code subscriptExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitSubscriptExpr(MningParser.SubscriptExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code multiExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMultiExpr(MningParser.MultiExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code multiExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMultiExpr(MningParser.MultiExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code bitwiseAndExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBitwiseAndExpr(MningParser.BitwiseAndExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code bitwiseAndExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBitwiseAndExpr(MningParser.BitwiseAndExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parenExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterParenExpr(MningParser.ParenExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parenExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitParenExpr(MningParser.ParenExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code shiftExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterShiftExpr(MningParser.ShiftExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code shiftExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitShiftExpr(MningParser.ShiftExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code OrExor}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterOrExor(MningParser.OrExorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code OrExor}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitOrExor(MningParser.OrExorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code prefixExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPrefixExpr(MningParser.PrefixExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code prefixExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPrefixExpr(MningParser.PrefixExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code methodCallExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMethodCallExpr(MningParser.MethodCallExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code methodCallExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMethodCallExpr(MningParser.MethodCallExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code addExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAddExpr(MningParser.AddExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code addExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAddExpr(MningParser.AddExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code postfixExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPostfixExpr(MningParser.PostfixExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code postfixExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPostfixExpr(MningParser.PostfixExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code bitwiseXorExor}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBitwiseXorExor(MningParser.BitwiseXorExorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code bitwiseXorExor}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBitwiseXorExor(MningParser.BitwiseXorExorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assignExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAssignExpr(MningParser.AssignExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assignExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAssignExpr(MningParser.AssignExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code componentExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterComponentExpr(MningParser.ComponentExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code componentExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitComponentExpr(MningParser.ComponentExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code newArrayExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNewArrayExpr(MningParser.NewArrayExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code newArrayExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNewArrayExpr(MningParser.NewArrayExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code idExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterIdExpr(MningParser.IdExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code idExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitIdExpr(MningParser.IdExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code constantExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterConstantExpr(MningParser.ConstantExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code constantExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitConstantExpr(MningParser.ConstantExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code compareExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterCompareExpr(MningParser.CompareExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code compareExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitCompareExpr(MningParser.CompareExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code biteiseOrExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBiteiseOrExpr(MningParser.BiteiseOrExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code biteiseOrExpr}
	 * labeled alternative in {@link MningParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBiteiseOrExpr(MningParser.BiteiseOrExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link MningParser#expressionList}.
	 * @param ctx the parse tree
	 */
	void enterExpressionList(MningParser.ExpressionListContext ctx);
	/**
	 * Exit a parse tree produced by {@link MningParser#expressionList}.
	 * @param ctx the parse tree
	 */
	void exitExpressionList(MningParser.ExpressionListContext ctx);
	/**
	 * Enter a parse tree produced by the {@code intType}
	 * labeled alternative in {@link MningParser#basicType}.
	 * @param ctx the parse tree
	 */
	void enterIntType(MningParser.IntTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code intType}
	 * labeled alternative in {@link MningParser#basicType}.
	 * @param ctx the parse tree
	 */
	void exitIntType(MningParser.IntTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolType}
	 * labeled alternative in {@link MningParser#basicType}.
	 * @param ctx the parse tree
	 */
	void enterBoolType(MningParser.BoolTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolType}
	 * labeled alternative in {@link MningParser#basicType}.
	 * @param ctx the parse tree
	 */
	void exitBoolType(MningParser.BoolTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringType}
	 * labeled alternative in {@link MningParser#basicType}.
	 * @param ctx the parse tree
	 */
	void enterStringType(MningParser.StringTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringType}
	 * labeled alternative in {@link MningParser#basicType}.
	 * @param ctx the parse tree
	 */
	void exitStringType(MningParser.StringTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code voidType}
	 * labeled alternative in {@link MningParser#basicType}.
	 * @param ctx the parse tree
	 */
	void enterVoidType(MningParser.VoidTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code voidType}
	 * labeled alternative in {@link MningParser#basicType}.
	 * @param ctx the parse tree
	 */
	void exitVoidType(MningParser.VoidTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code intConstant}
	 * labeled alternative in {@link MningParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterIntConstant(MningParser.IntConstantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code intConstant}
	 * labeled alternative in {@link MningParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitIntConstant(MningParser.IntConstantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringConstant}
	 * labeled alternative in {@link MningParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterStringConstant(MningParser.StringConstantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringConstant}
	 * labeled alternative in {@link MningParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitStringConstant(MningParser.StringConstantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolConstant}
	 * labeled alternative in {@link MningParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterBoolConstant(MningParser.BoolConstantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolConstant}
	 * labeled alternative in {@link MningParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitBoolConstant(MningParser.BoolConstantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nullConstant}
	 * labeled alternative in {@link MningParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterNullConstant(MningParser.NullConstantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nullConstant}
	 * labeled alternative in {@link MningParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitNullConstant(MningParser.NullConstantContext ctx);
	/**
	 * Enter a parse tree produced by {@link MningParser#boolLiteral}.
	 * @param ctx the parse tree
	 */
	void enterBoolLiteral(MningParser.BoolLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link MningParser#boolLiteral}.
	 * @param ctx the parse tree
	 */
	void exitBoolLiteral(MningParser.BoolLiteralContext ctx);
}