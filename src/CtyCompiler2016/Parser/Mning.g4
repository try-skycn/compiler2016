grammar Mning;

@header {
import java.util.*;
}

classBody
	: classBodyDeclaration*
	;

classBodyDeclaration
	: classFieldDeclaration
	| functionDeclaration
	| classDeclaration
	;

classFieldDeclaration
	: typeSpecifier[false] varInit (',' varInit)* ';'
	;

classDeclaration
	:   'class' ID '{' classBody '}'
	;

functionDeclaration
	: typeSpecifier[true] ID
		'(' paras? ')'
		compoundStatement
	;

paras
	: parameter (',' parameter)*
	;

parameter
	: typeSpecifier[false] ID
	;

// For statements

bodyStatement
	: statement
	;

statement
	: compoundStatement #finalCompoundStatement
	| selectStatement #finalSelectStatement
	| jumpStatement #finalJumpStatement
	| forStatement #finalForStatement
	| whileStatement #finalWhileStatement
	| superStatement #finalSuperStatement
	;

compoundStatement
	:	'{' (statement)* '}'
	;

selectStatement
	:	'if' '(' expression ')'
		bodyStatement
		( 'else' bodyStatement )?
	;

jumpStatement
	:	'continue' ';' #continueStatement
	|	'break' ';' #breakStatement
	|	'return' (expression)? ';' #returnStatement
	;

forStatement
	:	'for' '(' superStatement (expression)? ';' (expressionList)? ')'
		bodyStatement
	;

whileStatement
	:	'while' '(' expression ')' bodyStatement
	;

superStatement
	: expressionList ';' #superExpressionList
	| initExpression ';' #superInitExpression
	| ';' #superEmpty
	;

initExpression
	: typeSpecifier[false] varInit (',' varInit)*
	;

varInit
	: ID ('=' expression)?
	;

// For type specifier

typeSpecifier[boolean permitVoid]
	: plainTypeSpecifier[permitVoid] (arrayDimCounter+='[' ']')*
	;

plainTypeSpecifier[boolean permitVoid]
	: classType #classTypeSpecifier
	| basicType[permitVoid] #basicTypeSpecifier
	;

classType
	: ID ('.' ID)*
	;

// For expressions

expression
	: ID #idExpr
	| 'this' #thisExpr
	| constant #constantExpr
	| '(' expression ')' #parenExpr
	| expression oper=('++' | '--') #postfixExpr
	| expression '(' expressionList? ')' #methodCallExpr
	| expression '[' expression ']' #subscriptExpr
	| expression '.' ID #componentExpr
	| oper=('++' | '--' | '+' | '-' | '!' | '~') expression #prefixExpr
	| 'new' typeSpecifier[false] '[' expression ']' (dimSquares+='[' ']')* #newArrayExpr
	| 'new' classType #plainNewExpr
	| expression oper=('*' | '/' | '%') expression #multiExpr
	| expression oper=('+' | '-') expression #addExpr
	| expression oper=('<<' | '>>') expression #shiftExpr
	| expression oper=('<' | '>' | '<=' | '>=') expression #compareExpr
	| expression oper=('==' | '!=') expression #equivExpr
	| expression oper='&' expression #bitwiseAndExpr
	| expression oper='^' expression #bitwiseXorExor
	| expression oper='|' expression #biteiseOrExpr
	| expression oper='&&' expression #AndExpr
	| expression oper='||' expression #OrExor
	| <assoc=right> expression oper='=' expression #assignExpr
	;

expressionList
	: expression (',' expression)*
	;

basicType[boolean permitVoid]
	: 'int' #intType
	| 'bool' #boolType
	| 'string' #stringType
	| {$permitVoid}? 'void' #voidType
	;

constant
	: NUMBER_LITERAL #intConstant
	| STRING_LITERAL #stringConstant
	| boolLiteral #boolConstant
	| 'null' #nullConstant
	;

ID : ALPHA WORD*;
NUMBER_LITERAL : DIGIT+;
STRING_LITERAL : '"' SCharSequence? '"';
boolLiteral
	: 'true'
	| 'false'
	;

fragment
SCharSequence
	:   SChar+
	;
fragment
SChar
	:   ~["\\\r\n]
	|   EscapeSequence
	;
fragment
EscapeSequence
	:   '\\' ['"?abfnrtv\\]
	;
CHARACTER:   '\'' SChar '\'';
fragment
CChar
	:   ~['\\\r\n]
	|   EscapeSequence
	;

WS: [ \n\r\t]+ -> skip;

LINE_COMMENT: '//' ~[\r\n]* -> skip;

MULTILINE_COMMENT: '/*' .*? '*/' -> skip;

fragment
ALPHA: [a-zA-Z];

fragment
WORD: [a-zA-Z0-9_];

fragment
DIGIT: [0-9];