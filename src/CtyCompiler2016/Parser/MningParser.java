// Generated from /Users/SkytimChen/WorkSpace/Compiler/compiler2016/src/CtyCompiler2016/Parser/Mning.g4 by ANTLR 4.5.1
package CtyCompiler2016.Parser;

import java.util.*;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MningParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		T__38=39, T__39=40, T__40=41, T__41=42, T__42=43, T__43=44, T__44=45, 
		T__45=46, T__46=47, T__47=48, T__48=49, ID=50, NUMBER_LITERAL=51, STRING_LITERAL=52, 
		CHARACTER=53, WS=54, LINE_COMMENT=55, MULTILINE_COMMENT=56;
	public static final int
		RULE_classBody = 0, RULE_classBodyDeclaration = 1, RULE_classFieldDeclaration = 2, 
		RULE_classDeclaration = 3, RULE_functionDeclaration = 4, RULE_paras = 5, 
		RULE_parameter = 6, RULE_bodyStatement = 7, RULE_statement = 8, RULE_compoundStatement = 9, 
		RULE_selectStatement = 10, RULE_jumpStatement = 11, RULE_forStatement = 12, 
		RULE_whileStatement = 13, RULE_superStatement = 14, RULE_initExpression = 15, 
		RULE_varInit = 16, RULE_typeSpecifier = 17, RULE_plainTypeSpecifier = 18, 
		RULE_classType = 19, RULE_expression = 20, RULE_expressionList = 21, RULE_basicType = 22, 
		RULE_constant = 23, RULE_boolLiteral = 24;
	public static final String[] ruleNames = {
		"classBody", "classBodyDeclaration", "classFieldDeclaration", "classDeclaration", 
		"functionDeclaration", "paras", "parameter", "bodyStatement", "statement", 
		"compoundStatement", "selectStatement", "jumpStatement", "forStatement", 
		"whileStatement", "superStatement", "initExpression", "varInit", "typeSpecifier", 
		"plainTypeSpecifier", "classType", "expression", "expressionList", "basicType", 
		"constant", "boolLiteral"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "','", "';'", "'class'", "'{'", "'}'", "'('", "')'", "'if'", "'else'", 
		"'continue'", "'break'", "'return'", "'for'", "'while'", "'='", "'['", 
		"']'", "'.'", "'this'", "'++'", "'--'", "'+'", "'-'", "'!'", "'~'", "'new'", 
		"'*'", "'/'", "'%'", "'<<'", "'>>'", "'<'", "'>'", "'<='", "'>='", "'=='", 
		"'!='", "'&'", "'^'", "'|'", "'&&'", "'||'", "'int'", "'bool'", "'string'", 
		"'void'", "'null'", "'true'", "'false'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, "ID", "NUMBER_LITERAL", "STRING_LITERAL", "CHARACTER", "WS", 
		"LINE_COMMENT", "MULTILINE_COMMENT"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Mning.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public MningParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ClassBodyContext extends ParserRuleContext {
		public List<ClassBodyDeclarationContext> classBodyDeclaration() {
			return getRuleContexts(ClassBodyDeclarationContext.class);
		}
		public ClassBodyDeclarationContext classBodyDeclaration(int i) {
			return getRuleContext(ClassBodyDeclarationContext.class,i);
		}
		public ClassBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterClassBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitClassBody(this);
		}
	}

	public final ClassBodyContext classBody() throws RecognitionException {
		ClassBodyContext _localctx = new ClassBodyContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_classBody);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(53);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(50);
					classBodyDeclaration();
					}
					} 
				}
				setState(55);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassBodyDeclarationContext extends ParserRuleContext {
		public ClassFieldDeclarationContext classFieldDeclaration() {
			return getRuleContext(ClassFieldDeclarationContext.class,0);
		}
		public FunctionDeclarationContext functionDeclaration() {
			return getRuleContext(FunctionDeclarationContext.class,0);
		}
		public ClassDeclarationContext classDeclaration() {
			return getRuleContext(ClassDeclarationContext.class,0);
		}
		public ClassBodyDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classBodyDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterClassBodyDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitClassBodyDeclaration(this);
		}
	}

	public final ClassBodyDeclarationContext classBodyDeclaration() throws RecognitionException {
		ClassBodyDeclarationContext _localctx = new ClassBodyDeclarationContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_classBodyDeclaration);
		try {
			setState(59);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(56);
				classFieldDeclaration();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(57);
				functionDeclaration();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(58);
				classDeclaration();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassFieldDeclarationContext extends ParserRuleContext {
		public TypeSpecifierContext typeSpecifier() {
			return getRuleContext(TypeSpecifierContext.class,0);
		}
		public List<VarInitContext> varInit() {
			return getRuleContexts(VarInitContext.class);
		}
		public VarInitContext varInit(int i) {
			return getRuleContext(VarInitContext.class,i);
		}
		public ClassFieldDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classFieldDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterClassFieldDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitClassFieldDeclaration(this);
		}
	}

	public final ClassFieldDeclarationContext classFieldDeclaration() throws RecognitionException {
		ClassFieldDeclarationContext _localctx = new ClassFieldDeclarationContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_classFieldDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(61);
			typeSpecifier(false);
			setState(62);
			varInit();
			setState(67);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0) {
				{
				{
				setState(63);
				match(T__0);
				setState(64);
				varInit();
				}
				}
				setState(69);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(70);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassDeclarationContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(MningParser.ID, 0); }
		public ClassBodyContext classBody() {
			return getRuleContext(ClassBodyContext.class,0);
		}
		public ClassDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterClassDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitClassDeclaration(this);
		}
	}

	public final ClassDeclarationContext classDeclaration() throws RecognitionException {
		ClassDeclarationContext _localctx = new ClassDeclarationContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_classDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(72);
			match(T__2);
			setState(73);
			match(ID);
			setState(74);
			match(T__3);
			setState(75);
			classBody();
			setState(76);
			match(T__4);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionDeclarationContext extends ParserRuleContext {
		public TypeSpecifierContext typeSpecifier() {
			return getRuleContext(TypeSpecifierContext.class,0);
		}
		public TerminalNode ID() { return getToken(MningParser.ID, 0); }
		public CompoundStatementContext compoundStatement() {
			return getRuleContext(CompoundStatementContext.class,0);
		}
		public ParasContext paras() {
			return getRuleContext(ParasContext.class,0);
		}
		public FunctionDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterFunctionDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitFunctionDeclaration(this);
		}
	}

	public final FunctionDeclarationContext functionDeclaration() throws RecognitionException {
		FunctionDeclarationContext _localctx = new FunctionDeclarationContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_functionDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(78);
			typeSpecifier(true);
			setState(79);
			match(ID);
			setState(80);
			match(T__5);
			setState(82);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				{
				setState(81);
				paras();
				}
				break;
			}
			setState(84);
			match(T__6);
			setState(85);
			compoundStatement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParasContext extends ParserRuleContext {
		public List<ParameterContext> parameter() {
			return getRuleContexts(ParameterContext.class);
		}
		public ParameterContext parameter(int i) {
			return getRuleContext(ParameterContext.class,i);
		}
		public ParasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_paras; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterParas(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitParas(this);
		}
	}

	public final ParasContext paras() throws RecognitionException {
		ParasContext _localctx = new ParasContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_paras);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(87);
			parameter();
			setState(92);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0) {
				{
				{
				setState(88);
				match(T__0);
				setState(89);
				parameter();
				}
				}
				setState(94);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParameterContext extends ParserRuleContext {
		public TypeSpecifierContext typeSpecifier() {
			return getRuleContext(TypeSpecifierContext.class,0);
		}
		public TerminalNode ID() { return getToken(MningParser.ID, 0); }
		public ParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitParameter(this);
		}
	}

	public final ParameterContext parameter() throws RecognitionException {
		ParameterContext _localctx = new ParameterContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_parameter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(95);
			typeSpecifier(false);
			setState(96);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BodyStatementContext extends ParserRuleContext {
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public BodyStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bodyStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterBodyStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitBodyStatement(this);
		}
	}

	public final BodyStatementContext bodyStatement() throws RecognitionException {
		BodyStatementContext _localctx = new BodyStatementContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_bodyStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(98);
			statement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
	 
		public StatementContext() { }
		public void copyFrom(StatementContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class FinalJumpStatementContext extends StatementContext {
		public JumpStatementContext jumpStatement() {
			return getRuleContext(JumpStatementContext.class,0);
		}
		public FinalJumpStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterFinalJumpStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitFinalJumpStatement(this);
		}
	}
	public static class FinalWhileStatementContext extends StatementContext {
		public WhileStatementContext whileStatement() {
			return getRuleContext(WhileStatementContext.class,0);
		}
		public FinalWhileStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterFinalWhileStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitFinalWhileStatement(this);
		}
	}
	public static class FinalForStatementContext extends StatementContext {
		public ForStatementContext forStatement() {
			return getRuleContext(ForStatementContext.class,0);
		}
		public FinalForStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterFinalForStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitFinalForStatement(this);
		}
	}
	public static class FinalSelectStatementContext extends StatementContext {
		public SelectStatementContext selectStatement() {
			return getRuleContext(SelectStatementContext.class,0);
		}
		public FinalSelectStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterFinalSelectStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitFinalSelectStatement(this);
		}
	}
	public static class FinalSuperStatementContext extends StatementContext {
		public SuperStatementContext superStatement() {
			return getRuleContext(SuperStatementContext.class,0);
		}
		public FinalSuperStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterFinalSuperStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitFinalSuperStatement(this);
		}
	}
	public static class FinalCompoundStatementContext extends StatementContext {
		public CompoundStatementContext compoundStatement() {
			return getRuleContext(CompoundStatementContext.class,0);
		}
		public FinalCompoundStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterFinalCompoundStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitFinalCompoundStatement(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_statement);
		try {
			setState(106);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				_localctx = new FinalCompoundStatementContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(100);
				compoundStatement();
				}
				break;
			case 2:
				_localctx = new FinalSelectStatementContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(101);
				selectStatement();
				}
				break;
			case 3:
				_localctx = new FinalJumpStatementContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(102);
				jumpStatement();
				}
				break;
			case 4:
				_localctx = new FinalForStatementContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(103);
				forStatement();
				}
				break;
			case 5:
				_localctx = new FinalWhileStatementContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(104);
				whileStatement();
				}
				break;
			case 6:
				_localctx = new FinalSuperStatementContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(105);
				superStatement();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CompoundStatementContext extends ParserRuleContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public CompoundStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compoundStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterCompoundStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitCompoundStatement(this);
		}
	}

	public final CompoundStatementContext compoundStatement() throws RecognitionException {
		CompoundStatementContext _localctx = new CompoundStatementContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_compoundStatement);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(108);
			match(T__3);
			setState(112);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(109);
					statement();
					}
					} 
				}
				setState(114);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
			}
			setState(115);
			match(T__4);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectStatementContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<BodyStatementContext> bodyStatement() {
			return getRuleContexts(BodyStatementContext.class);
		}
		public BodyStatementContext bodyStatement(int i) {
			return getRuleContext(BodyStatementContext.class,i);
		}
		public SelectStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterSelectStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitSelectStatement(this);
		}
	}

	public final SelectStatementContext selectStatement() throws RecognitionException {
		SelectStatementContext _localctx = new SelectStatementContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_selectStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(117);
			match(T__7);
			setState(118);
			match(T__5);
			setState(119);
			expression(0);
			setState(120);
			match(T__6);
			setState(121);
			bodyStatement();
			setState(124);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				{
				setState(122);
				match(T__8);
				setState(123);
				bodyStatement();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class JumpStatementContext extends ParserRuleContext {
		public JumpStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_jumpStatement; }
	 
		public JumpStatementContext() { }
		public void copyFrom(JumpStatementContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BreakStatementContext extends JumpStatementContext {
		public BreakStatementContext(JumpStatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterBreakStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitBreakStatement(this);
		}
	}
	public static class ContinueStatementContext extends JumpStatementContext {
		public ContinueStatementContext(JumpStatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterContinueStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitContinueStatement(this);
		}
	}
	public static class ReturnStatementContext extends JumpStatementContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ReturnStatementContext(JumpStatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterReturnStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitReturnStatement(this);
		}
	}

	public final JumpStatementContext jumpStatement() throws RecognitionException {
		JumpStatementContext _localctx = new JumpStatementContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_jumpStatement);
		int _la;
		try {
			setState(135);
			switch (_input.LA(1)) {
			case T__9:
				_localctx = new ContinueStatementContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(126);
				match(T__9);
				setState(127);
				match(T__1);
				}
				break;
			case T__10:
				_localctx = new BreakStatementContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(128);
				match(T__10);
				setState(129);
				match(T__1);
				}
				break;
			case T__11:
				_localctx = new ReturnStatementContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(130);
				match(T__11);
				setState(132);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__5) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__46) | (1L << T__47) | (1L << T__48) | (1L << ID) | (1L << NUMBER_LITERAL) | (1L << STRING_LITERAL))) != 0)) {
					{
					setState(131);
					expression(0);
					}
				}

				setState(134);
				match(T__1);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForStatementContext extends ParserRuleContext {
		public SuperStatementContext superStatement() {
			return getRuleContext(SuperStatementContext.class,0);
		}
		public BodyStatementContext bodyStatement() {
			return getRuleContext(BodyStatementContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ExpressionListContext expressionList() {
			return getRuleContext(ExpressionListContext.class,0);
		}
		public ForStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterForStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitForStatement(this);
		}
	}

	public final ForStatementContext forStatement() throws RecognitionException {
		ForStatementContext _localctx = new ForStatementContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_forStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(137);
			match(T__12);
			setState(138);
			match(T__5);
			setState(139);
			superStatement();
			setState(141);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__5) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__46) | (1L << T__47) | (1L << T__48) | (1L << ID) | (1L << NUMBER_LITERAL) | (1L << STRING_LITERAL))) != 0)) {
				{
				setState(140);
				expression(0);
				}
			}

			setState(143);
			match(T__1);
			setState(145);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__5) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__46) | (1L << T__47) | (1L << T__48) | (1L << ID) | (1L << NUMBER_LITERAL) | (1L << STRING_LITERAL))) != 0)) {
				{
				setState(144);
				expressionList();
				}
			}

			setState(147);
			match(T__6);
			setState(148);
			bodyStatement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhileStatementContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public BodyStatementContext bodyStatement() {
			return getRuleContext(BodyStatementContext.class,0);
		}
		public WhileStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whileStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterWhileStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitWhileStatement(this);
		}
	}

	public final WhileStatementContext whileStatement() throws RecognitionException {
		WhileStatementContext _localctx = new WhileStatementContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_whileStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(150);
			match(T__13);
			setState(151);
			match(T__5);
			setState(152);
			expression(0);
			setState(153);
			match(T__6);
			setState(154);
			bodyStatement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SuperStatementContext extends ParserRuleContext {
		public SuperStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_superStatement; }
	 
		public SuperStatementContext() { }
		public void copyFrom(SuperStatementContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class SuperExpressionListContext extends SuperStatementContext {
		public ExpressionListContext expressionList() {
			return getRuleContext(ExpressionListContext.class,0);
		}
		public SuperExpressionListContext(SuperStatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterSuperExpressionList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitSuperExpressionList(this);
		}
	}
	public static class SuperEmptyContext extends SuperStatementContext {
		public SuperEmptyContext(SuperStatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterSuperEmpty(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitSuperEmpty(this);
		}
	}
	public static class SuperInitExpressionContext extends SuperStatementContext {
		public InitExpressionContext initExpression() {
			return getRuleContext(InitExpressionContext.class,0);
		}
		public SuperInitExpressionContext(SuperStatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterSuperInitExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitSuperInitExpression(this);
		}
	}

	public final SuperStatementContext superStatement() throws RecognitionException {
		SuperStatementContext _localctx = new SuperStatementContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_superStatement);
		try {
			setState(163);
			switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
			case 1:
				_localctx = new SuperExpressionListContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(156);
				expressionList();
				setState(157);
				match(T__1);
				}
				break;
			case 2:
				_localctx = new SuperInitExpressionContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(159);
				initExpression();
				setState(160);
				match(T__1);
				}
				break;
			case 3:
				_localctx = new SuperEmptyContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(162);
				match(T__1);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InitExpressionContext extends ParserRuleContext {
		public TypeSpecifierContext typeSpecifier() {
			return getRuleContext(TypeSpecifierContext.class,0);
		}
		public List<VarInitContext> varInit() {
			return getRuleContexts(VarInitContext.class);
		}
		public VarInitContext varInit(int i) {
			return getRuleContext(VarInitContext.class,i);
		}
		public InitExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_initExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterInitExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitInitExpression(this);
		}
	}

	public final InitExpressionContext initExpression() throws RecognitionException {
		InitExpressionContext _localctx = new InitExpressionContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_initExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(165);
			typeSpecifier(false);
			setState(166);
			varInit();
			setState(171);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0) {
				{
				{
				setState(167);
				match(T__0);
				setState(168);
				varInit();
				}
				}
				setState(173);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarInitContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(MningParser.ID, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public VarInitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_varInit; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterVarInit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitVarInit(this);
		}
	}

	public final VarInitContext varInit() throws RecognitionException {
		VarInitContext _localctx = new VarInitContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_varInit);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(174);
			match(ID);
			setState(177);
			_la = _input.LA(1);
			if (_la==T__14) {
				{
				setState(175);
				match(T__14);
				setState(176);
				expression(0);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeSpecifierContext extends ParserRuleContext {
		public boolean permitVoid;
		public Token s16;
		public List<Token> arrayDimCounter = new ArrayList<Token>();
		public PlainTypeSpecifierContext plainTypeSpecifier() {
			return getRuleContext(PlainTypeSpecifierContext.class,0);
		}
		public TypeSpecifierContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public TypeSpecifierContext(ParserRuleContext parent, int invokingState, boolean permitVoid) {
			super(parent, invokingState);
			this.permitVoid = permitVoid;
		}
		@Override public int getRuleIndex() { return RULE_typeSpecifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterTypeSpecifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitTypeSpecifier(this);
		}
	}

	public final TypeSpecifierContext typeSpecifier(boolean permitVoid) throws RecognitionException {
		TypeSpecifierContext _localctx = new TypeSpecifierContext(_ctx, getState(), permitVoid);
		enterRule(_localctx, 34, RULE_typeSpecifier);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(179);
			plainTypeSpecifier(permitVoid);
			setState(184);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(180);
					((TypeSpecifierContext)_localctx).s16 = match(T__15);
					((TypeSpecifierContext)_localctx).arrayDimCounter.add(((TypeSpecifierContext)_localctx).s16);
					setState(181);
					match(T__16);
					}
					} 
				}
				setState(186);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PlainTypeSpecifierContext extends ParserRuleContext {
		public boolean permitVoid;
		public PlainTypeSpecifierContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PlainTypeSpecifierContext(ParserRuleContext parent, int invokingState, boolean permitVoid) {
			super(parent, invokingState);
			this.permitVoid = permitVoid;
		}
		@Override public int getRuleIndex() { return RULE_plainTypeSpecifier; }
	 
		public PlainTypeSpecifierContext() { }
		public void copyFrom(PlainTypeSpecifierContext ctx) {
			super.copyFrom(ctx);
			this.permitVoid = ctx.permitVoid;
		}
	}
	public static class ClassTypeSpecifierContext extends PlainTypeSpecifierContext {
		public ClassTypeContext classType() {
			return getRuleContext(ClassTypeContext.class,0);
		}
		public ClassTypeSpecifierContext(PlainTypeSpecifierContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterClassTypeSpecifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitClassTypeSpecifier(this);
		}
	}
	public static class BasicTypeSpecifierContext extends PlainTypeSpecifierContext {
		public BasicTypeContext basicType() {
			return getRuleContext(BasicTypeContext.class,0);
		}
		public BasicTypeSpecifierContext(PlainTypeSpecifierContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterBasicTypeSpecifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitBasicTypeSpecifier(this);
		}
	}

	public final PlainTypeSpecifierContext plainTypeSpecifier(boolean permitVoid) throws RecognitionException {
		PlainTypeSpecifierContext _localctx = new PlainTypeSpecifierContext(_ctx, getState(), permitVoid);
		enterRule(_localctx, 36, RULE_plainTypeSpecifier);
		try {
			setState(189);
			switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
			case 1:
				_localctx = new ClassTypeSpecifierContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(187);
				classType();
				}
				break;
			case 2:
				_localctx = new BasicTypeSpecifierContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(188);
				basicType(permitVoid);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassTypeContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(MningParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(MningParser.ID, i);
		}
		public ClassTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterClassType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitClassType(this);
		}
	}

	public final ClassTypeContext classType() throws RecognitionException {
		ClassTypeContext _localctx = new ClassTypeContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_classType);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(191);
			match(ID);
			setState(196);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(192);
					match(T__17);
					setState(193);
					match(ID);
					}
					} 
				}
				setState(198);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	 
		public ExpressionContext() { }
		public void copyFrom(ExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AndExprContext extends ExpressionContext {
		public Token oper;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public AndExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterAndExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitAndExpr(this);
		}
	}
	public static class ThisExprContext extends ExpressionContext {
		public ThisExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterThisExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitThisExpr(this);
		}
	}
	public static class EquivExprContext extends ExpressionContext {
		public Token oper;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public EquivExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterEquivExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitEquivExpr(this);
		}
	}
	public static class PlainNewExprContext extends ExpressionContext {
		public ClassTypeContext classType() {
			return getRuleContext(ClassTypeContext.class,0);
		}
		public PlainNewExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterPlainNewExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitPlainNewExpr(this);
		}
	}
	public static class SubscriptExprContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public SubscriptExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterSubscriptExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitSubscriptExpr(this);
		}
	}
	public static class MultiExprContext extends ExpressionContext {
		public Token oper;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public MultiExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterMultiExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitMultiExpr(this);
		}
	}
	public static class BitwiseAndExprContext extends ExpressionContext {
		public Token oper;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public BitwiseAndExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterBitwiseAndExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitBitwiseAndExpr(this);
		}
	}
	public static class ParenExprContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ParenExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterParenExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitParenExpr(this);
		}
	}
	public static class ShiftExprContext extends ExpressionContext {
		public Token oper;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ShiftExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterShiftExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitShiftExpr(this);
		}
	}
	public static class OrExorContext extends ExpressionContext {
		public Token oper;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public OrExorContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterOrExor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitOrExor(this);
		}
	}
	public static class PrefixExprContext extends ExpressionContext {
		public Token oper;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public PrefixExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterPrefixExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitPrefixExpr(this);
		}
	}
	public static class MethodCallExprContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ExpressionListContext expressionList() {
			return getRuleContext(ExpressionListContext.class,0);
		}
		public MethodCallExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterMethodCallExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitMethodCallExpr(this);
		}
	}
	public static class AddExprContext extends ExpressionContext {
		public Token oper;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public AddExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterAddExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitAddExpr(this);
		}
	}
	public static class PostfixExprContext extends ExpressionContext {
		public Token oper;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public PostfixExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterPostfixExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitPostfixExpr(this);
		}
	}
	public static class BitwiseXorExorContext extends ExpressionContext {
		public Token oper;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public BitwiseXorExorContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterBitwiseXorExor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitBitwiseXorExor(this);
		}
	}
	public static class AssignExprContext extends ExpressionContext {
		public Token oper;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public AssignExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterAssignExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitAssignExpr(this);
		}
	}
	public static class ComponentExprContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode ID() { return getToken(MningParser.ID, 0); }
		public ComponentExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterComponentExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitComponentExpr(this);
		}
	}
	public static class NewArrayExprContext extends ExpressionContext {
		public Token s16;
		public List<Token> dimSquares = new ArrayList<Token>();
		public TypeSpecifierContext typeSpecifier() {
			return getRuleContext(TypeSpecifierContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public NewArrayExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterNewArrayExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitNewArrayExpr(this);
		}
	}
	public static class IdExprContext extends ExpressionContext {
		public TerminalNode ID() { return getToken(MningParser.ID, 0); }
		public IdExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterIdExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitIdExpr(this);
		}
	}
	public static class ConstantExprContext extends ExpressionContext {
		public ConstantContext constant() {
			return getRuleContext(ConstantContext.class,0);
		}
		public ConstantExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterConstantExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitConstantExpr(this);
		}
	}
	public static class CompareExprContext extends ExpressionContext {
		public Token oper;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public CompareExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterCompareExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitCompareExpr(this);
		}
	}
	public static class BiteiseOrExprContext extends ExpressionContext {
		public Token oper;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public BiteiseOrExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterBiteiseOrExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitBiteiseOrExpr(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 40;
		enterRecursionRule(_localctx, 40, RULE_expression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(223);
			switch ( getInterpreter().adaptivePredict(_input,19,_ctx) ) {
			case 1:
				{
				_localctx = new PrefixExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(200);
				((PrefixExprContext)_localctx).oper = _input.LT(1);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24))) != 0)) ) {
					((PrefixExprContext)_localctx).oper = (Token)_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(201);
				expression(14);
				}
				break;
			case 2:
				{
				_localctx = new IdExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(202);
				match(ID);
				}
				break;
			case 3:
				{
				_localctx = new ThisExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(203);
				match(T__18);
				}
				break;
			case 4:
				{
				_localctx = new ConstantExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(204);
				constant();
				}
				break;
			case 5:
				{
				_localctx = new ParenExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(205);
				match(T__5);
				setState(206);
				expression(0);
				setState(207);
				match(T__6);
				}
				break;
			case 6:
				{
				_localctx = new NewArrayExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(209);
				match(T__25);
				setState(210);
				typeSpecifier(false);
				setState(211);
				match(T__15);
				setState(212);
				expression(0);
				setState(213);
				match(T__16);
				setState(218);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(214);
						((NewArrayExprContext)_localctx).s16 = match(T__15);
						((NewArrayExprContext)_localctx).dimSquares.add(((NewArrayExprContext)_localctx).s16);
						setState(215);
						match(T__16);
						}
						} 
					}
					setState(220);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
				}
				}
				break;
			case 7:
				{
				_localctx = new PlainNewExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(221);
				match(T__25);
				setState(222);
				classType();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(276);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(274);
					switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
					case 1:
						{
						_localctx = new MultiExprContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(225);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(226);
						((MultiExprContext)_localctx).oper = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__26) | (1L << T__27) | (1L << T__28))) != 0)) ) {
							((MultiExprContext)_localctx).oper = (Token)_errHandler.recoverInline(this);
						} else {
							consume();
						}
						setState(227);
						expression(12);
						}
						break;
					case 2:
						{
						_localctx = new AddExprContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(228);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(229);
						((AddExprContext)_localctx).oper = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==T__21 || _la==T__22) ) {
							((AddExprContext)_localctx).oper = (Token)_errHandler.recoverInline(this);
						} else {
							consume();
						}
						setState(230);
						expression(11);
						}
						break;
					case 3:
						{
						_localctx = new ShiftExprContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(231);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(232);
						((ShiftExprContext)_localctx).oper = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==T__29 || _la==T__30) ) {
							((ShiftExprContext)_localctx).oper = (Token)_errHandler.recoverInline(this);
						} else {
							consume();
						}
						setState(233);
						expression(10);
						}
						break;
					case 4:
						{
						_localctx = new CompareExprContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(234);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(235);
						((CompareExprContext)_localctx).oper = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__34))) != 0)) ) {
							((CompareExprContext)_localctx).oper = (Token)_errHandler.recoverInline(this);
						} else {
							consume();
						}
						setState(236);
						expression(9);
						}
						break;
					case 5:
						{
						_localctx = new EquivExprContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(237);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(238);
						((EquivExprContext)_localctx).oper = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==T__35 || _la==T__36) ) {
							((EquivExprContext)_localctx).oper = (Token)_errHandler.recoverInline(this);
						} else {
							consume();
						}
						setState(239);
						expression(8);
						}
						break;
					case 6:
						{
						_localctx = new BitwiseAndExprContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(240);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(241);
						((BitwiseAndExprContext)_localctx).oper = match(T__37);
						setState(242);
						expression(7);
						}
						break;
					case 7:
						{
						_localctx = new BitwiseXorExorContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(243);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(244);
						((BitwiseXorExorContext)_localctx).oper = match(T__38);
						setState(245);
						expression(6);
						}
						break;
					case 8:
						{
						_localctx = new BiteiseOrExprContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(246);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(247);
						((BiteiseOrExprContext)_localctx).oper = match(T__39);
						setState(248);
						expression(5);
						}
						break;
					case 9:
						{
						_localctx = new AndExprContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(249);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(250);
						((AndExprContext)_localctx).oper = match(T__40);
						setState(251);
						expression(4);
						}
						break;
					case 10:
						{
						_localctx = new OrExorContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(252);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(253);
						((OrExorContext)_localctx).oper = match(T__41);
						setState(254);
						expression(3);
						}
						break;
					case 11:
						{
						_localctx = new AssignExprContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(255);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(256);
						((AssignExprContext)_localctx).oper = match(T__14);
						setState(257);
						expression(1);
						}
						break;
					case 12:
						{
						_localctx = new PostfixExprContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(258);
						if (!(precpred(_ctx, 18))) throw new FailedPredicateException(this, "precpred(_ctx, 18)");
						setState(259);
						((PostfixExprContext)_localctx).oper = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==T__19 || _la==T__20) ) {
							((PostfixExprContext)_localctx).oper = (Token)_errHandler.recoverInline(this);
						} else {
							consume();
						}
						}
						break;
					case 13:
						{
						_localctx = new MethodCallExprContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(260);
						if (!(precpred(_ctx, 17))) throw new FailedPredicateException(this, "precpred(_ctx, 17)");
						setState(261);
						match(T__5);
						setState(263);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__5) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__46) | (1L << T__47) | (1L << T__48) | (1L << ID) | (1L << NUMBER_LITERAL) | (1L << STRING_LITERAL))) != 0)) {
							{
							setState(262);
							expressionList();
							}
						}

						setState(265);
						match(T__6);
						}
						break;
					case 14:
						{
						_localctx = new SubscriptExprContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(266);
						if (!(precpred(_ctx, 16))) throw new FailedPredicateException(this, "precpred(_ctx, 16)");
						setState(267);
						match(T__15);
						setState(268);
						expression(0);
						setState(269);
						match(T__16);
						}
						break;
					case 15:
						{
						_localctx = new ComponentExprContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(271);
						if (!(precpred(_ctx, 15))) throw new FailedPredicateException(this, "precpred(_ctx, 15)");
						setState(272);
						match(T__17);
						setState(273);
						match(ID);
						}
						break;
					}
					} 
				}
				setState(278);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ExpressionListContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ExpressionListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expressionList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterExpressionList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitExpressionList(this);
		}
	}

	public final ExpressionListContext expressionList() throws RecognitionException {
		ExpressionListContext _localctx = new ExpressionListContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_expressionList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(279);
			expression(0);
			setState(284);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0) {
				{
				{
				setState(280);
				match(T__0);
				setState(281);
				expression(0);
				}
				}
				setState(286);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BasicTypeContext extends ParserRuleContext {
		public boolean permitVoid;
		public BasicTypeContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public BasicTypeContext(ParserRuleContext parent, int invokingState, boolean permitVoid) {
			super(parent, invokingState);
			this.permitVoid = permitVoid;
		}
		@Override public int getRuleIndex() { return RULE_basicType; }
	 
		public BasicTypeContext() { }
		public void copyFrom(BasicTypeContext ctx) {
			super.copyFrom(ctx);
			this.permitVoid = ctx.permitVoid;
		}
	}
	public static class IntTypeContext extends BasicTypeContext {
		public IntTypeContext(BasicTypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterIntType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitIntType(this);
		}
	}
	public static class StringTypeContext extends BasicTypeContext {
		public StringTypeContext(BasicTypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterStringType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitStringType(this);
		}
	}
	public static class VoidTypeContext extends BasicTypeContext {
		public VoidTypeContext(BasicTypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterVoidType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitVoidType(this);
		}
	}
	public static class BoolTypeContext extends BasicTypeContext {
		public BoolTypeContext(BasicTypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterBoolType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitBoolType(this);
		}
	}

	public final BasicTypeContext basicType(boolean permitVoid) throws RecognitionException {
		BasicTypeContext _localctx = new BasicTypeContext(_ctx, getState(), permitVoid);
		enterRule(_localctx, 44, RULE_basicType);
		try {
			setState(292);
			switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
			case 1:
				_localctx = new IntTypeContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(287);
				match(T__42);
				}
				break;
			case 2:
				_localctx = new BoolTypeContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(288);
				match(T__43);
				}
				break;
			case 3:
				_localctx = new StringTypeContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(289);
				match(T__44);
				}
				break;
			case 4:
				_localctx = new VoidTypeContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(290);
				if (!(_localctx.permitVoid)) throw new FailedPredicateException(this, "$permitVoid");
				setState(291);
				match(T__45);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstantContext extends ParserRuleContext {
		public ConstantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constant; }
	 
		public ConstantContext() { }
		public void copyFrom(ConstantContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class IntConstantContext extends ConstantContext {
		public TerminalNode NUMBER_LITERAL() { return getToken(MningParser.NUMBER_LITERAL, 0); }
		public IntConstantContext(ConstantContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterIntConstant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitIntConstant(this);
		}
	}
	public static class StringConstantContext extends ConstantContext {
		public TerminalNode STRING_LITERAL() { return getToken(MningParser.STRING_LITERAL, 0); }
		public StringConstantContext(ConstantContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterStringConstant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitStringConstant(this);
		}
	}
	public static class NullConstantContext extends ConstantContext {
		public NullConstantContext(ConstantContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterNullConstant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitNullConstant(this);
		}
	}
	public static class BoolConstantContext extends ConstantContext {
		public BoolLiteralContext boolLiteral() {
			return getRuleContext(BoolLiteralContext.class,0);
		}
		public BoolConstantContext(ConstantContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterBoolConstant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitBoolConstant(this);
		}
	}

	public final ConstantContext constant() throws RecognitionException {
		ConstantContext _localctx = new ConstantContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_constant);
		try {
			setState(298);
			switch (_input.LA(1)) {
			case NUMBER_LITERAL:
				_localctx = new IntConstantContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(294);
				match(NUMBER_LITERAL);
				}
				break;
			case STRING_LITERAL:
				_localctx = new StringConstantContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(295);
				match(STRING_LITERAL);
				}
				break;
			case T__47:
			case T__48:
				_localctx = new BoolConstantContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(296);
				boolLiteral();
				}
				break;
			case T__46:
				_localctx = new NullConstantContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(297);
				match(T__46);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoolLiteralContext extends ParserRuleContext {
		public BoolLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).enterBoolLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MningListener ) ((MningListener)listener).exitBoolLiteral(this);
		}
	}

	public final BoolLiteralContext boolLiteral() throws RecognitionException {
		BoolLiteralContext _localctx = new BoolLiteralContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_boolLiteral);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(300);
			_la = _input.LA(1);
			if ( !(_la==T__47 || _la==T__48) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 20:
			return expression_sempred((ExpressionContext)_localctx, predIndex);
		case 22:
			return basicType_sempred((BasicTypeContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 11);
		case 1:
			return precpred(_ctx, 10);
		case 2:
			return precpred(_ctx, 9);
		case 3:
			return precpred(_ctx, 8);
		case 4:
			return precpred(_ctx, 7);
		case 5:
			return precpred(_ctx, 6);
		case 6:
			return precpred(_ctx, 5);
		case 7:
			return precpred(_ctx, 4);
		case 8:
			return precpred(_ctx, 3);
		case 9:
			return precpred(_ctx, 2);
		case 10:
			return precpred(_ctx, 1);
		case 11:
			return precpred(_ctx, 18);
		case 12:
			return precpred(_ctx, 17);
		case 13:
			return precpred(_ctx, 16);
		case 14:
			return precpred(_ctx, 15);
		}
		return true;
	}
	private boolean basicType_sempred(BasicTypeContext _localctx, int predIndex) {
		switch (predIndex) {
		case 15:
			return _localctx.permitVoid;
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3:\u0131\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\3\2\7\2\66\n\2\f\2\16\29\13\2\3\3\3\3\3\3\5\3>\n\3\3\4\3\4"+
		"\3\4\3\4\7\4D\n\4\f\4\16\4G\13\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3"+
		"\6\3\6\3\6\5\6U\n\6\3\6\3\6\3\6\3\7\3\7\3\7\7\7]\n\7\f\7\16\7`\13\7\3"+
		"\b\3\b\3\b\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\5\nm\n\n\3\13\3\13\7\13q\n"+
		"\13\f\13\16\13t\13\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\177\n"+
		"\f\3\r\3\r\3\r\3\r\3\r\3\r\5\r\u0087\n\r\3\r\5\r\u008a\n\r\3\16\3\16\3"+
		"\16\3\16\5\16\u0090\n\16\3\16\3\16\5\16\u0094\n\16\3\16\3\16\3\16\3\17"+
		"\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\20\3\20\5\20\u00a6"+
		"\n\20\3\21\3\21\3\21\3\21\7\21\u00ac\n\21\f\21\16\21\u00af\13\21\3\22"+
		"\3\22\3\22\5\22\u00b4\n\22\3\23\3\23\3\23\7\23\u00b9\n\23\f\23\16\23\u00bc"+
		"\13\23\3\24\3\24\5\24\u00c0\n\24\3\25\3\25\3\25\7\25\u00c5\n\25\f\25\16"+
		"\25\u00c8\13\25\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\3\26\3\26\3\26\3\26\3\26\3\26\7\26\u00db\n\26\f\26\16\26\u00de\13\26"+
		"\3\26\3\26\5\26\u00e2\n\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\3\26\5\26\u010a\n\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\7\26"+
		"\u0115\n\26\f\26\16\26\u0118\13\26\3\27\3\27\3\27\7\27\u011d\n\27\f\27"+
		"\16\27\u0120\13\27\3\30\3\30\3\30\3\30\3\30\5\30\u0127\n\30\3\31\3\31"+
		"\3\31\3\31\5\31\u012d\n\31\3\32\3\32\3\32\2\3*\33\2\4\6\b\n\f\16\20\22"+
		"\24\26\30\32\34\36 \"$&(*,.\60\62\2\n\3\2\26\33\3\2\35\37\3\2\30\31\3"+
		"\2 !\3\2\"%\3\2&\'\3\2\26\27\3\2\62\63\u014e\2\67\3\2\2\2\4=\3\2\2\2\6"+
		"?\3\2\2\2\bJ\3\2\2\2\nP\3\2\2\2\fY\3\2\2\2\16a\3\2\2\2\20d\3\2\2\2\22"+
		"l\3\2\2\2\24n\3\2\2\2\26w\3\2\2\2\30\u0089\3\2\2\2\32\u008b\3\2\2\2\34"+
		"\u0098\3\2\2\2\36\u00a5\3\2\2\2 \u00a7\3\2\2\2\"\u00b0\3\2\2\2$\u00b5"+
		"\3\2\2\2&\u00bf\3\2\2\2(\u00c1\3\2\2\2*\u00e1\3\2\2\2,\u0119\3\2\2\2."+
		"\u0126\3\2\2\2\60\u012c\3\2\2\2\62\u012e\3\2\2\2\64\66\5\4\3\2\65\64\3"+
		"\2\2\2\669\3\2\2\2\67\65\3\2\2\2\678\3\2\2\28\3\3\2\2\29\67\3\2\2\2:>"+
		"\5\6\4\2;>\5\n\6\2<>\5\b\5\2=:\3\2\2\2=;\3\2\2\2=<\3\2\2\2>\5\3\2\2\2"+
		"?@\5$\23\2@E\5\"\22\2AB\7\3\2\2BD\5\"\22\2CA\3\2\2\2DG\3\2\2\2EC\3\2\2"+
		"\2EF\3\2\2\2FH\3\2\2\2GE\3\2\2\2HI\7\4\2\2I\7\3\2\2\2JK\7\5\2\2KL\7\64"+
		"\2\2LM\7\6\2\2MN\5\2\2\2NO\7\7\2\2O\t\3\2\2\2PQ\5$\23\2QR\7\64\2\2RT\7"+
		"\b\2\2SU\5\f\7\2TS\3\2\2\2TU\3\2\2\2UV\3\2\2\2VW\7\t\2\2WX\5\24\13\2X"+
		"\13\3\2\2\2Y^\5\16\b\2Z[\7\3\2\2[]\5\16\b\2\\Z\3\2\2\2]`\3\2\2\2^\\\3"+
		"\2\2\2^_\3\2\2\2_\r\3\2\2\2`^\3\2\2\2ab\5$\23\2bc\7\64\2\2c\17\3\2\2\2"+
		"de\5\22\n\2e\21\3\2\2\2fm\5\24\13\2gm\5\26\f\2hm\5\30\r\2im\5\32\16\2"+
		"jm\5\34\17\2km\5\36\20\2lf\3\2\2\2lg\3\2\2\2lh\3\2\2\2li\3\2\2\2lj\3\2"+
		"\2\2lk\3\2\2\2m\23\3\2\2\2nr\7\6\2\2oq\5\22\n\2po\3\2\2\2qt\3\2\2\2rp"+
		"\3\2\2\2rs\3\2\2\2su\3\2\2\2tr\3\2\2\2uv\7\7\2\2v\25\3\2\2\2wx\7\n\2\2"+
		"xy\7\b\2\2yz\5*\26\2z{\7\t\2\2{~\5\20\t\2|}\7\13\2\2}\177\5\20\t\2~|\3"+
		"\2\2\2~\177\3\2\2\2\177\27\3\2\2\2\u0080\u0081\7\f\2\2\u0081\u008a\7\4"+
		"\2\2\u0082\u0083\7\r\2\2\u0083\u008a\7\4\2\2\u0084\u0086\7\16\2\2\u0085"+
		"\u0087\5*\26\2\u0086\u0085\3\2\2\2\u0086\u0087\3\2\2\2\u0087\u0088\3\2"+
		"\2\2\u0088\u008a\7\4\2\2\u0089\u0080\3\2\2\2\u0089\u0082\3\2\2\2\u0089"+
		"\u0084\3\2\2\2\u008a\31\3\2\2\2\u008b\u008c\7\17\2\2\u008c\u008d\7\b\2"+
		"\2\u008d\u008f\5\36\20\2\u008e\u0090\5*\26\2\u008f\u008e\3\2\2\2\u008f"+
		"\u0090\3\2\2\2\u0090\u0091\3\2\2\2\u0091\u0093\7\4\2\2\u0092\u0094\5,"+
		"\27\2\u0093\u0092\3\2\2\2\u0093\u0094\3\2\2\2\u0094\u0095\3\2\2\2\u0095"+
		"\u0096\7\t\2\2\u0096\u0097\5\20\t\2\u0097\33\3\2\2\2\u0098\u0099\7\20"+
		"\2\2\u0099\u009a\7\b\2\2\u009a\u009b\5*\26\2\u009b\u009c\7\t\2\2\u009c"+
		"\u009d\5\20\t\2\u009d\35\3\2\2\2\u009e\u009f\5,\27\2\u009f\u00a0\7\4\2"+
		"\2\u00a0\u00a6\3\2\2\2\u00a1\u00a2\5 \21\2\u00a2\u00a3\7\4\2\2\u00a3\u00a6"+
		"\3\2\2\2\u00a4\u00a6\7\4\2\2\u00a5\u009e\3\2\2\2\u00a5\u00a1\3\2\2\2\u00a5"+
		"\u00a4\3\2\2\2\u00a6\37\3\2\2\2\u00a7\u00a8\5$\23\2\u00a8\u00ad\5\"\22"+
		"\2\u00a9\u00aa\7\3\2\2\u00aa\u00ac\5\"\22\2\u00ab\u00a9\3\2\2\2\u00ac"+
		"\u00af\3\2\2\2\u00ad\u00ab\3\2\2\2\u00ad\u00ae\3\2\2\2\u00ae!\3\2\2\2"+
		"\u00af\u00ad\3\2\2\2\u00b0\u00b3\7\64\2\2\u00b1\u00b2\7\21\2\2\u00b2\u00b4"+
		"\5*\26\2\u00b3\u00b1\3\2\2\2\u00b3\u00b4\3\2\2\2\u00b4#\3\2\2\2\u00b5"+
		"\u00ba\5&\24\2\u00b6\u00b7\7\22\2\2\u00b7\u00b9\7\23\2\2\u00b8\u00b6\3"+
		"\2\2\2\u00b9\u00bc\3\2\2\2\u00ba\u00b8\3\2\2\2\u00ba\u00bb\3\2\2\2\u00bb"+
		"%\3\2\2\2\u00bc\u00ba\3\2\2\2\u00bd\u00c0\5(\25\2\u00be\u00c0\5.\30\2"+
		"\u00bf\u00bd\3\2\2\2\u00bf\u00be\3\2\2\2\u00c0\'\3\2\2\2\u00c1\u00c6\7"+
		"\64\2\2\u00c2\u00c3\7\24\2\2\u00c3\u00c5\7\64\2\2\u00c4\u00c2\3\2\2\2"+
		"\u00c5\u00c8\3\2\2\2\u00c6\u00c4\3\2\2\2\u00c6\u00c7\3\2\2\2\u00c7)\3"+
		"\2\2\2\u00c8\u00c6\3\2\2\2\u00c9\u00ca\b\26\1\2\u00ca\u00cb\t\2\2\2\u00cb"+
		"\u00e2\5*\26\20\u00cc\u00e2\7\64\2\2\u00cd\u00e2\7\25\2\2\u00ce\u00e2"+
		"\5\60\31\2\u00cf\u00d0\7\b\2\2\u00d0\u00d1\5*\26\2\u00d1\u00d2\7\t\2\2"+
		"\u00d2\u00e2\3\2\2\2\u00d3\u00d4\7\34\2\2\u00d4\u00d5\5$\23\2\u00d5\u00d6"+
		"\7\22\2\2\u00d6\u00d7\5*\26\2\u00d7\u00dc\7\23\2\2\u00d8\u00d9\7\22\2"+
		"\2\u00d9\u00db\7\23\2\2\u00da\u00d8\3\2\2\2\u00db\u00de\3\2\2\2\u00dc"+
		"\u00da\3\2\2\2\u00dc\u00dd\3\2\2\2\u00dd\u00e2\3\2\2\2\u00de\u00dc\3\2"+
		"\2\2\u00df\u00e0\7\34\2\2\u00e0\u00e2\5(\25\2\u00e1\u00c9\3\2\2\2\u00e1"+
		"\u00cc\3\2\2\2\u00e1\u00cd\3\2\2\2\u00e1\u00ce\3\2\2\2\u00e1\u00cf\3\2"+
		"\2\2\u00e1\u00d3\3\2\2\2\u00e1\u00df\3\2\2\2\u00e2\u0116\3\2\2\2\u00e3"+
		"\u00e4\f\r\2\2\u00e4\u00e5\t\3\2\2\u00e5\u0115\5*\26\16\u00e6\u00e7\f"+
		"\f\2\2\u00e7\u00e8\t\4\2\2\u00e8\u0115\5*\26\r\u00e9\u00ea\f\13\2\2\u00ea"+
		"\u00eb\t\5\2\2\u00eb\u0115\5*\26\f\u00ec\u00ed\f\n\2\2\u00ed\u00ee\t\6"+
		"\2\2\u00ee\u0115\5*\26\13\u00ef\u00f0\f\t\2\2\u00f0\u00f1\t\7\2\2\u00f1"+
		"\u0115\5*\26\n\u00f2\u00f3\f\b\2\2\u00f3\u00f4\7(\2\2\u00f4\u0115\5*\26"+
		"\t\u00f5\u00f6\f\7\2\2\u00f6\u00f7\7)\2\2\u00f7\u0115\5*\26\b\u00f8\u00f9"+
		"\f\6\2\2\u00f9\u00fa\7*\2\2\u00fa\u0115\5*\26\7\u00fb\u00fc\f\5\2\2\u00fc"+
		"\u00fd\7+\2\2\u00fd\u0115\5*\26\6\u00fe\u00ff\f\4\2\2\u00ff\u0100\7,\2"+
		"\2\u0100\u0115\5*\26\5\u0101\u0102\f\3\2\2\u0102\u0103\7\21\2\2\u0103"+
		"\u0115\5*\26\3\u0104\u0105\f\24\2\2\u0105\u0115\t\b\2\2\u0106\u0107\f"+
		"\23\2\2\u0107\u0109\7\b\2\2\u0108\u010a\5,\27\2\u0109\u0108\3\2\2\2\u0109"+
		"\u010a\3\2\2\2\u010a\u010b\3\2\2\2\u010b\u0115\7\t\2\2\u010c\u010d\f\22"+
		"\2\2\u010d\u010e\7\22\2\2\u010e\u010f\5*\26\2\u010f\u0110\7\23\2\2\u0110"+
		"\u0115\3\2\2\2\u0111\u0112\f\21\2\2\u0112\u0113\7\24\2\2\u0113\u0115\7"+
		"\64\2\2\u0114\u00e3\3\2\2\2\u0114\u00e6\3\2\2\2\u0114\u00e9\3\2\2\2\u0114"+
		"\u00ec\3\2\2\2\u0114\u00ef\3\2\2\2\u0114\u00f2\3\2\2\2\u0114\u00f5\3\2"+
		"\2\2\u0114\u00f8\3\2\2\2\u0114\u00fb\3\2\2\2\u0114\u00fe\3\2\2\2\u0114"+
		"\u0101\3\2\2\2\u0114\u0104\3\2\2\2\u0114\u0106\3\2\2\2\u0114\u010c\3\2"+
		"\2\2\u0114\u0111\3\2\2\2\u0115\u0118\3\2\2\2\u0116\u0114\3\2\2\2\u0116"+
		"\u0117\3\2\2\2\u0117+\3\2\2\2\u0118\u0116\3\2\2\2\u0119\u011e\5*\26\2"+
		"\u011a\u011b\7\3\2\2\u011b\u011d\5*\26\2\u011c\u011a\3\2\2\2\u011d\u0120"+
		"\3\2\2\2\u011e\u011c\3\2\2\2\u011e\u011f\3\2\2\2\u011f-\3\2\2\2\u0120"+
		"\u011e\3\2\2\2\u0121\u0127\7-\2\2\u0122\u0127\7.\2\2\u0123\u0127\7/\2"+
		"\2\u0124\u0125\6\30\21\3\u0125\u0127\7\60\2\2\u0126\u0121\3\2\2\2\u0126"+
		"\u0122\3\2\2\2\u0126\u0123\3\2\2\2\u0126\u0124\3\2\2\2\u0127/\3\2\2\2"+
		"\u0128\u012d\7\65\2\2\u0129\u012d\7\66\2\2\u012a\u012d\5\62\32\2\u012b"+
		"\u012d\7\61\2\2\u012c\u0128\3\2\2\2\u012c\u0129\3\2\2\2\u012c\u012a\3"+
		"\2\2\2\u012c\u012b\3\2\2\2\u012d\61\3\2\2\2\u012e\u012f\t\t\2\2\u012f"+
		"\63\3\2\2\2\34\67=ET^lr~\u0086\u0089\u008f\u0093\u00a5\u00ad\u00b3\u00ba"+
		"\u00bf\u00c6\u00dc\u00e1\u0109\u0114\u0116\u011e\u0126\u012c";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}