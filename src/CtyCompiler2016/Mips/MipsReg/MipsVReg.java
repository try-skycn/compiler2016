package CtyCompiler2016.Mips.MipsReg;

import CtyCompiler2016.Tool.UIDManager;

import java.util.Collection;

public class MipsVReg extends MipsReg {
	public static int vRegLimit = 2;
	private static final UIDManager<MipsVReg> vRegList = resetStaticMips();

	private static UIDManager<MipsVReg> resetStaticMips() {
		UIDManager<MipsVReg> manager = new UIDManager<>();
		for (int i = 0; i < vRegLimit; i++) {
			new MipsVReg(manager);
		}
		return manager;
	}

	public static MipsVReg get(int id) {
		return vRegList.get(id);
	}

	public static void getRegs(Collection<MipsReg> regs) {
		regs.addAll(vRegList.getList());
	}

	private final int uID;

	private MipsVReg(UIDManager<MipsVReg> manager) {
		uID = manager.add(this);
	}

	@Override
	public String toString() {
		return super.toString() + "v" + uID;
	}
}
