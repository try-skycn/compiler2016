package CtyCompiler2016.Mips.MipsReg;

public class MipsRegZero extends MipsReg {
	public static MipsRegZero instance = new MipsRegZero();

	@Override
	public String toString() {
		return "$0";
	}
}
