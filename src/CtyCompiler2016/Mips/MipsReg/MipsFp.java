package CtyCompiler2016.Mips.MipsReg;

import java.util.Collection;

public class MipsFp extends MipsReg {
	public static final MipsFp instance = new MipsFp();

	public static void getRegs(Collection<MipsReg> regs) {
		regs.add(instance);
	}

	@Override
	public String toString() {
		return super.toString() + "fp";
	}
}
