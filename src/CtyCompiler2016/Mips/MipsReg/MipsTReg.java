package CtyCompiler2016.Mips.MipsReg;

import CtyCompiler2016.Tool.UIDManager;

import java.util.Collection;

public class MipsTReg extends MipsReg {
	public static int tRegLimit = 10;
	private static final UIDManager<MipsTReg> tRegList = resetStaticMips();

	private static UIDManager<MipsTReg> resetStaticMips() {
		UIDManager<MipsTReg> manager = new UIDManager<>();
		for (int i = 0; i < tRegLimit; i++) {
			new MipsTReg(manager);
		}
		return manager;
	}

	public static MipsTReg get(int id) {
		return tRegList.get(id);
	}

	public static void getRegs(Collection<MipsReg> regs) {
		regs.addAll(tRegList.getList());
	}

	private final int uID;

	private MipsTReg(UIDManager<MipsTReg> manager) {
		uID = manager.add(this);
	}

	@Override
	public String toString() {
		return super.toString() + "t" + uID;
	}
}
