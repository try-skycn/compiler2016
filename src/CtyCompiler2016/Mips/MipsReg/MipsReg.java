package CtyCompiler2016.Mips.MipsReg;

import CtyCompiler2016.Mips.MipsOperand.MipsOperand;

public abstract class MipsReg extends MipsOperand {
	@Override
	public String toString() {
		return "$";
	}
}
