package CtyCompiler2016.Mips.MipsReg;

import java.util.Collection;

public class MipsSp extends MipsReg {
	public static MipsSp instance = new MipsSp();

	public static void getRegs(Collection<MipsReg> regs) {
		regs.add(instance);
	}

	@Override
	public String toString() {
		return super.toString() + "sp";
	}
}
