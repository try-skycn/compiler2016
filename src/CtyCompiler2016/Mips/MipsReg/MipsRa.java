package CtyCompiler2016.Mips.MipsReg;

import java.util.Collection;

public class MipsRa extends MipsReg {
	public static MipsRa instance = new MipsRa();

	public static void getRegs(Collection<MipsReg> regs) {
		regs.add(instance);
	}

	@Override
	public String toString() {
		return super.toString() + "ra";
	}
}
