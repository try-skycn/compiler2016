package CtyCompiler2016.Mips.MipsReg;

import CtyCompiler2016.Tool.UIDManager;

import java.util.Collection;

public class MipsArgReg extends MipsReg {
	public static int argRegLimit = 4;
	private static final UIDManager<MipsArgReg> argRegList = resetStaticMips();

	private static UIDManager<MipsArgReg> resetStaticMips() {
		UIDManager<MipsArgReg> manager = new UIDManager<>();
		for (int i = 0; i < argRegLimit; i++) {
			new MipsArgReg(manager);
		}
		return manager;
	}

	public static MipsArgReg get(int id) {
		return argRegList.get(id);
	}

	public static void getRegs(Collection<MipsReg> regs) {
		regs.addAll(argRegList.getList());
	}

	private final int uID;

	private MipsArgReg(UIDManager<MipsArgReg> manager) {
		uID = manager.add(this);
	}

	@Override
	public String toString() {
		return super.toString() + "a" + uID;
	}
}
