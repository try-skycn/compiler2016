package CtyCompiler2016.Mips.MipsReg;

import CtyCompiler2016.Tool.UIDManager;

import java.util.Collection;

public class MipsSReg extends MipsReg {
	public static int sRegLimit = 8;
	private static final UIDManager<MipsSReg> sRegList = resetStaticMips();

	private static UIDManager<MipsSReg> resetStaticMips() {
		UIDManager<MipsSReg> manager = new UIDManager<>();
		for (int i = 0; i < sRegLimit; i++) {
			new MipsSReg(manager);
		}
		return manager;
	}

	public static MipsSReg get(int id) {
		return sRegList.get(id);
	}

	public static void getRegs(Collection<MipsReg> regs) {
		regs.addAll(sRegList.getList());
	}

	private final int uID;

	private MipsSReg(UIDManager<MipsSReg> manager) {
		uID = manager.add(this);
	}

	@Override
	public String toString() {
		return super.toString() + "s" + uID;
	}
}
