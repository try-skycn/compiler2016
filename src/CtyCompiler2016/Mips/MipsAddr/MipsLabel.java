package CtyCompiler2016.Mips.MipsAddr;

import CtyCompiler2016.Mips.MipsLine;

/**
 * Created by SkytimChen on 4/24/16.
 */
public class MipsLabel extends MipsAddr implements MipsLine {
	private final String name;

	private MipsLabel(String name) {
		this.name = name;
	}

	public static MipsLabel main() {
		return new MipsLabel("main");
	}

	public static MipsLabel entry(int methodID) {
		return new MipsLabel("Entry" + methodID);
	}

	public static MipsLabel name(int methodID, String name) {
		return new MipsLabel("Label" + methodID + "_" + name);
	}

	public static MipsLabel make(int methodID, int index) {
		return new MipsLabel("Label" + methodID + "_" + index);
	}

	public static MipsLabel staticMem(int id) {
		return new MipsLabel("static" + id);
	}

	@Override
	public String toString() {
		return name;
	}
}
