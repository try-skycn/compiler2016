package CtyCompiler2016.Mips.MipsAddr;

import CtyCompiler2016.Mips.MipsOperand.MipsImm;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

/**
 * Created by SkytimChen on 4/24/16.
 */
public class MipsRegAddr extends MipsAddr {
	private final MipsReg reg;
	private final MipsImm imm;

	public MipsRegAddr(MipsReg reg, MipsImm imm) {
		this.reg = reg;
		this.imm = imm;
	}

	public MipsRegAddr(MipsReg reg) {
		this.reg = reg;
		this.imm = null;
	}

	@Override
	public String toString() {
		if (imm == null) {
			return "(" + reg + ")";
		} else {
			return imm + "(" + reg + ")";
		}
	}
}
