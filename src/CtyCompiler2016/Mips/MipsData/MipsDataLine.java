package CtyCompiler2016.Mips.MipsData;

import CtyCompiler2016.Mips.MipsLine;

/**
 * Created by SkytimChen on 4/24/16.
 */
public class MipsDataLine implements MipsLine {
	private final MipsDataCode code;
	private final Object initVal;

	public MipsDataLine(MipsDataCode code, Object initVal) {
		this.code = code;
		this.initVal = initVal;
	}

	public static MipsDataLine align() {
		return new MipsDataLine(MipsDataCode.Align, 2);
	}

	@Override
	public String toString() {
		return code.toString() + " " + initVal;
	}
}
