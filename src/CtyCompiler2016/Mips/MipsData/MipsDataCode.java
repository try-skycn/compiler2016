package CtyCompiler2016.Mips.MipsData;

/**
 * Created by SkytimChen on 4/24/16.
 */
public enum MipsDataCode {
	Align,
	Byte,
	Word,
	String,
	Space;

	@Override
	public String toString() {
		switch (this) {
			case Align:
				return ".align";
			case Byte:
				return ".byte";
			case Word:
				return ".word";
			case String:
				return ".asciiz";
			case Space:
				return ".space";
		}
		return null;
	}
}
