package CtyCompiler2016.Mips;

/**
 * Created by SkytimChen on 4/26/16.
 */
public class MipsComment implements MipsLine {
	private final String str;

	private MipsComment(String str) {
		this.str = str;
	}

	public static MipsComment emptyLine = new MipsComment(null);

	public static MipsComment make(String str) {
		return new MipsComment(str);
	}

	@Override
	public String toString() {
		if (str == null) {
			return "";
		} else {
			return "# " + str;
		}
	}
}
