package CtyCompiler2016.Mips.MipsInst.MipsMove;

import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsInst.MipsInst;

/**
 * Created by SkytimChen on 4/24/16.
 */
public class MipsBaseMove extends MipsInst {
	protected MipsBaseMove(MipsCode code) {
		super(code);
	}
}
