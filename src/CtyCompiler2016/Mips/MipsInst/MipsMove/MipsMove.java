package CtyCompiler2016.Mips.MipsInst.MipsMove;

import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

/**
 * Created by SkytimChen on 4/24/16.
 */
public class MipsMove extends MipsBaseMove {
	private final MipsReg dest;
	private final MipsReg src;

	public MipsMove(MipsReg dest, MipsReg src) {
		super(MipsCode.move);
		this.dest = dest;
		this.src = src;
	}

	@Override
	public String toString() {
		return super.toString() + " " + dest + " " + src;
	}
}
