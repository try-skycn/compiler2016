package CtyCompiler2016.Mips.MipsInst.MipsMove.MipsStore;

import CtyCompiler2016.Mips.MipsAddr.MipsAddr;
import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

/**
 * Created by SkytimChen on 4/24/16.
 */
public class MipsStoreByte extends MipsStore {
	public MipsStoreByte(MipsReg src, MipsAddr addr) {
		super(MipsCode.sb, src, addr);
	}
}
