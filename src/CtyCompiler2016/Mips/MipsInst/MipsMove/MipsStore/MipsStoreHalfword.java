package CtyCompiler2016.Mips.MipsInst.MipsMove.MipsStore;

import CtyCompiler2016.Mips.MipsAddr.MipsAddr;
import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

/**
 * Created by SkytimChen on 4/24/16.
 */
class MipsStoreHalfword extends MipsStore {
	MipsStoreHalfword(MipsReg src, MipsAddr addr) {
		super(MipsCode.sh, src, addr);
	}
}
