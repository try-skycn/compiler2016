package CtyCompiler2016.Mips.MipsInst.MipsMove.MipsStore;

import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.Mips.MipsAddr.MipsAddr;
import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsBaseMove;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

/**
 * Created by SkytimChen on 4/24/16.
 */
public class MipsStore extends MipsBaseMove {
	private final MipsReg src;
	private final MipsAddr addr;

	MipsStore(MipsCode code, MipsReg src, MipsAddr addr) {
		super(code);
		this.src = src;
		this.addr = addr;
	}

	public static MipsStore make(MipsReg src, MipsAddr addr, int memSize) {
		switch (memSize) {
			case 1:
				return new MipsStoreByte(src, addr);
			case 2:
				return new MipsStoreHalfword(src, addr);
			case 4:
				return new MipsStoreWord(src, addr);
			default:
				CompilerError.errorHandle(ErrorType.UnknownError);
				return null;
		}
	}

	@Override
	public String toString() {
		return super.toString() + " " + src + " " + addr;
	}
}
