package CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad;

import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsBaseMove;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

/**
 * Created by SkytimChen on 4/24/16.
 */
public class MipsLoad extends MipsBaseMove {
	private final MipsReg dest;

	MipsLoad(MipsCode code, MipsReg dest) {
		super(code);
		this.dest = dest;
	}

	@Override
	public String toString() {
		return super.toString() + " " + dest;
	}
}
