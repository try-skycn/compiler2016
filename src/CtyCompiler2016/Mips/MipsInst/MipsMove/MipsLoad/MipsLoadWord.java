package CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad;

import CtyCompiler2016.Mips.MipsAddr.MipsAddr;
import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

/**
 * Created by SkytimChen on 4/24/16.
 */
public class MipsLoadWord extends MipsLoadMem {
	public MipsLoadWord(MipsReg dest, MipsAddr addr) {
		super(MipsCode.lw, dest, addr);
	}
}
