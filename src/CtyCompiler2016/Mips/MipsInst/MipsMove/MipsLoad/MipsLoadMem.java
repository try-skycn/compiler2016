package CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad;

import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.Mips.MipsAddr.MipsAddr;
import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

/**
 * Created by SkytimChen on 4/24/16.
 */
public class MipsLoadMem extends MipsLoad {
	private final MipsAddr addr;

	MipsLoadMem(MipsCode code, MipsReg dest, MipsAddr addr) {
		super(code, dest);
		this.addr = addr;
	}

	public static MipsLoadMem make(MipsReg dest, MipsAddr addr, int memSize) {
		switch (memSize) {
			case 1:
				return new MipsLoadByte(dest, addr);
			case 2:
				return new MipsLoadHalfword(dest, addr);
			case 4:
				return new MipsLoadWord(dest, addr);
			default:
				CompilerError.errorHandle(ErrorType.UnknownError);
				return null;
		}
	}

	@Override
	public String toString() {
		return super.toString() + " " + addr;
	}
}
