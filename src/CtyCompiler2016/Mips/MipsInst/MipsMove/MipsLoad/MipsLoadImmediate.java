package CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad;

import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsOperand.MipsImm;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

/**
 * Created by SkytimChen on 4/24/16.
 */
public class MipsLoadImmediate extends MipsLoad {
	private final MipsImm imm;

	public MipsLoadImmediate(MipsReg dest, MipsImm imm) {
		super(MipsCode.li, dest);
		this.imm = imm;
	}

	@Override
	public String toString() {
		return super.toString() + " " + imm;
	}
}
