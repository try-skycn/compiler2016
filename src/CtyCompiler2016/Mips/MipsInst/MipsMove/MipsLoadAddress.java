package CtyCompiler2016.Mips.MipsInst.MipsMove;

import CtyCompiler2016.Mips.MipsAddr.MipsAddr;
import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsInst.MipsInst;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

/**
 * Created by SkytimChen on 4/24/16.
 */
public class MipsLoadAddress extends MipsInst {
	private final MipsReg dest;
	private final MipsAddr addr;

	public MipsLoadAddress(MipsReg dest, MipsAddr addr) {
		super(MipsCode.la);
		this.dest = dest;
		this.addr = addr;
	}

	@Override
	public String toString() {
		return super.toString() + " " + dest + " " + addr;
	}
}
