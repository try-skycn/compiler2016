package CtyCompiler2016.Mips.MipsInst;

/**
 * Created by SkytimChen on 4/24/16.
 */
public enum MipsCode {
	// Arithmetic / Logic
	abs,
	add,
	and,
	div,
	mul,
	neg,
	nor,
	not,
	or,
	rem,
	rol,
	sll,
	sra,
	srl,
	sub,
	xor,

	// Comparison Instructions
	seq,
	sge,
	sgt,
	sle,
	slt,
	sne,

	// Branch and Jump Instructions
	b,
	beq,
	bge,
	bgt,
	ble,
	blt,
	bne,
	beqz,
	bnez,
	j,
	jal,
	jalr,
	jr,

	// Load/Store/Move Instructions
	move,
	li,
	la,
	lb,
	lh,
	lw,
	sb,
	sh,
	sw,

	// Syscall
	syscall
}
