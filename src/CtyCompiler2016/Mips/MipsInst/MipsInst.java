package CtyCompiler2016.Mips.MipsInst;

import CtyCompiler2016.Mips.MipsLine;

/**
 * Created by SkytimChen on 4/24/16.
 */
public abstract class MipsInst implements MipsLine {
	private final MipsCode code;

	protected MipsInst(MipsCode code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return code.toString();
	}
}
