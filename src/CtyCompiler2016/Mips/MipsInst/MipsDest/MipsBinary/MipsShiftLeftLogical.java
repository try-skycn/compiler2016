package CtyCompiler2016.Mips.MipsInst.MipsDest.MipsBinary;

import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsOperand.MipsOperand;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

public class MipsShiftLeftLogical extends MipsBinary {
	public MipsShiftLeftLogical(MipsReg dest, MipsReg lsrc, MipsOperand rsrc) {
		super(MipsCode.sll, dest, lsrc, rsrc);
	}
}
