package CtyCompiler2016.Mips.MipsInst.MipsDest.MipsBinary;

import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsOperand.MipsOperand;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

public class MipsSetEqual extends MipsBinary {
	public MipsSetEqual(MipsReg dest, MipsReg lsrc, MipsOperand rsrc) {
		super(MipsCode.seq, dest, lsrc, rsrc);
	}
}
