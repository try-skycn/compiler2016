package CtyCompiler2016.Mips.MipsInst.MipsDest.MipsBinary;

import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsInst.MipsDest.MipsDest;
import CtyCompiler2016.Mips.MipsOperand.MipsOperand;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

/**
 * Created by SkytimChen on 4/24/16.
 */
public class MipsBinary extends MipsDest {
	private final MipsReg lsrc;
	private final MipsOperand rsrc;

	MipsBinary(MipsCode code, MipsReg dest, MipsReg lsrc, MipsOperand rsrc) {
		super(code, dest);
		this.lsrc = lsrc;
		this.rsrc = rsrc;
	}

	@Override
	public String toString() {
		return super.toString() + " " + lsrc + " " + rsrc;
	}
}
