package CtyCompiler2016.Mips.MipsInst.MipsDest.MipsBinary;

import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsOperand.MipsOperand;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

public class MipsShiftRightLogical extends MipsBinary {
	public MipsShiftRightLogical(MipsReg dest, MipsReg lsrc, MipsOperand rsrc) {
		super(MipsCode.srl, dest, lsrc, rsrc);
	}
}
