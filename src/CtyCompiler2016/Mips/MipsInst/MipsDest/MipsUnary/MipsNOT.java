package CtyCompiler2016.Mips.MipsInst.MipsDest.MipsUnary;

import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

public class MipsNOT extends MipsUnary {
	public MipsNOT(MipsReg dest, MipsReg src) {
		super(MipsCode.not, dest, src);
	}
}
