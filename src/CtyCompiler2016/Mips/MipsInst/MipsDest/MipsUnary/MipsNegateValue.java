package CtyCompiler2016.Mips.MipsInst.MipsDest.MipsUnary;

import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

public class MipsNegateValue extends MipsUnary {
	public MipsNegateValue(MipsReg dest, MipsReg src) {
		super(MipsCode.neg, dest, src);
	}
}
