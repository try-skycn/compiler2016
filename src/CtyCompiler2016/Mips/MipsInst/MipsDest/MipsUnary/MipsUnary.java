package CtyCompiler2016.Mips.MipsInst.MipsDest.MipsUnary;

import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsInst.MipsDest.MipsDest;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

/**
 * Created by SkytimChen on 4/24/16.
 */
public class MipsUnary extends MipsDest {
	private final MipsReg src;

	MipsUnary(MipsCode code, MipsReg dest, MipsReg src) {
		super(code, dest);
		this.src = src;
	}

	@Override
	public String toString() {
		return super.toString() + " " + src;
	}
}
