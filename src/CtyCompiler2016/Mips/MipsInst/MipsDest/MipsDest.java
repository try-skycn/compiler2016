package CtyCompiler2016.Mips.MipsInst.MipsDest;

import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsInst.MipsInst;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

/**
 * Created by SkytimChen on 4/24/16.
 */
public class MipsDest extends MipsInst {
	private final MipsReg dest;

	protected MipsDest(MipsCode code, MipsReg dest) {
		super(code);
		this.dest = dest;
	}

	@Override
	public String toString() {
		return super.toString() + " " + dest;
	}
}
