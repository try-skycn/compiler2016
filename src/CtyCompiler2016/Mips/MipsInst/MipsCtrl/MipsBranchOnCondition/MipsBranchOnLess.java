package CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsBranchOnCondition;

import CtyCompiler2016.Mips.MipsAddr.MipsLabel;
import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsOperand.MipsOperand;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

public class MipsBranchOnLess extends MipsBranchOnCondition {
	public MipsBranchOnLess(MipsReg lsrc, MipsOperand rsrc, MipsLabel label) {
		super(MipsCode.blt, lsrc, rsrc, label);
	}
}

