package CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsBranchOnCondition;

import CtyCompiler2016.Mips.MipsAddr.MipsLabel;
import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

/**
 * Created by SkytimChen on 4/26/16.
 */
public class MipsBranchOnEqualZero extends MipsBranchOnSingle {
	public MipsBranchOnEqualZero(MipsReg src, MipsLabel label) {
		super(MipsCode.beqz, src, label);
	}
}
