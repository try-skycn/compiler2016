package CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsBranchOnCondition;

import CtyCompiler2016.Mips.MipsAddr.MipsLabel;
import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsCtrlLabel;
import CtyCompiler2016.Mips.MipsOperand.MipsOperand;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

/**
 * Created by SkytimChen on 4/24/16.
 */
public abstract class MipsBranchOnCondition extends MipsCtrlLabel {
	private final MipsReg lsrc;
	private final MipsOperand rsrc;

	MipsBranchOnCondition(MipsCode code, MipsReg lsrc, MipsOperand rsrc, MipsLabel label) {
		super(code, label);
		this.lsrc = lsrc;
		this.rsrc = rsrc;
	}

	@Override
	public String toString() {
		return super.toString() + " " + lsrc + " " + rsrc + " " + label;
	}
}
