package CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsBranchOnCondition;

import CtyCompiler2016.Mips.MipsAddr.MipsLabel;
import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsOperand.MipsOperand;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

public class MipsBranchOnLessEqual extends MipsBranchOnCondition {
	public MipsBranchOnLessEqual(MipsReg lsrc, MipsOperand rsrc, MipsLabel label) {
		super(MipsCode.ble, lsrc, rsrc, label);
	}
}

