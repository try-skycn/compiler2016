package CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsBranchOnCondition;

import CtyCompiler2016.Mips.MipsAddr.MipsLabel;
import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsCtrlLabel;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

/**
 * Created by SkytimChen on 4/26/16.
 */
public class MipsBranchOnSingle extends MipsCtrlLabel {
	private final MipsReg src;

	MipsBranchOnSingle(MipsCode code, MipsReg src, MipsLabel label) {
		super(code, label);
		this.src = src;
	}

	@Override
	public String toString() {
		return super.toString() + " " + src + " " + label;
	}
}
