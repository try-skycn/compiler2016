package CtyCompiler2016.Mips.MipsInst.MipsCtrl;

import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsInst.MipsInst;

/**
 * Created by SkytimChen on 4/24/16.
 */
public abstract class MipsCtrl extends MipsInst {
	MipsCtrl(MipsCode code) {
		super(code);
	}
}
