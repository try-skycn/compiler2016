package CtyCompiler2016.Mips.MipsInst.MipsCtrl;

import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsInst.MipsInst;

/**
 * Created by SkytimChen on 4/25/16.
 */
public class MipsSyscall extends MipsInst {
	public MipsSyscall() {
		super(MipsCode.syscall);
	}
}
