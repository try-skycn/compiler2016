package CtyCompiler2016.Mips.MipsInst.MipsCtrl;

import CtyCompiler2016.Mips.MipsAddr.MipsLabel;
import CtyCompiler2016.Mips.MipsInst.MipsCode;

/**
 * Created by SkytimChen on 4/24/16.
 */
public class MipsJumpAndLink extends MipsCtrlLabel {
	public MipsJumpAndLink(MipsLabel label) {
		super(MipsCode.jal, label);
	}

	@Override
	public String toString() {
		return super.toString() + " " + label;
	}
}
