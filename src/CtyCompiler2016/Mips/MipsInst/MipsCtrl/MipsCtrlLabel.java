package CtyCompiler2016.Mips.MipsInst.MipsCtrl;

import CtyCompiler2016.Mips.MipsAddr.MipsLabel;
import CtyCompiler2016.Mips.MipsInst.MipsCode;

/**
 * Created by SkytimChen on 4/24/16.
 */
public abstract class MipsCtrlLabel extends MipsCtrl {
	protected final MipsLabel label;

	protected MipsCtrlLabel(MipsCode code, MipsLabel label) {
		super(code);
		this.label = label;
	}
}
