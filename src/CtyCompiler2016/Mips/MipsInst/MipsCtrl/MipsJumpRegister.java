package CtyCompiler2016.Mips.MipsInst.MipsCtrl;

import CtyCompiler2016.Mips.MipsInst.MipsCode;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

/**
 * Created by SkytimChen on 4/24/16.
 */
public class MipsJumpRegister extends MipsCtrl {
	private final MipsReg dest;

	public MipsJumpRegister(MipsReg dest) {
		super(MipsCode.jr);
		this.dest = dest;
	}

	@Override
	public String toString() {
		return super.toString() + " " + dest;
	}
}
