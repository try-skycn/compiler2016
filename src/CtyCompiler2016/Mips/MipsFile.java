package CtyCompiler2016.Mips;

import CtyCompiler2016.IR.IRStructure;
import CtyCompiler2016.Mips.MipsAddr.MipsLabel;
import CtyCompiler2016.Printer.MipsPrinter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SkytimChen on 4/24/16.
 */
public class MipsFile {
	private final List<MipsLine> dataLineList = new ArrayList<>();
	private final List<MipsLine> instLineList = new ArrayList<>();
	private List<MipsLine> currentLine;
	private final MipsLabel entry;

	private MipsFile(MipsLabel entry) {
		this.entry = entry;
		currentLine = instLineList;
	}

	public static MipsFile makeNaiveMips(IRStructure structure) {
		MipsFile file = new MipsFile(MipsLabel.entry(structure.getEntryUID()));
		structure.printNaiveMips(file);
		return file;
	}

	public static MipsFile makeAdvancedMips(IRStructure structure) {
		MipsFile file = new MipsFile(MipsLabel.entry(structure.getEntryUID()));
		structure.printAdvancedMips(file);
		return file;
	}

	public static MipsFile make(MipsLabel entry) {
		return new MipsFile(entry);
	}

	public void beginData() {
		currentLine = dataLineList;
	}

	public void beginText() {
		currentLine = instLineList;
	}

	public void append(MipsLine instLine) {
		currentLine.add(instLine);
	}

	public void printMips(MipsPrinter printer) {
		printer.beginData();
		for (MipsLine line : dataLineList) {
			printer.printInst(line);
		}
		printer.beginText(entry);
		for (MipsLine line : instLineList) {
			printer.printInst(line);
		}
	}
}
