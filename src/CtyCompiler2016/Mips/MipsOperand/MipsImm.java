package CtyCompiler2016.Mips.MipsOperand;

/**
 * Created by SkytimChen on 4/24/16.
 */
public class MipsImm extends MipsOperand {
	private final int value;

	public MipsImm(int value) {
		this.value = value;
	}

	public MipsImm(int gap, int value) {
		this.value = gap * value;
	}

	@Override
	public String toString() {
		return Integer.toString(value);
	}
}
