package CtyCompiler2016.Printer;

import CtyCompiler2016.Mips.MipsAddr.MipsLabel;
import CtyCompiler2016.Mips.MipsComment;
import CtyCompiler2016.Mips.MipsLine;

import java.io.PrintStream;

/**
 * Created by SkytimChen on 4/24/16.
 */
public class MipsPrinter extends BasePrinter {
	public MipsPrinter(PrintStream out) {
		super(out);
	}

	public void beginData() {
		println(".data");
	}

	public void beginText(MipsLabel entry) {
		println(".text");
	}

	public void printInst(MipsLine line) {
		if (line instanceof MipsLabel) {
			println(line + ":");
		} else if (line instanceof MipsComment) {
			println(line.toString());
		} else {
			println("\t" + line.toString());
		}
	}
}
