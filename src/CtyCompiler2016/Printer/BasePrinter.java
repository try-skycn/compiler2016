package CtyCompiler2016.Printer;

import java.io.PrintStream;

/**
 * Created by SkytimChen on 4/5/16.
 */
public class BasePrinter {
	final PrintStream out;

	public BasePrinter(PrintStream out) {
		this.out = out;
	}

	public void print(String str) {
		out.print(str);
	}

	public void println(String str) {
		out.println(str);
	}

	public void println() {
		out.println();
	}
}
