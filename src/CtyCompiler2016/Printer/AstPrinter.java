package CtyCompiler2016.Printer;

import java.io.PrintStream;

/**
 * Created by SkytimChen on 4/5/16.
 */
public class AstPrinter extends BasePrinter {
	public AstPrinter(PrintStream out) {
		super(out);
	}

	private void leftpad(int tab) {
		if (tab > 0) {
			for (int i = 0; i < tab - 1; i++) {
				out.print("\t");
			}
			out.print("\t");
		}
	}
	public void println(String str, int tab) {
		leftpad(tab);
		out.println(str);
	}
}
