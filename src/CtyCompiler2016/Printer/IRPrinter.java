package CtyCompiler2016.Printer;

import CtyCompiler2016.IR.ControlFlow.Label;
import CtyCompiler2016.IR.Instruction.Instruction;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;

import java.io.PrintStream;

/**
 * Created by SkytimChen on 4/18/16.
 */
public class IRPrinter extends BasePrinter {
	public IRPrinter(PrintStream out) {
		super(out);
	}

	public void printMethod(int uID, int paraCount) {
		println("Method #" + uID + " with " + paraCount + " parameters");
	}

	public void printInstruction(Instruction instruction) {
		println("\t\t" + instruction.toString());
	}

	public void printLabel(Label label) {
		println("\t" + label.toString());
	}

	public void printInit(VirtualRegister reg, Object o) {
		println(reg + " = " + o);
	}
}
