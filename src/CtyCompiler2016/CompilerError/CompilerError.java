package CtyCompiler2016.CompilerError;

/**
 * Created by SkytimChen on 3/30/16.
 */
public class CompilerError extends Error {

	public CompilerError(String err) {
		super(err);
	}

	public static void check(boolean value, ErrorType err) {
		if (!value) {
			errorHandle(err);
		}
	}

	public static void errorHandle(ErrorType err) {
		switch (err) {
			case UnknownType:
				throw new CompilerError("Unknown type.");
			case UnknownMethod:
				throw new CompilerError("Unknown method.");
			case Collision:
				throw new CompilerError("Collision.");
			case UnknownExpression:
				throw new CompilerError("Unknown expression.");
			case UnknownStatement:
				throw new CompilerError("Unknown statement.");
			case UnknownComponent:
				throw new CompilerError("Unknown component.");
			case IncompatibleType:
				throw new CompilerError("Incompatible type.");
			case UnknownScope:
				throw new CompilerError("Unknown scope.");
			case NoMainMethod:
				throw new CompilerError("No main method.");
			default:
				throw new CompilerError("Unknown error.");
		}
	}
}
