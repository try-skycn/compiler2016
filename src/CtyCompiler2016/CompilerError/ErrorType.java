package CtyCompiler2016.CompilerError;

/**
 * Created by SkytimChen on 3/30/16.
 */
public enum ErrorType {
	UnknownType,
	UnknownMethod,
	UnknownExpression,
	UnknownStatement,
	UnknownComponent,
	UnknownScope,
	Collision,
	IncompatibleType,
	NoMainMethod,
	UnknownError
}
