package CtyCompiler2016.CompilerError;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

/**
 * Created by SkytimChen on 4/5/16.
 */
public class ParserError extends BaseErrorListener {
	@Override
	public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
		throw new CompilerError("line: " + line + ", pos: " + charPositionInLine + " msg: " + msg);
	}
}
