package CtyCompiler2016;

import CtyCompiler2016.AST.Constructor.Constructor;
import CtyCompiler2016.AST.Constructor.Environment;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ParserError;
import CtyCompiler2016.IR.IRStructure;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Parser.MningLexer;
import CtyCompiler2016.Parser.MningParser;
import CtyCompiler2016.Printer.AstPrinter;
import CtyCompiler2016.Printer.IRPrinter;
import CtyCompiler2016.Printer.MipsPrinter;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.*;

public class Main {

	private OutputStream outFile;
	private InputStream inFile;
	private PrintStream out = System.out;
	private InputStream in = System.in;

	public static void main(String[] args) {
		Main entry = null;
		try{
			entry = new Main(args);
			entry.compile();
		} catch (Exception | CompilerError e) {
			if (entry == null || entry.isPrintError()) {
				e.printStackTrace(System.out);
			}
			System.exit(1);
		}
	}

	private ParseTree parseTree;
	private Environment environment;
	private IRStructure intermediate;
	private MipsFile mipsFile;
	private enum OutputMode {
		Ast,
		Intermediate,
		NaiveMips,
		MIPS,
		Default
	}
	private OutputMode outputMode = OutputMode.MIPS;
	private boolean printError = false;

	public Main(String[] args) throws Exception {
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("--source")) {
				i++;
				inFile = new FileInputStream(args[i]);
				in = inFile;
			} else if (args[i].equals("--out")) {
				i++;
				outFile = new FileOutputStream(args[i]);
				out = new PrintStream(outFile);
			} else if (args[i].equals("--ast")) {
				outputMode = OutputMode.Ast;
			} else if (args[i].equals("--ir")) {
				outputMode = OutputMode.Intermediate;
			} else if (args[i].equals("--naivemips")) {
				outputMode = OutputMode.NaiveMips;
			} else if (args[i].equals("--mips")) {
				outputMode = OutputMode.MIPS;
			} else if (args[i].equals("--err")) {
				printError = true;
			}
		}
	}

	public void compile() throws Exception {
		ANTLRInputStream antlrIn = new ANTLRInputStream(in);
		MningLexer lexer = new MningLexer(antlrIn);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		MningParser parser = new MningParser(tokens);

		parser.removeErrorListeners();
		parser.addErrorListener(new ParserError());

		parseTree = parser.classBody();
		process();
		destroy();
	}

	private void process() throws Exception {
		switch (outputMode) {
			case Ast:
				environment = Constructor.construct(parseTree);
				environment.printAST(new AstPrinter(out));
				break;
			case Intermediate:
				environment = Constructor.construct(parseTree);
				intermediate = IRStructure.make(environment.getGlobalClass());
				intermediate.printIR(new IRPrinter(out));
				break;
			case NaiveMips:
				environment = Constructor.construct(parseTree);
				intermediate = IRStructure.make(environment.getGlobalClass());
				mipsFile = MipsFile.makeNaiveMips(intermediate);
				mipsFile.printMips(new MipsPrinter(out));
				break;
			case MIPS:
				environment = Constructor.construct(parseTree);
				intermediate = IRStructure.make(environment.getGlobalClass());
				mipsFile = MipsFile.makeAdvancedMips(intermediate);
				mipsFile.printMips(new MipsPrinter(out));
				break;
			default:
		}
	}

	private void destroy() throws Exception {
		if (inFile != null) {
			in = System.in;
			inFile.close();
		}
		if (outFile != null) {
			out = System.out;
			outFile.close();
		}
	}

	private boolean isPrintError() {
		return printError;
	}
}
