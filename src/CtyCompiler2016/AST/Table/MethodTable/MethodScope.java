package CtyCompiler2016.AST.Table.MethodTable;

import CtyCompiler2016.AST.Constituent.Method.Method;
import CtyCompiler2016.AST.Type.Type;

import java.util.List;

/**
 * Created by SkytimChen on 3/31/16.
 */
public interface MethodScope {
	void putMethod(String key, Method value);
	Method fetchMethod(String key, List<Type> typeList);
}
