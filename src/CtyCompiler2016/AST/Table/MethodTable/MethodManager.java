package CtyCompiler2016.AST.Table.MethodTable;

import CtyCompiler2016.AST.ASTInterface;
import CtyCompiler2016.AST.Constituent.Method.Method;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.Printer.AstPrinter;

import java.util.*;

/**
 * Created by SkytimChen on 3/31/16.
 */
public class MethodManager implements ASTInterface, MethodScope {
	private final List<Method> methodList = new ArrayList<>();
	private final Map<String, Collection<Integer>> nameTable = new HashMap<>();

	@Override
	public void putMethod(String key, Method value) {
		CompilerError.check(fetchMethod(key, value.getParaTypeList()) == null, ErrorType.Collision);
		if (!nameTable.containsKey(key)) {
			nameTable.put(key, new ArrayList<>());
		}
		int index = methodList.size();
		Collection<Integer> overloadList = nameTable.get(key);
		overloadList.add(index);
		methodList.add(value);
		// For MethodNames
		value.setName(key);
	}

	@Override
	public Method fetchMethod(String key, List<Type> typeList) {
		Collection<Integer> overloadList = nameTable.get(key);
		if (overloadList != null) {
			for (Integer index : overloadList) {
				if (methodList.get(index).matchParaList(typeList)) {
					return methodList.get(index);
				}
			}
		}
		return null;
	}

	// For ASTInterface
	
	@Override
	public void printAST(AstPrinter printer, int tab) {
		for (Map.Entry<String, Collection<Integer>> entry : nameTable.entrySet()) {
			printer.println("[Method]" + entry.getKey(), tab);
			for (Integer index : entry.getValue()) {
				methodList.get(index).printAST(printer, tab + 1);
				printer.println();
			}
		}
	}
}
