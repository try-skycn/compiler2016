package CtyCompiler2016.AST.Table.FieldTable;

import CtyCompiler2016.AST.Type.BasicType.IntType;

/**
 * Created by SkytimChen on 4/17/16.
 */
public class ClassFieldList extends FieldList {
	public int getOffset(int index) {
		return index * IntType.getInstance().sizeof();
	}

	public int getTotSize() {
		return getSize() * IntType.getInstance().sizeof();
	}
}
