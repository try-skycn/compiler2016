package CtyCompiler2016.AST.Table.FieldTable;

import CtyCompiler2016.AST.Table.FieldTable.FieldValue.FieldValue;

import java.util.Map;
import java.util.Set;

/**
 * Created by SkytimChen on 3/31/16.
 */
public interface FieldScope {
	void putField(String key, FieldValue value);
	FieldValue fetchField(String key);
	Set<Map.Entry<String, FieldValue>> fieldEntrySet();
}
