package CtyCompiler2016.AST.Table.FieldTable;

import CtyCompiler2016.AST.Table.FieldTable.FieldValue.FieldValue;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by SkytimChen on 3/31/16.
 */
public class FieldManager implements FieldScope {
	Map<String, FieldValue> fieldMap = new HashMap<>();

	@Override
	public void putField(String key, FieldValue value) {
		CompilerError.check(fetchField(key) == null, ErrorType.Collision);
		fieldMap.put(key, value);
	}

	@Override
	public FieldValue fetchField(String key) {
		return fieldMap.get(key);
	}

	@Override
	public Set<Map.Entry<String, FieldValue>> fieldEntrySet() {
		return fieldMap.entrySet();
	}
}
