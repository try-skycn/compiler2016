package CtyCompiler2016.AST.Table.FieldTable;

import CtyCompiler2016.AST.Type.Type;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SkytimChen on 3/31/16.
 */
public class FieldList {
	private final List<Type> typeList = new ArrayList<>();

	public int add(Type type) {
		int index = typeList.size();
		typeList.add(type);
		return index;
	}

	public Type getType(int index) {
		return typeList.get(index);
	}

	public int getSize() {
		return typeList.size();
	}

	// For ASTInterface

	public static String deepToString(FieldList fieldList) {
		StringBuilder builder = new StringBuilder();
		fieldList.print(builder);
		return builder.toString();
	}

	public void print(StringBuilder builder) {
		int cnt = 0;
		for (Type type : typeList) {
			if (cnt > 0) {
				builder.append(", ");
			}
			builder.append("#");
			builder.append(cnt);
			builder.append(":");
			builder.append(type.toString());
			cnt++;
		}
	}
}
