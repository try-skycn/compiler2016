package CtyCompiler2016.AST.Table.FieldTable;

import CtyCompiler2016.AST.Table.FieldTable.FieldValue.FieldValue;
import CtyCompiler2016.AST.Type.Type;

/**
 * Created by SkytimChen on 3/31/16.
 */
public interface FieldMaker {
	FieldValue makeField(Type type);
	Type getFieldType(int index);
}
