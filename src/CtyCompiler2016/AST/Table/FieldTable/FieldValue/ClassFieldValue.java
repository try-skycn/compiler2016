package CtyCompiler2016.AST.Table.FieldTable.FieldValue;

import CtyCompiler2016.AST.Type.ClassType;
import CtyCompiler2016.AST.Type.Type;

/**
 * Created by SkytimChen on 3/31/16.
 */
public class ClassFieldValue implements FieldValue {
	public final ClassType classType;
	public final int index;

	private ClassFieldValue(ClassType classType, int index) {
		this.classType = classType;
		this.index = index;
	}

	public static ClassFieldValue make(ClassType classType, int index) {
		return new ClassFieldValue(classType, index);
	}

	@Override
	public Type getType() {
		return classType.getFieldType(index);
	}

	public int getOffset() {
		return classType.getOffset(index);
	}
}
