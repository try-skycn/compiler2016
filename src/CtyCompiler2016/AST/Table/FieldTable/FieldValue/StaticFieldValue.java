package CtyCompiler2016.AST.Table.FieldTable.FieldValue;

import CtyCompiler2016.AST.Constituent.Expression.Field.StaticFieldExpression;

/**
 * Created by SkytimChen on 3/31/16.
 */
public interface StaticFieldValue extends FieldValue {
	static StaticFieldValue make(int index) {
		return StaticFieldExpression.make(index);
	}
}
