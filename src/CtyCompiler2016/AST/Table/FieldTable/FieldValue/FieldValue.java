package CtyCompiler2016.AST.Table.FieldTable.FieldValue;

import CtyCompiler2016.AST.Type.Type;

/**
 * Created by SkytimChen on 3/31/16.
 */
public interface FieldValue {
	Type getType();
}
