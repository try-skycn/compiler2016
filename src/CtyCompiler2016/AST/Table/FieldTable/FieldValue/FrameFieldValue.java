package CtyCompiler2016.AST.Table.FieldTable.FieldValue;

import CtyCompiler2016.AST.Constituent.Expression.Field.FrameFieldExpression;
import CtyCompiler2016.AST.Constituent.Frame.FrameFieldMaker;

/**
 * Created by SkytimChen on 3/31/16.
 */
public interface FrameFieldValue extends FieldValue {
	static FrameFieldValue make(FrameFieldMaker pool, int index) {
		return FrameFieldExpression.make(pool, index);
	}
}
