package CtyCompiler2016.AST.Table.ClassTable;

import CtyCompiler2016.AST.Type.ClassType;

/**
 * Created by SkytimChen on 3/31/16.
 */
public interface ClassScope {
	void putClass(String key, ClassType value);
	ClassType fetchClass(String key);
}
