package CtyCompiler2016.AST.Constituent.Statement.Jump;

import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Constituent.Method.Method;
import CtyCompiler2016.AST.Type.BasicType.VoidType;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Register.ExprOperand;
import CtyCompiler2016.Printer.AstPrinter;

/**
 * Created by SkytimChen on 3/31/16.
 */
public class ReturnStatement extends JumpStatement {
	private final Method enclosingMethod;
	private final Expression returnValue;

	private ReturnStatement(Method enclosingMethod, Expression returnValue) {
		this.enclosingMethod = enclosingMethod;
		this.returnValue = returnValue;
	}

	public static ReturnStatement make(Method enclosingMethod, Expression returnValue) {
		CompilerError.check(enclosingMethod.getReturnType() == returnValue.getType(), ErrorType.IncompatibleType);
		return new ReturnStatement(enclosingMethod, returnValue);
	}

	public static ReturnStatement make(Method enclosingMethod) {
		CompilerError.check(enclosingMethod.getReturnType() instanceof VoidType, ErrorType.IncompatibleType);
		return new ReturnStatement(enclosingMethod, null);
	}

	// For ASTInterface

	@Override
	public void printAST(AstPrinter printer, int tab) {
		printer.println("[return]method#" + enclosingMethod.uID, tab);
		if (returnValue != null) {
			printer.println("[value](" + Expression.deepToString(returnValue) + ")", tab + 1);
		}
	}

	// For IR

	@Override
	public void makeIR(CFGMaker maker) {
		if (returnValue == null) {
			maker.putVoidReturn();
		} else {
			ExprOperand returnOperand = returnValue.makeIR(maker);
			maker.putReturn(returnOperand);
		}
	}
}
