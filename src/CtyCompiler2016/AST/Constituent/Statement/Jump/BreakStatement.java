package CtyCompiler2016.AST.Constituent.Statement.Jump;

import CtyCompiler2016.AST.Constituent.Statement.Iterator.IterationStatement;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.Printer.AstPrinter;

/**
 * Created by SkytimChen on 3/31/16.
 */
public class BreakStatement extends JumpStatement {
	private final IterationStatement enclosingIteration;

	private BreakStatement(IterationStatement enclosingIteration) {
		this.enclosingIteration = enclosingIteration;
	}

	public static BreakStatement make(IterationStatement enclosingIteration) {
		return new BreakStatement(enclosingIteration);
	}

	// For ASTInterface

	@Override
	public void printAST(AstPrinter printer, int tab) {
		printer.println("[break]loop#" + enclosingIteration.objectIndex, tab);
	}

	// For IR

	@Override
	public void makeIR(CFGMaker maker) {
		maker.putBreak();
	}
}
