package CtyCompiler2016.AST.Constituent.Statement;

import CtyCompiler2016.AST.Constituent.Frame.FrameFieldMaker;
import CtyCompiler2016.AST.Constituent.Statement.Jump.JumpStatement;
import CtyCompiler2016.AST.Table.FieldTable.FieldList;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.Printer.AstPrinter;

import java.util.List;

/**
 * Created by SkytimChen on 3/30/16.
 */
public class CompoundStatement extends Statement {
	private final FrameFieldMaker pool;
	private List<Statement> stmtList;

	private CompoundStatement(FrameFieldMaker pool) {this.pool = pool;}

	public static CompoundStatement define(FrameFieldMaker pool) {
		return new CompoundStatement(pool);
	}

	public void make(List<Statement> stmtList) {
		this.stmtList = stmtList;
	}

	// For ASTInterface

	@Override
	public void printAST(AstPrinter printer, int tab) {
		printer.println("[compound]", tab);
		printer.println("{", tab);
		if (pool != null) {
			printer.println("[load](pool#" + pool.getFrameIndex() + ")" + FieldList.deepToString(pool.getPool()), tab + 1);
		}
		for (Statement statement : stmtList) {
			statement.printAST(printer, tab + 1);
		}
		printer.println("}", tab);
	}

	// For IR

	@Override
	public void makeIR(CFGMaker maker) {
		for (Statement statement : stmtList) {
			statement.makeIR(maker);
			if (statement instanceof JumpStatement) {
				break;
			}
		}
	}
}
