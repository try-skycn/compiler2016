package CtyCompiler2016.AST.Constituent.Statement.Iterator;

import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Constituent.Statement.Statement;
import CtyCompiler2016.AST.Type.BasicType.BoolType;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.ControlFlow.Label;
import CtyCompiler2016.IR.Register.ExprOperand;
import CtyCompiler2016.Printer.AstPrinter;

/**
 * Created by SkytimChen on 3/31/16.
 */
public class IterationStatement extends Statement {
	Expression condition;
	Statement loopBody;

	IterationStatement() {}

	public static IterationStatement define() {
		return new IterationStatement();
	}

	public void make(Expression condition, Statement loopBody) {
		CompilerError.check(condition.getType() instanceof BoolType, ErrorType.IncompatibleType);
		this.condition = condition;
		this.loopBody = loopBody;
	}

	// For ASTInterface

	private static int iterationIndex = 0;
	public final int objectIndex = iterationIndex++;

	@Override
	public void printAST(AstPrinter printer, int tab) {
		printer.println("[while]loop#" + objectIndex, tab);
		printer.println("[cond](" + Expression.deepToString(condition) + ")", tab + 1);
		printer.println("[body]", tab + 1);
		loopBody.printAST(printer, tab + 2);
	}

	// For IR

	@Override
	public void makeIR(CFGMaker maker) {
		Label conditionLabel = maker.makeLabel();
		Label bodyLabel = maker.makeLabel();
		Label nextLabel = maker.makeLabel();
		maker.setLoop(conditionLabel, nextLabel);

		maker.putLabel(conditionLabel);
		condition.makeConditionIR(maker, bodyLabel, nextLabel);

		maker.putLabel(bodyLabel);
		loopBody.makeIR(maker);
		maker.putJump(conditionLabel);

		maker.endLoop();
		maker.putLabel(nextLabel);
	}
}
