package CtyCompiler2016.AST.Constituent.Statement.Iterator;

import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Constituent.Statement.Statement;
import CtyCompiler2016.AST.Constituent.Statement.SuperStatement;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.ControlFlow.Label;
import CtyCompiler2016.Printer.AstPrinter;

/**
 * Created by SkytimChen on 3/30/16.
 */
public class ForStatement extends IterationStatement {
	SuperStatement preStmt;
	SuperStatement postStmt;

	ForStatement() {}

	public static ForStatement define() {
		return new ForStatement();
	}

	public void make(SuperStatement preStmt, Expression condition, SuperStatement postStmt, Statement loopBody) {
		super.make(condition, loopBody);
		this.preStmt = preStmt;
		this.postStmt = postStmt;
	}

	// For ASTInterface

	@Override
	public void printAST(AstPrinter printer, int tab) {
		printer.println("[for]loop#" + objectIndex, tab);
		printer.println("[pre]", tab + 1);
		preStmt.printAST(printer, tab + 2);
		printer.println("[cond](" + Expression.deepToString(condition) + ")", tab + 1);
		printer.println("[post]", tab + 1);
		postStmt.printAST(printer, tab + 2);
		printer.println("[body]", tab + 1);
		loopBody.printAST(printer, tab + 1);
	}

	// For IR

	@Override
	public void makeIR(CFGMaker maker) {
		preStmt.makeIR(maker);

		Label conditionLabel = maker.makeLabel();
		Label bodyLabel = maker.makeLabel();
		Label continueDest = maker.makeLabel();
		Label nextLabel = maker.makeLabel();
		maker.setLoop(continueDest, nextLabel);

		maker.putLabel(conditionLabel);
		condition.makeConditionIR(maker, bodyLabel, nextLabel);

		maker.putLabel(bodyLabel);
		loopBody.makeIR(maker);

		maker.putLabel(continueDest);
		postStmt.makeIR(maker);
		maker.putJump(conditionLabel);

		maker.endLoop();
		maker.putLabel(nextLabel);
	}
}
