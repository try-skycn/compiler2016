package CtyCompiler2016.AST.Constituent.Statement;

import CtyCompiler2016.AST.ASTInterface;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;

/**
 * Created by SkytimChen on 3/19/16.
 */
public abstract class Statement implements ASTInterface {
	public abstract void makeIR(CFGMaker maker);
}
