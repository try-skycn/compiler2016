package CtyCompiler2016.AST.Constituent.Statement;

import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Type.BasicType.BoolType;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.ControlFlow.Label;
import CtyCompiler2016.Printer.AstPrinter;

/**
 * Created by SkytimChen on 3/30/16.
 */
public class SelectStatement extends Statement {
	private final Expression condition;
	private final Statement ifBody, elseBody;

	private SelectStatement(Expression condition, Statement ifBody, Statement elseBody) {
		this.condition = condition;
		this.ifBody = ifBody;
		this.elseBody = elseBody;
	}

	public static SelectStatement make(Expression condition, Statement ifBody, Statement elseBody) {
		CompilerError.check(condition.getType() instanceof BoolType, ErrorType.IncompatibleType);
		return  new SelectStatement(condition, ifBody, elseBody);
	}

	// For ASTInterface

	@Override
	public void printAST(AstPrinter printer, int tab) {
		printer.println("[if](" + Expression.deepToString(condition) + ")", tab);
		printer.println("[then]", tab);
		ifBody.printAST(printer, tab + 1);
		if (elseBody != null) {
			printer.println("[else]", tab);
			elseBody.printAST(printer, tab + 1);
		}
	}

	// For IR

	@Override
	public void makeIR(CFGMaker maker) {
		Label nextLabel = maker.makeLabel();
		Label ifLabel = maker.makeLabel();
		if (elseBody == null) {
			condition.makeConditionIR(maker, ifLabel, nextLabel);

			maker.putLabel(ifLabel);
			ifBody.makeIR(maker);
			maker.putJump(nextLabel);
		} else {
			Label elseLabel = maker.makeLabel();

			condition.makeConditionIR(maker, ifLabel, elseLabel);

			maker.putLabel(ifLabel);
			ifBody.makeIR(maker);
			maker.putJump(nextLabel);

			maker.putLabel(elseLabel);
			elseBody.makeIR(maker);
			maker.putJump(nextLabel);
		}
		maker.putLabel(nextLabel);
	}
}
