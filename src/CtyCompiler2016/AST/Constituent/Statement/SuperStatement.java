package CtyCompiler2016.AST.Constituent.Statement;

import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.Printer.AstPrinter;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by SkytimChen on 3/30/16.
 */
public class SuperStatement extends Statement {
	private final List<Expression> exprList;

	private SuperStatement(List<Expression> exprList) {
		this.exprList = exprList;
	}

	public static SuperStatement make(List<Expression> exprList) {
		return new SuperStatement(exprList);
	}

	public static SuperStatement make() {
		return new SuperStatement(new LinkedList<>());
	}

	// For ASTInterface

	@Override
	public void printAST(AstPrinter printer, int tab) {
		printer.println("[exprstmt]", tab);
		for (Expression expression : exprList) {
			printer.println(Expression.deepToString(expression), tab + 1);
		}
		printer.println(";", tab);
	}

	// For IR

	@Override
	public void makeIR(CFGMaker maker) {
		for (Expression expression : exprList) {
			expression.makeIR(maker);
		}
	}
}
