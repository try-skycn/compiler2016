package CtyCompiler2016.AST.Constituent.Expression;

import CtyCompiler2016.AST.Constituent.Expression.Constant.StringConstanceExpression;
import CtyCompiler2016.AST.Constituent.Expression.Field.FrameFieldExpression;
import CtyCompiler2016.AST.Constituent.Expression.Field.StaticFieldExpression;
import CtyCompiler2016.AST.Type.BasicType.BoolType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.ControlFlow.Label;
import CtyCompiler2016.IR.IRStructure;
import CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction.MoveInstruction;
import CtyCompiler2016.IR.Register.ExprLeft;
import CtyCompiler2016.IR.Register.ExprOperand;
import CtyCompiler2016.IR.Register.ImmediateValue;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;

/**
 * Created by SkytimChen on 3/25/16.
 */
public abstract class Expression {
	public abstract Type getType();
	public abstract boolean isLeft();

	public static void resetStatic() {
		StringConstanceExpression.resetStatic();
		StaticFieldExpression.resetStatic();
		FrameFieldExpression.resetStatic();
	}

	// For ASTInterface

	public abstract void printAST(StringBuilder builder);

	public static String deepToString(Expression expr) {
		StringBuilder builder = new StringBuilder();
		expr.printAST(builder);
		return builder.toString();
	}

	// For IR

	public static void resetStaticIR(IRStructure structure) {
		StringConstanceExpression.resetStaticIR(structure);
		StaticFieldExpression.resetStaticIR(structure);
	}

	public ExprLeft makeLeftIR(CFGMaker maker) {
		CompilerError.errorHandle(ErrorType.UnknownError);
		return null;
	}

	public abstract ExprOperand makeIR(CFGMaker maker);

	protected VirtualRegister makeBoolIR(CFGMaker maker) {
		Label ifTrue = maker.makeLabel();
		Label ifFalse = maker.makeLabel();
		Label nextLabel = maker.makeLabel();
		makeConditionIR(maker, ifTrue, ifFalse);

		VirtualRegister dest = maker.makeLocal();

		maker.putLabel(ifTrue);
		MoveInstruction.make(maker, dest, ImmediateValue.boolTrue());
		maker.putJump(nextLabel);

		maker.putLabel(ifFalse);
		MoveInstruction.make(maker, dest, ImmediateValue.boolFalse());

		maker.putLabel(nextLabel);
		return dest;
	}

	public void makeConditionIR(CFGMaker maker, Label ifTrue, Label ifFalse) {
		if (getType() instanceof BoolType) {
			maker.putBranch(makeIR(maker), ifTrue, ifFalse);
		} else {
			CompilerError.errorHandle(ErrorType.UnknownError);
		}
	}
}
