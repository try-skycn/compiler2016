package CtyCompiler2016.AST.Constituent.Expression.Field;

import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Table.FieldTable.FieldValue.ClassFieldValue;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction.LoadInstruction;
import CtyCompiler2016.IR.Register.*;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;

/**
 * Created by SkytimChen on 3/28/16.
 */
public class ComponentExpression extends FieldExpression {
	private final Expression pointer;
	private final ClassFieldValue value;

	private ComponentExpression(Expression pointer, ClassFieldValue value) {
		this.pointer = pointer;
		this.value = value;
	}

	public static ComponentExpression make(Expression pointer, ClassFieldValue value) {
		return new ComponentExpression(pointer, value);
	}

	@Override
	public Type getType() {
		return value.getType();
	}

	// For ASTInterface

	@Override
	public void printAST(StringBuilder builder) {
		builder.append("[component]");
		builder.append("(");
		builder.append(value.index);
		builder.append(" @ ");
		pointer.printAST(builder);
		builder.append(")");
	}

	// For IR

	private VirtualAddress getAddress(CFGMaker maker) {
		VirtualRegister startAddress = ((VirtualRegister) pointer.makeIR(maker));
		return maker.makeAddress(startAddress, ImmediateValue.make(value.getOffset()), getType());
	}

	@Override
	public VirtualAddress makeLeftIR(CFGMaker maker) {
		return getAddress(maker);
	}

	@Override
	public VirtualRegister makeIR(CFGMaker maker) {
		return LoadInstruction.make(maker, getAddress(maker));
	}
}
