package CtyCompiler2016.AST.Constituent.Expression.Field;

import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Type.ArrayType;
import CtyCompiler2016.AST.Type.BasicType.IntType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction.BinaryInstruction;
import CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction.LoadInstruction;
import CtyCompiler2016.IR.Instruction.InstructionCode;
import CtyCompiler2016.IR.Register.ExprOperand;
import CtyCompiler2016.IR.Register.ImmediateValue;
import CtyCompiler2016.IR.Register.VirtualAddress;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;

/**
 * Created by SkytimChen on 3/30/16.
 */
public class SubscriptExpression extends FieldExpression {
	private final Expression pointer;
	private final Expression index;
	private final Type type;

	private SubscriptExpression(Expression pointer, Expression index) {
		this.pointer = pointer;
		this.index = index;
		type = ((ArrayType) pointer.getType()).getOriginalType();
	}

	public static SubscriptExpression make(Expression pointer, Expression index) {
		CompilerError.check(pointer.getType() instanceof ArrayType, ErrorType.IncompatibleType);
		CompilerError.check(index.getType() instanceof IntType, ErrorType.IncompatibleType);
		return new SubscriptExpression(pointer, index);
	}

	@Override
	public Type getType() {
		return type;
	}

	// For ASTInterface

	@Override
	public void printAST(StringBuilder builder) {
		builder.append("[subscript]");
		builder.append("(");
		index.printAST(builder);
		builder.append(" @ ");
		pointer.printAST(builder);
		builder.append(")");
	}

	// For IR

	private VirtualAddress getAddress(CFGMaker maker) {
		VirtualRegister pointerOperand = ((VirtualRegister) pointer.makeIR(maker));
		ExprOperand indexOperand = index.makeIR(maker);
		ExprOperand offset = BinaryInstruction.make(maker, InstructionCode.Multiply, indexOperand, ImmediateValue.make(type.sizeof()));
		return maker.makeAddress(pointerOperand, offset, getType());
	}

	@Override
	public VirtualAddress makeLeftIR(CFGMaker maker) {
		return getAddress(maker);
	}

	@Override
	public VirtualRegister makeIR(CFGMaker maker) {
		return LoadInstruction.make(maker, getAddress(maker));
	}
}
