package CtyCompiler2016.AST.Constituent.Expression.Field;

import CtyCompiler2016.AST.Table.FieldTable.FieldValue.StaticFieldValue;
import CtyCompiler2016.AST.Type.ClassType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.IRStructure;
import CtyCompiler2016.IR.Register.VirtualRegister.StaticRegister;
import CtyCompiler2016.Tool.UIDManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SkytimChen on 3/31/16.
 */
public class StaticFieldExpression extends FieldExpression implements StaticFieldValue {
	private final int uID = staticFieldList.add(this);
	private static UIDManager<StaticFieldExpression> staticFieldList;

	public static void resetStatic() {
		staticFieldList = new UIDManager<>();
	}

	public final int index;

	private StaticFieldExpression(int index) {
		this.index = index;
	}

	public static StaticFieldExpression make(int index) {
		return new StaticFieldExpression(index);
	}

	@Override
	public Type getType() {
		return ClassType.globalClass.getFieldType(index);
	}

	// For ASTInterface

	@Override
	public void printAST(StringBuilder builder) {
		builder.append("[static]");
		builder.append("(");
		builder.append("#");
		builder.append(index);
		builder.append(")");
	}

	// For IR

	private static List<StaticRegister> staticRegisterList;

	public static void resetStaticIR(IRStructure structure) {
		staticRegisterList = new ArrayList<>();
		for (StaticFieldExpression expression : staticFieldList.getList()) {
			staticRegisterList.add(structure.makeStaticWord(0));
		}
	}

	private StaticRegister getRegister(CFGMaker maker) {
		StaticRegister register = staticRegisterList.get(uID);
		maker.useStatic(register);
		return register;
	}

	@Override
	public StaticRegister makeLeftIR(CFGMaker maker) {
		return getRegister(maker);
	}

	@Override
	public StaticRegister makeIR(CFGMaker maker) {
		return getRegister(maker);
	}
}
