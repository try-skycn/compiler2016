package CtyCompiler2016.AST.Constituent.Expression.Field;

import CtyCompiler2016.AST.Constituent.Expression.Expression;

/**
 * Created by SkytimChen on 3/29/16.
 */
public abstract class FieldExpression extends Expression {
	@Override
	public boolean isLeft() {
		return true;
	}
}
