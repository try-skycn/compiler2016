package CtyCompiler2016.AST.Constituent.Expression.Field;

import CtyCompiler2016.AST.Constituent.Frame.FrameFieldMaker;
import CtyCompiler2016.AST.Table.FieldTable.FieldValue.FrameFieldValue;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;
import CtyCompiler2016.Tool.UIDManager;

/**
 * Created by SkytimChen on 3/28/16.
 */
public class FrameFieldExpression extends FieldExpression implements FrameFieldValue {
	private final int uID = frameFieldList.add(this);
	private static UIDManager<FrameFieldExpression> frameFieldList;

	public static void resetStatic() {
		frameFieldList = new UIDManager<>();
	}

	private final FrameFieldMaker pool;
	private final int index;

	private FrameFieldExpression(FrameFieldMaker pool, int index) {
		this.pool = pool;
		this.index = index;
	}

	public static FrameFieldExpression make(FrameFieldMaker pool, int index) {
		return new FrameFieldExpression(pool, index);
	}

	@Override
	public Type getType() {
		return pool.getFieldType(index);
	}

	// For ASTInterface

	@Override
	public void printAST(StringBuilder builder) {
		builder.append("[local]");
		builder.append("(");
		builder.append(index);
		builder.append(" @ pool#");
		builder.append(pool.getFrameIndex());
		builder.append(")");
	}

	// For IR

	private VirtualRegister getRegister(CFGMaker maker) {
		if (pool.getFrameIndex() == 0) {
			return maker.getPara(index);
		} else {
			return maker.getLocal(uID);
		}
	}

	@Override
	public VirtualRegister makeLeftIR(CFGMaker maker) {
		return getRegister(maker);
	}

	@Override
	public VirtualRegister makeIR(CFGMaker maker) {
		return getRegister(maker);
	}
}
