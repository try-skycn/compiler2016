package CtyCompiler2016.AST.Constituent.Expression.Binary;

import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Constituent.Expression.Operator;
import CtyCompiler2016.AST.Type.ArrayType;
import CtyCompiler2016.AST.Type.BasicType.BoolType;
import CtyCompiler2016.AST.Type.BasicType.IntType;
import CtyCompiler2016.AST.Type.BasicType.StringType;
import CtyCompiler2016.AST.Type.ClassType;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction.BinaryInstruction;
import CtyCompiler2016.IR.Register.ExprOperand;

/**
 * Created by SkytimChen on 3/26/16.
 */
public abstract class BinaryExpression extends Expression {
	final Operator operator;
	final Expression left, right;

	BinaryExpression(Expression left, Operator operator, Expression right) {
		this.operator = operator;
		this.left = left;
		this.right = right;
	}

	public static Expression make(Expression left, String oper, Expression right) {
		if ((left.getType() instanceof IntType) && (left.getType() == right.getType())) {
			if (oper.equals("+") || oper.equals("-") || oper.equals("*") || oper.equals("/") || oper.equals("%") || oper.equals("|") || oper.equals("^") || oper.equals("&") || oper.equals("<<") || oper.equals(">>")) {
				return IntBinaryExpression.make(left, oper, right);
			}
		}
		if ((left.getType() instanceof StringType) && (left.getType() == right.getType())) {
			if (oper.equals("+")) {
				return ConcatenateExpression.make(left, oper, right);
			}
		}
		if ((left.getType() instanceof IntType) && (left.getType() == right.getType())) {
			if (oper.equals("<") || oper.equals("<=") || oper.equals(">") || oper.equals(">=")) {
				return IntRelationExpression.make(left, oper, right);
			}
		}
		if ((left.getType() instanceof StringType) && (left.getType() == right.getType())) {
			if (oper.equals("<") || oper.equals("<=") || oper.equals(">") || oper.equals(">=") || oper.equals("==") || oper.equals("!=")) {
				return StringRelationExpression.make(left, oper, right);
			}
		}
		if ((left.getType() instanceof BoolType) && (left.getType() == right.getType())) {
			if (oper.equals("||") || oper.equals("&&")) {
				return BoolBinaryExpression.make(left, oper, right);
			}
		}
		if (left.isLeft() && (left.getType() == right.getType())) {
			if (oper.equals("=")) {
				return AssignExpression.make(left, oper, right);
			}
		}
		if (left.isLeft() && ((left.getType() instanceof ClassType) || (left.getType() instanceof ArrayType)) && right.getType() == null) {
			if (oper.equals("=")) {
				return AssignExpression.make(left, oper, right);
			}
		}
		if ((left.getType() == right.getType()) && !(left.getType() instanceof StringType) || (left.getType() instanceof ClassType || left.getType() instanceof ArrayType) && right.getType() == null || (right.getType() instanceof ClassType || right.getType() instanceof ArrayType) && left.getType() == null) {
			if (oper.equals("==") || oper.equals("!=")) {
				return EquivBasicExpression.make(left, oper, right);
			}
		}
		CompilerError.errorHandle(ErrorType.IncompatibleType);
		return null;
	}

	@Override
	public boolean isLeft() {
		return false;
	}

	// For ASTInterface

	@Override
	public void printAST(StringBuilder builder) {
		builder.append("[operator]");
		builder.append(operator.toString());
		builder.append("(");
		left.printAST(builder);
		builder.append(", ");
		right.printAST(builder);
		builder.append(")");
	}

	// For IR

	@Override
	public ExprOperand makeIR(CFGMaker maker) {
		ExprOperand leftOperand = left.makeIR(maker);
		ExprOperand rightOperand = right.makeIR(maker);
		return BinaryInstruction.make(maker, operator.toInstructionCode(), leftOperand, rightOperand);
	}
}
