package CtyCompiler2016.AST.Constituent.Expression.Binary;
import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Constituent.Expression.Special.MethodCallExpression;
import CtyCompiler2016.AST.Constituent.Method.SystemMethod.Global.StringConcatenate;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;

import java.util.Arrays;

//"+":(left@string)&&(left==right)#Concatenate->string;
interface ConcatenateExpression {
	static Expression make(Expression left, String operator, Expression right) {
		if (operator.equals("+")) {
			return MethodCallExpression.make(StringConcatenate.instance, Arrays.asList(left, right));
		} else {
			CompilerError.errorHandle(ErrorType.UnknownError);
		}
		return null;
	}
}
