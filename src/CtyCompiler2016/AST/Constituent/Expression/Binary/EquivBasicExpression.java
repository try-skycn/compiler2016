package CtyCompiler2016.AST.Constituent.Expression.Binary;
import CtyCompiler2016.AST.Constituent.Expression.*;
import CtyCompiler2016.AST.Type.*;
import CtyCompiler2016.AST.Type.BasicType.*;
import CtyCompiler2016.CompilerError.*;
//"==""!=":(left==right)&&!(left@string)||(left@class||left@array)&&right==null#EquivBasic->bool;
public class EquivBasicExpression extends BinaryExpression {
	private final Type type;

	private EquivBasicExpression(Expression left, Operator operator, Expression right) {
		super(left, operator, right);
		type = BoolType.getInstance();
	}

	public static EquivBasicExpression make(Expression left, String operator, Expression right) {
		Operator op = null;
		if (operator.equals("==")) {
			op = Operator.Equiv;
		} else if (operator.equals("!=")) {
			op = Operator.NotEquiv;
		} else {
			CompilerError.errorHandle(ErrorType.UnknownError);
		}
		return new EquivBasicExpression (left, op, right);
	}

	@Override
	public Type getType() {
		return type;
	}
}
