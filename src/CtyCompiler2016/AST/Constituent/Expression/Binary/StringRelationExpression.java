package CtyCompiler2016.AST.Constituent.Expression.Binary;
import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Constituent.Expression.Special.MethodCallExpression;
import CtyCompiler2016.AST.Constituent.Method.SystemMethod.Global.*;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;

import java.util.Arrays;

//"<":(left@string)&&(left==right)#StringRelation->bool;
interface StringRelationExpression {
	static Expression make(Expression left, String operator, Expression right) {
		if (operator.equals("<")) {
			return MethodCallExpression.make(StringLess.instance, Arrays.asList(left, right));
		} else if (operator.equals("<=")) {
			return MethodCallExpression.make(StringLessEqual.instance, Arrays.asList(left, right));
		} else if (operator.equals(">")) {
			return MethodCallExpression.make(StringGreater.instance, Arrays.asList(left, right));
		} else if (operator.equals(">=")) {
			return MethodCallExpression.make(StringGreaterEqual.instance, Arrays.asList(left, right));
		} else if (operator.equals("==")) {
			return MethodCallExpression.make(StringEqual.instance, Arrays.asList(left, right));
		} else if (operator.equals("!=")) {
			return MethodCallExpression.make(StringNotEqual.instance, Arrays.asList(left, right));
		} else {
			CompilerError.errorHandle(ErrorType.UnknownError);
		}
		return null;
	}
}
