package CtyCompiler2016.AST.Constituent.Expression.Binary;
import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Constituent.Expression.Operator;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Register.ExprLeft;
import CtyCompiler2016.IR.Register.ExprOperand;

//"=":(left==right)#Assign->left;
public class AssignExpression extends BinaryExpression {
	private final Type type;

	private AssignExpression(Expression left, Operator operator, Expression right) {
		super(left, operator, right);
		type = left.getType();
	}

	public static AssignExpression make(Expression left, String operator, Expression right) {
		Operator op = null;
		if (operator.equals("=")) {
			op = Operator.Assign;
		} else {
			CompilerError.errorHandle(ErrorType.UnknownError);
		}
		return new AssignExpression (left, op, right);
	}

	@Override
	public Type getType() {
		return type;
	}

	// For IR

	@Override
	public ExprOperand makeIR(CFGMaker maker) {
		ExprLeft leftOperand = left.makeLeftIR(maker);
		ExprOperand rightResult = right.makeIR(maker);
		leftOperand.consume(maker, rightResult);
		return rightResult;
	}
}
