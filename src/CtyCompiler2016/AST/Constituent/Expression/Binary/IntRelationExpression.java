package CtyCompiler2016.AST.Constituent.Expression.Binary;
import CtyCompiler2016.AST.Constituent.Expression.*;
import CtyCompiler2016.AST.Type.*;
import CtyCompiler2016.AST.Type.BasicType.*;
import CtyCompiler2016.CompilerError.*;
//"<""<="">"">=":(left@int)&&(left==right)#IntRelation->bool;
public class IntRelationExpression extends BinaryExpression {
	private final Type type;

	private IntRelationExpression(Expression left, Operator operator, Expression right) {
		super(left, operator, right);
		type = BoolType.getInstance();
	}

	public static IntRelationExpression make(Expression left, String operator, Expression right) {
		Operator op = null;
		if (operator.equals(">=")) {
			op = Operator.GreaterEqual;
		} else if (operator.equals(">")) {
			op = Operator.Greater;
		} else if (operator.equals("<")) {
			op = Operator.Less;
		} else if (operator.equals("<=")) {
			op = Operator.LessEqual;
		} else {
			CompilerError.errorHandle(ErrorType.UnknownError);
		}
		return new IntRelationExpression (left, op, right);
	}

	@Override
	public Type getType() {
		return type;
	}
}
