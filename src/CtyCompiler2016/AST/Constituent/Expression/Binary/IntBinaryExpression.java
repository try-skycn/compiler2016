package CtyCompiler2016.AST.Constituent.Expression.Binary;
import CtyCompiler2016.AST.Constituent.Expression.*;
import CtyCompiler2016.AST.Type.*;
import CtyCompiler2016.AST.Type.BasicType.*;
import CtyCompiler2016.CompilerError.*;
//"+""-""*""/""%""|""^""&""<<"">>":(left@int)&&(left==right)#IntBinary->int;
public class IntBinaryExpression extends BinaryExpression {
	private final Type type;

	private IntBinaryExpression(Expression left, Operator operator, Expression right) {
		super(left, operator, right);
		type = IntType.getInstance();
	}

	public static IntBinaryExpression make(Expression left, String operator, Expression right) {
		Operator op = null;
		if (operator.equals("<<")) {
			op = Operator.LeftShift;
		} else if (operator.equals(">>")) {
			op = Operator.RightShift;
		} else if (operator.equals("/")) {
			op = Operator.Division;
		} else if (operator.equals("^")) {
			op = Operator.BitwiseXor;
		} else if (operator.equals("-")) {
			op = Operator.Subtraction;
		} else if (operator.equals("|")) {
			op = Operator.BitwiseOr;
		} else if (operator.equals("+")) {
			op = Operator.Addition;
		} else if (operator.equals("*")) {
			op = Operator.Multiplication;
		} else if (operator.equals("&")) {
			op = Operator.BitwiseAnd;
		} else if (operator.equals("%")) {
			op = Operator.Remainder;
		} else {
			CompilerError.errorHandle(ErrorType.UnknownError);
		}
		return new IntBinaryExpression (left, op, right);
	}

	@Override
	public Type getType() {
		return type;
	}
}
