package CtyCompiler2016.AST.Constituent.Expression.Binary;
import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Constituent.Expression.Operator;
import CtyCompiler2016.AST.Type.BasicType.BoolType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.ControlFlow.Label;
import CtyCompiler2016.IR.Register.ExprOperand;

//"||""&&":(left@bool)&&(left==right)#BoolBinary->bool;
public class BoolBinaryExpression extends BinaryExpression {
	private final Type type;

	private BoolBinaryExpression(Expression left, Operator operator, Expression right) {
		super(left, operator, right);
		type = BoolType.getInstance();
	}

	public static BoolBinaryExpression make(Expression left, String operator, Expression right) {
		Operator op = null;
		if (operator.equals("&&")) {
			op = Operator.LogicalAnd;
		} else if (operator.equals("||")) {
			op = Operator.LogicalOr;
		} else {
			CompilerError.errorHandle(ErrorType.UnknownError);
		}
		return new BoolBinaryExpression (left, op, right);
	}

	@Override
	public Type getType() {
		return type;
	}

	// For IR

	@Override
	public ExprOperand makeIR(CFGMaker maker) {
		return makeBoolIR(maker);
	}

	@Override
	public void makeConditionIR(CFGMaker maker, Label ifTrue, Label ifFalse) {
		if (operator == Operator.LogicalAnd) {
			Label midLabel = maker.makeLabel();
			left.makeConditionIR(maker, midLabel, ifFalse);
			maker.putLabel(midLabel);
			right.makeConditionIR(maker, ifTrue, ifFalse);
		} else if (operator == Operator.LogicalOr) {
			Label midLabel = maker.makeLabel();
			left.makeConditionIR(maker, ifTrue, midLabel);
			maker.putLabel(midLabel);
			right.makeConditionIR(maker, ifTrue, ifFalse);
		} else {
			CompilerError.errorHandle(ErrorType.UnknownError);
		}
	}
}
