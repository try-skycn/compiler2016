package CtyCompiler2016.AST.Constituent.Expression.Special.New;

import CtyCompiler2016.AST.Type.ClassType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction.NewInstruction;
import CtyCompiler2016.IR.Register.ExprOperand;
import CtyCompiler2016.IR.Register.ImmediateValue;

/**
 * Created by SkytimChen on 4/4/16.
 */
public class PlainNewExpression extends NewExpression {
	private final ClassType type;

	private PlainNewExpression(ClassType type) {
		this.type = type;
	}

	public static PlainNewExpression make(Type type) {
		if (type instanceof ClassType) {
			return new PlainNewExpression(((ClassType) type));
		}
		CompilerError.errorHandle(ErrorType.IncompatibleType);
		return null;
	}

	@Override
	public Type getType() {
		return type;
	}

	@Override
	public boolean isLeft() {
		return false;
	}

	// For ASTInterface

	@Override
	public void printAST(StringBuilder builder) {
		builder.append("[new]");
		builder.append(type.toString());
	}

	// For IR

	@Override
	public ExprOperand makeIR(CFGMaker maker) {
		return NewInstruction.make(maker, ImmediateValue.make(type.allocateSize()));
	}
}
