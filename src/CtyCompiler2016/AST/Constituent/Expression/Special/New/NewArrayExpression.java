package CtyCompiler2016.AST.Constituent.Expression.Special.New;

import CtyCompiler2016.AST.Constituent.Expression.Constant.IntConstantExpression;
import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Type.ArrayType;
import CtyCompiler2016.AST.Type.BasicType.IntType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction.BinaryInstruction;
import CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction.NewInstruction;
import CtyCompiler2016.IR.Instruction.FlowInstruction.ImpactInstruction.StoreInstruction;
import CtyCompiler2016.IR.Instruction.InstructionCode;
import CtyCompiler2016.IR.Register.ExprOperand;
import CtyCompiler2016.IR.Register.ImmediateValue;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;

/**
 * Created by SkytimChen on 4/1/16.
 */
public class NewArrayExpression extends NewExpression {
	private final Type type, resultType;
	private final Expression expr;

	private NewArrayExpression(Type type, Expression expr) {
		this.type = type;
		this.expr = expr;
		this.resultType = ArrayType.getInstance(type);
	}

	public static NewArrayExpression make(Type baseType, Expression expr) {
		CompilerError.check(expr.getType() instanceof IntType, ErrorType.IncompatibleType);
		if (expr instanceof IntConstantExpression) {
			CompilerError.check(((IntConstantExpression) expr).getValue() > 0, ErrorType.UnknownExpression);
		}
		return new NewArrayExpression(baseType, expr);
	}

	@Override
	public Type getType() {
		return resultType;
	}

	@Override
	public boolean isLeft() {
		return false;
	}

	// For ASTInterface

	@Override
	public void printAST(StringBuilder builder) {
		builder.append("[new]");
		builder.append("(");
		builder.append(type.toString());
		builder.append(", ");
		expr.printAST(builder);
		builder.append(")");
	}

	// For IR

	@Override
	public VirtualRegister makeIR(CFGMaker maker) {
		ExprOperand arrayLength = expr.makeIR(maker);
		ExprOperand length = BinaryInstruction.make(maker, InstructionCode.Multiply, arrayLength, ImmediateValue.make(type.sizeof()));
		ExprOperand extendLength = BinaryInstruction.make(maker, InstructionCode.Add, length, ImmediateValue.intSize());
		VirtualRegister address = NewInstruction.make(maker, extendLength);
		StoreInstruction.make(maker, arrayLength, maker.makeAddress(address, ImmediateValue.zero(), IntType.getInstance()));
		return BinaryInstruction.assign(maker, InstructionCode.Add, address, ImmediateValue.intSize());
	}
}
