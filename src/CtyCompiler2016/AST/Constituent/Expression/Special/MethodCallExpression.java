package CtyCompiler2016.AST.Constituent.Expression.Special;

import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Constituent.Method.Method;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;

import java.util.List;

public class MethodCallExpression extends Expression {
	private final Method method;
	private final List<Expression> parameterList;

	private MethodCallExpression(Method method, List<Expression> parameterList) {
		this.method = method;
		this.parameterList = parameterList;
	}

	public static MethodCallExpression make(Method method, List<Expression> parameterList) {
		CompilerError.check(method != null, ErrorType.UnknownMethod);
		return new MethodCallExpression(method, parameterList);
	}

	@Override
	public Type getType() {
		return method.getReturnType();
	}

	@Override
	public boolean isLeft() {
		return false;
	}

	public Method getMethod() {
		return method;
	}

	public List<Expression> getParameterList() {
		return parameterList;
	}

	// For ASTInterface

	@Override
	public void printAST(StringBuilder builder) {
		builder.append("[call]");
		builder.append("(method#");
		builder.append(method.uID);
		builder.append(")");
		builder.append("(");
		int cnt = 0;
		for (Expression expression : parameterList) {
			if (cnt++ > 0) {
				builder.append(", ");
			}
			expression.printAST(builder);
		}
		builder.append(")");
	}

	// For IR

	@Override
	public VirtualRegister makeIR(CFGMaker maker) {
		return method.makeRawCallIR(maker, parameterList);
	}
}
