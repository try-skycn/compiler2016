package CtyCompiler2016.AST.Constituent.Expression.Constant;

import CtyCompiler2016.AST.Constituent.Expression.Expression;

/**
 * Created by SkytimChen on 3/28/16.
 */
public abstract class ConstantExpression extends Expression {
	public abstract Object getValue();

	@Override
	public boolean isLeft() {
		return false;
	}
}
