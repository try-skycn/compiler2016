package CtyCompiler2016.AST.Constituent.Expression.Constant;

import CtyCompiler2016.AST.Type.BasicType.IntType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.ControlFlow.Label;
import CtyCompiler2016.IR.Register.ExprResult;
import CtyCompiler2016.IR.Register.ImmediateValue;

/**
 * Created by SkytimChen on 3/28/16.
 */
public class IntConstantExpression extends ConstantExpression {
	private final int constant;

	private IntConstantExpression(int constant) {
		this.constant = constant;
	}

	public static IntConstantExpression make(String text) {
		return new IntConstantExpression(Integer.parseInt(text));
	}

	public static IntConstantExpression make(Integer constant) {
		return new IntConstantExpression(constant);
	}

	@Override
	public Integer getValue() {
		return constant;
	}

	@Override
	public Type getType() {
		return IntType.getInstance();
	}

	// For ASTInterface

	@Override
	public void printAST(StringBuilder builder) {
		builder.append(Integer.toString(constant));
	}

	// For IR

	@Override
	public ImmediateValue makeIR(CFGMaker maker) {
		return ImmediateValue.make(constant);
	}
}
