package CtyCompiler2016.AST.Constituent.Expression.Constant;

import CtyCompiler2016.AST.Type.BasicType.BoolType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Register.ImmediateValue;

/**
 * Created by SkytimChen on 3/28/16.
 */
public class BoolConstantExpression extends ConstantExpression {
	public final boolean constant;

	private BoolConstantExpression(boolean constant) {
		this.constant = constant;
	}

	private static final BoolConstantExpression trueConstant = new BoolConstantExpression(true);
	private static final BoolConstantExpression falseConstant = new BoolConstantExpression(false);

	public static BoolConstantExpression make(String text) {
		if (text.equals("true")) {
			return trueConstant;
		} else {
			return falseConstant;
		}
	}

	public static BoolConstantExpression make(Boolean constant) {
		if (constant) {
			return trueConstant;
		} else {
			return falseConstant;
		}
	}

	@Override
	public Boolean getValue() {
		return constant;
	}

	@Override
	public Type getType() {
		return BoolType.getInstance();
	}

	// For ASTInterface

	@Override
	public void printAST(StringBuilder builder) {
		builder.append(Boolean.toString(constant));
	}

	// For IR

	@Override
	public ImmediateValue makeIR(CFGMaker maker) {
		return ImmediateValue.make(constant ? 1 : 0);
	}
}
