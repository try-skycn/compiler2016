package CtyCompiler2016.AST.Constituent.Expression.Constant;

import CtyCompiler2016.AST.Type.BasicType.StringType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.IRStructure;
import CtyCompiler2016.IR.Register.VirtualRegister.StaticRegister;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;
import CtyCompiler2016.Tool.UIDManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SkytimChen on 3/28/16.
 */
public class StringConstanceExpression extends ConstantExpression {
	private final int uID = stringList.add(this);
	private static UIDManager<StringConstanceExpression> stringList;

	public static void resetStatic() {
		stringList = new UIDManager<>();
	}

	private final String constant;

	private StringConstanceExpression(String constant) {
		this.constant = constant;
	}

	public static StringConstanceExpression make(String text) {
		return new StringConstanceExpression(text);
	}

	@Override
	public String getValue() {
		return constant;
	}

	@Override
	public Type getType() {
		return StringType.getInstance();
	}

	// For ASTInterface

	@Override
	public void printAST(StringBuilder builder) {
		builder.append("[string]");
		builder.append(constant);
	}

	// For IR

	private static List<StaticRegister> stringRegisterList;

	public static void resetStaticIR(IRStructure structure) {
		stringRegisterList = new ArrayList<>();
		for (StringConstanceExpression expression : stringList.getList()) {
			stringRegisterList.add(structure.putString(expression.constant));
		}
	}

	@Override
	public VirtualRegister makeIR(CFGMaker maker) {
		return stringRegisterList.get(uID);
	}
}
