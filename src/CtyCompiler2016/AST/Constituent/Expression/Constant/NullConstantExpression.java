package CtyCompiler2016.AST.Constituent.Expression.Constant;

import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Register.ImmediateValue;

/**
 * Created by SkytimChen on 3/28/16.
 */
public class NullConstantExpression extends ConstantExpression {
	private static final NullConstantExpression instance = new NullConstantExpression();

	public static NullConstantExpression make() {
		return instance;
	}

	@Override
	public Object getValue() {
		return null;
	}

	@Override
	public Type getType() {
		return null;
	}

	// For ASTInterface

	@Override
	public void printAST(StringBuilder builder) {
		builder.append("null");
	}

	// For IR

	@Override
	public ImmediateValue makeIR(CFGMaker maker) {
		return ImmediateValue.zero();
	}
}
