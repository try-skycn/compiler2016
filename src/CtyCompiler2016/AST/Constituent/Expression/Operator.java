package CtyCompiler2016.AST.Constituent.Expression;

import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.IR.Instruction.InstructionCode;

/**
 * Created by SkytimChen on 4/1/16.
 */
public enum Operator {
	PrefixIncrement,
	PrefixDecrement,
	SuffixIncrement,
	SuffixDecrement,
	UnaryPlus,
	UnaryMinus,
	BitwiseNot,
	BitwiseAnd,
	BitwiseXor,
	BitwiseOr,
	LogicalNot,
	LogicalAnd,
	LogicalOr,
	Multiplication,
	Division,
	Remainder,
	Addition,
	Subtraction,
	LeftShift,
	RightShift,
	Less,
	LessEqual,
	Greater,
	GreaterEqual,
	Equiv,
	NotEquiv,
	Assign;

	public InstructionCode toInstructionCode() {
		switch (this) {
			case Equiv:
				return InstructionCode.Equal;
			case NotEquiv:
				return InstructionCode.NotEqual;
			case Less:
				return InstructionCode.Less;
			case LessEqual:
				return InstructionCode.LessEqual;
			case Greater:
				return InstructionCode.Greater;
			case GreaterEqual:
				return InstructionCode.GreaterEqual;
			case Addition:
				return InstructionCode.Add;
			case Subtraction:
				return InstructionCode.Subtract;
			case Multiplication:
				return InstructionCode.Multiply;
			case Division:
				return InstructionCode.Divide;
			case Remainder:
				return InstructionCode.Remainder;
			case BitwiseOr:
				return InstructionCode.Or;
			case BitwiseAnd:
				return InstructionCode.And;
			case BitwiseXor:
				return InstructionCode.Xor;
			case LeftShift:
				return InstructionCode.LeftShift;
			case RightShift:
				return InstructionCode.RightShift;
			default:
				CompilerError.errorHandle(ErrorType.UnknownError);
				return null;
		}
	}
}
