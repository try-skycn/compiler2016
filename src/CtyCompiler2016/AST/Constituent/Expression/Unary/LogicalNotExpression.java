package CtyCompiler2016.AST.Constituent.Expression.Unary;

import CtyCompiler2016.AST.Constituent.Expression.Constant.BoolConstantExpression;
import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Constituent.Expression.Operator;
import CtyCompiler2016.AST.Type.BasicType.BoolType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.ControlFlow.Label;
import CtyCompiler2016.IR.Register.ExprOperand;

/**
 * Created by SkytimChen on 4/1/16.
 */
public class LogicalNotExpression extends UnaryExpression {
	private LogicalNotExpression(Operator operator, Expression operand) {
		super(operator, operand);
	}

	public static Expression make(String operator, Expression operand) {
		Operator op = null;
		if (operator.equals("!")) {
			op = Operator.LogicalNot;
		} else {
			CompilerError.errorHandle(ErrorType.UnknownError);
		}
		if (operand instanceof BoolConstantExpression) {
			return BoolConstantExpression.make(calc(op, ((BoolConstantExpression) operand).getValue()));
		}
		return new LogicalNotExpression(op, operand);
	}

	public static Boolean calc(Operator operator, Boolean operand) {
		switch (operator) {
			case LogicalNot:
				return !operand;
			default:
				CompilerError.errorHandle(ErrorType.UnknownError);
				return null;
		}
	}

	@Override
	public Type getType() {
		return BoolType.getInstance();
	}

	@Override
	public boolean isLeft() {
		return false;
	}

	// For IR

	@Override
	public ExprOperand makeIR(CFGMaker maker) {
		return makeBoolIR(maker);
	}

	@Override
	public void makeConditionIR(CFGMaker maker, Label ifTrue, Label ifFalse) {
		operand.makeConditionIR(maker, ifFalse, ifTrue);
	}
}
