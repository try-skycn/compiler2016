package CtyCompiler2016.AST.Constituent.Expression.Unary;

import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Constituent.Expression.Operator;

/**
 * Created by SkytimChen on 3/26/16.
 */
public abstract class UnaryExpression extends Expression {
	final Operator operator;
	final Expression operand;

	UnaryExpression(Operator operator, Expression operand) {
		this.operator = operator;
		this.operand = operand;
	}

	// For ASTInterface

	@Override
	public void printAST(StringBuilder builder) {
		builder.append("[operator]");
		builder.append(operator.toString());
		builder.append("(");
		operand.printAST(builder);
		builder.append(")");
	}
}
