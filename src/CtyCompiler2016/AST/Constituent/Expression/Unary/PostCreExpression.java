package CtyCompiler2016.AST.Constituent.Expression.Unary;

import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Constituent.Expression.Operator;
import CtyCompiler2016.AST.Type.BasicType.IntType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction.BinaryInstruction;
import CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction.MoveInstruction;
import CtyCompiler2016.IR.Instruction.InstructionCode;
import CtyCompiler2016.IR.Register.ExprLeft;
import CtyCompiler2016.IR.Register.ExprOperand;
import CtyCompiler2016.IR.Register.ImmediateValue;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;

/**
 * Created by SkytimChen on 4/1/16.
 */
public class PostCreExpression extends UnaryExpression {
	private PostCreExpression(Operator operator, Expression operand) {
		super(operator, operand);
	}

	public static Expression make(String operator, Expression operand) {
		Operator op = null;
		if (operator.equals("++")) {
			op = Operator.SuffixIncrement;
		} else if (operator.equals("--")) {
			op = Operator.SuffixDecrement;
		} else {
			CompilerError.errorHandle(ErrorType.UnknownError);
		}
		return new PostCreExpression(op, operand);
	}

	@Override
	public Type getType() {
		return IntType.getInstance();
	}

	@Override
	public boolean isLeft() {
		return false;
	}

	// For IR

	private ImmediateValue getDelta() {
		switch (operator) {
			case SuffixIncrement:
				return ImmediateValue.one();
			case SuffixDecrement:
				return ImmediateValue.one().negate();
			default:
				CompilerError.errorHandle(ErrorType.UnknownError);
				return null;
		}
	}

	@Override
	public ExprOperand makeIR(CFGMaker maker) {
		ExprLeft lowerOperand = operand.makeLeftIR(maker);
		VirtualRegister register = lowerOperand.load(maker);
		VirtualRegister result = MoveInstruction.alias(maker, register);
		VirtualRegister tmp = BinaryInstruction.assign(maker, InstructionCode.Add, register, getDelta());
		lowerOperand.consume(maker, tmp);
		return result;
	}
}
