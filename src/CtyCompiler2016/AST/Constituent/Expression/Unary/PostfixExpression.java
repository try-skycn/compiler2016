package CtyCompiler2016.AST.Constituent.Expression.Unary;

import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Type.BasicType.IntType;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;

/**
 * Created by SkytimChen on 4/1/16.
 */
public interface PostfixExpression {
	static Expression make(String operator, Expression operand) {
		if (operand.getType() instanceof IntType && operand.isLeft()) {
			if (operator.equals("++") || operator.equals("--")) {
				return PostCreExpression.make(operator, operand);
			}
		}
		CompilerError.errorHandle(ErrorType.IncompatibleType);
		return null;
	}
}
