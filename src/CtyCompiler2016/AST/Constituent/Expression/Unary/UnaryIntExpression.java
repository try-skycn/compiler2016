package CtyCompiler2016.AST.Constituent.Expression.Unary;

import CtyCompiler2016.AST.Constituent.Expression.Constant.IntConstantExpression;
import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Constituent.Expression.Operator;
import CtyCompiler2016.AST.Type.BasicType.IntType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction.UnaryInstruction;
import CtyCompiler2016.IR.Instruction.InstructionCode;
import CtyCompiler2016.IR.Register.ExprOperand;

/**
 * Created by SkytimChen on 4/1/16.
 */
public class UnaryIntExpression extends UnaryExpression {
	private UnaryIntExpression(Operator operator, Expression operand) {
		super(operator, operand);
	}

	public static Expression make(String operator, Expression operand) {
		Operator op = null;
		if (operator.equals("~")) {
			op = Operator.BitwiseNot;
		} else if (operator.equals("+")) {
			return operand;
		} else if (operator.equals("-")) {
			op = Operator.UnaryMinus;
		} else {
			CompilerError.errorHandle(ErrorType.UnknownError);
		}
		if (operand instanceof IntConstantExpression) {
			return IntConstantExpression.make(calc(op, ((IntConstantExpression) operand).getValue()));
		}
		return new UnaryIntExpression(op, operand);
	}

	public static Integer calc(Operator operator, Integer operand) {
		switch (operator) {
			case BitwiseNot:
				return ~operand;
			case UnaryMinus:
				return -operand;
			default:
				CompilerError.errorHandle(ErrorType.UnknownError);
				return null;
		}
	}

	@Override
	public Type getType() {
		return IntType.getInstance();
	}

	@Override
	public boolean isLeft() {
		return false;
	}

	// For IR

	@Override
	public ExprOperand makeIR(CFGMaker maker) {
		ExprOperand lowerOperand = operand.makeIR(maker);
		switch (operator) {
			case BitwiseNot:
				return UnaryInstruction.make(maker, InstructionCode.Not, lowerOperand);
			case UnaryMinus:
				return UnaryInstruction.make(maker, InstructionCode.Negate, lowerOperand);
			default:
				CompilerError.errorHandle(ErrorType.UnknownError);
				return null;
		}
	}
}
