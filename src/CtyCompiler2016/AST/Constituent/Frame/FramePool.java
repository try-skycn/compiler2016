package CtyCompiler2016.AST.Constituent.Frame;

import CtyCompiler2016.AST.Table.FieldTable.FieldList;
import CtyCompiler2016.AST.Table.FieldTable.FieldValue.FieldValue;
import CtyCompiler2016.AST.Table.FieldTable.FieldValue.FrameFieldValue;
import CtyCompiler2016.AST.Type.Type;

/**
 * Created by SkytimChen on 3/31/16.
 */
public class FramePool implements FrameFieldMaker {
	private final FieldList fieldList = new FieldList();
	private final FrameFieldMaker enclosingMaker;
	private final int frameIndex;

	private FramePool(FrameFieldMaker enclosingMaker) {
		this.enclosingMaker = enclosingMaker;
		frameIndex = enclosingMaker.getFrameIndex() + 1;
	}

	public static FramePool make(FrameFieldMaker enclosingMaker) {
		return new FramePool(enclosingMaker);
	}

	@Override
	public FieldValue makeField(Type type) {
		return FrameFieldValue.make(this, fieldList.add(type));
	}

	@Override
	public Type getFieldType(int index) {
		return fieldList.getType(index);
	}

	@Override
	public int getFrameIndex() {
		return frameIndex;
	}

	@Override
	public FieldList getPool() {
		return fieldList;
	}
}
