package CtyCompiler2016.AST.Constituent.Frame;

import CtyCompiler2016.AST.Table.FieldTable.FieldList;
import CtyCompiler2016.AST.Table.FieldTable.FieldMaker;

/**
 * Created by SkytimChen on 3/31/16.
 */
public interface FrameFieldMaker extends FieldMaker {
	int getFrameIndex();
	FieldList getPool();
}
