package CtyCompiler2016.AST.Constituent.Method;

import CtyCompiler2016.AST.Type.Type;

import java.util.List;

/**
 * Created by SkytimChen on 3/31/16.
 */
public class StaticMethod extends Method {
	protected StaticMethod(Type returnType, List<Type> paraTypeList, List<String> paraNameList) {
		super(returnType);
		for (int i = 0; i < paraTypeList.size(); i++) {
			addPara(paraTypeList.get(i), paraNameList.get(i));
		}
	}

	public static StaticMethod make(Type returnType, List<Type> paraTypeList, List<String> paraNameList) {
		return new StaticMethod(returnType, paraTypeList, paraNameList);
	}
}
