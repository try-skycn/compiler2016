package CtyCompiler2016.AST.Constituent.Method;

import CtyCompiler2016.AST.Constituent.Expression.Field.FrameFieldExpression;
import CtyCompiler2016.AST.Table.FieldTable.FieldValue.FrameFieldValue;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;

import java.util.List;

/**
 * Created by SkytimChen on 3/31/16.
 */
public class MemberMethod extends Method {
	public final Type thisType;
	public final FrameFieldExpression thisPointer;

	protected MemberMethod(Type thisType, Type returnType, List<Type> paraTypeList, List<String> paraNameList) {
		super(returnType);
		this.thisType = thisType;
		FrameFieldValue value = makeField(thisType);
		if (value instanceof FrameFieldExpression) {
			thisPointer = ((FrameFieldExpression) value);
		} else {
			thisPointer = null;
			CompilerError.errorHandle(ErrorType.UnknownError);
		}
		for (int i = 0; i < paraTypeList.size(); i++) {
			addPara(paraTypeList.get(i), paraNameList.get(i));
		}
	}

	public static MemberMethod make(Type thisType, Type returnType, List<Type> paraTypeList, List<String> paraNameList) {
		return new MemberMethod(thisType, returnType, paraTypeList, paraNameList);
	}

	// For MethodNames

	@Override
	public void setName(String name) {
		super.setName(thisType.toString() + ":" + name);
	}
}
