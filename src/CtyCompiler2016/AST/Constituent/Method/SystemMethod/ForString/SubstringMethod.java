package CtyCompiler2016.AST.Constituent.Method.SystemMethod.ForString;

import CtyCompiler2016.AST.Constituent.Method.MemberMethod;
import CtyCompiler2016.AST.Type.BasicType.IntType;
import CtyCompiler2016.AST.Type.BasicType.StringType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.IR.IRStructure;
import CtyCompiler2016.IR.MethodStructure.BuildIn.BuildInSubstring;
import CtyCompiler2016.IR.MethodStructure.MethodStructure;

import java.util.Arrays;
import java.util.List;

/**
 * Created by SkytimChen on 4/4/16.
 */
public class SubstringMethod extends MemberMethod {
	private SubstringMethod(Type thisType, Type returnType, List<Type> paraTypeList, List<String> paraNameList) {
		super(thisType, returnType, paraTypeList, paraNameList);
	}

	public static SubstringMethod instance;

	public static void resetStatic() {
		instance = make();
	}

	private static SubstringMethod make() {
		return new SubstringMethod(StringType.getInstance(), StringType.getInstance(), Arrays.asList(IntType.getInstance(), IntType.getInstance()), Arrays.asList("left", "right"));
	}

	// For IR

	@Override
	public MethodStructure makeIR(IRStructure structure) {
		return BuildInSubstring.make(this);
	}
}
