package CtyCompiler2016.AST.Constituent.Method.SystemMethod.ForString;

import CtyCompiler2016.AST.Constituent.Method.MemberMethod;
import CtyCompiler2016.AST.Type.BasicType.IntType;
import CtyCompiler2016.AST.Type.BasicType.StringType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.IR.IRStructure;
import CtyCompiler2016.IR.MethodStructure.BuildIn.BuildInParseInt;
import CtyCompiler2016.IR.MethodStructure.MethodStructure;

import java.util.Collections;
import java.util.List;

/**
 * Created by SkytimChen on 4/4/16.
 */
public class ParseIntMethod extends MemberMethod {
	private ParseIntMethod(Type thisType, Type returnType, List<Type> paraTypeList, List<String> paraNameList) {
		super(thisType, returnType, paraTypeList, paraNameList);
	}

	public static ParseIntMethod instance;

	public static void resetStatic() {
		instance = make();
	}

	private static ParseIntMethod make() {
		return new ParseIntMethod(StringType.getInstance(), IntType.getInstance(), Collections.emptyList(), Collections.emptyList());
	}

	// For IR

	@Override
	public MethodStructure makeIR(IRStructure structure) {
		return BuildInParseInt.make(this);
	}
}
