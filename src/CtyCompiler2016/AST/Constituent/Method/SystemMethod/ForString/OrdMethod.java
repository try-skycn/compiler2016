package CtyCompiler2016.AST.Constituent.Method.SystemMethod.ForString;

import CtyCompiler2016.AST.Constituent.Method.MemberMethod;
import CtyCompiler2016.AST.Type.BasicType.IntType;
import CtyCompiler2016.AST.Type.BasicType.StringType;
import CtyCompiler2016.AST.Type.BasicType.VoidType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.IRStructure;
import CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction.LoadInstruction;
import CtyCompiler2016.IR.MethodStructure.MethodStructure;
import CtyCompiler2016.IR.Register.ExprOperand;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;

import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by SkytimChen on 4/4/16.
 */
public class OrdMethod extends MemberMethod {
	private OrdMethod(Type thisType, Type returnType, List<Type> paraTypeList, List<String> paraNameList) {
		super(thisType, returnType, paraTypeList, paraNameList);
	}

	public static OrdMethod instance;

	public static void resetStatic() {
		instance = make();
	}

	private static OrdMethod make() {
		return new OrdMethod(StringType.getInstance(), IntType.getInstance(), Collections.singletonList(IntType.getInstance()), Collections.singletonList("pos"));
	}

	// For IR

	@Override
	protected int inituID() {
		return -1;
	}

	@Override
	public VirtualRegister makeCallIR(CFGMaker maker, List<ExprOperand> paraRegisterList) {
		ListIterator<ExprOperand> iterator = paraRegisterList.listIterator();
		VirtualRegister str = ((VirtualRegister) iterator.next());
		ExprOperand offset = iterator.next();
		return LoadInstruction.make(maker, maker.makeAddress(str, offset, VoidType.getInstance()));
	}

	@Override
	public MethodStructure makeIR(IRStructure structure) {
		return null;
	}
}
