package CtyCompiler2016.AST.Constituent.Method.SystemMethod.ForString;

import CtyCompiler2016.AST.Constituent.Method.MemberMethod;
import CtyCompiler2016.AST.Type.BasicType.IntType;
import CtyCompiler2016.AST.Type.BasicType.StringType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.IRStructure;
import CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction.LoadInstruction;
import CtyCompiler2016.IR.MethodStructure.MethodStructure;
import CtyCompiler2016.IR.Register.ExprOperand;
import CtyCompiler2016.IR.Register.ImmediateValue;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;

import java.util.Collections;
import java.util.List;

/**
 * Created by SkytimChen on 4/4/16.
 */
public class LengthMethod extends MemberMethod {
	private LengthMethod(Type thisType, Type returnType, List<Type> paraTypeList, List<String> paraNameList) {
		super(thisType, returnType, paraTypeList, paraNameList);
	}

	public static LengthMethod instance;

	public static void resetStatic() {
		instance = make();
	}

	private static LengthMethod make() {
		return new LengthMethod(StringType.getInstance(), IntType.getInstance(), Collections.emptyList(), Collections.emptyList());
	}

	// For IR

	@Override
	protected int inituID() {
		return -1;
	}

	@Override
	public VirtualRegister makeCallIR(CFGMaker maker, List<ExprOperand> paraRegisterList) {
		VirtualRegister ary = ((VirtualRegister) paraRegisterList.get(0));
		return LoadInstruction.make(maker, maker.makeAddress(ary, ImmediateValue.intSize().negate(), IntType.getInstance()));
	}

	@Override
	public MethodStructure makeIR(IRStructure structure) {
		return null;
	}
}
