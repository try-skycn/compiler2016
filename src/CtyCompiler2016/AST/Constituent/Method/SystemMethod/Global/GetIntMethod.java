package CtyCompiler2016.AST.Constituent.Method.SystemMethod.Global;

import CtyCompiler2016.AST.Constituent.Method.StaticMethod;
import CtyCompiler2016.AST.Type.BasicType.IntType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.IRStructure;
import CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction.CallInstruction;
import CtyCompiler2016.IR.Instruction.Tool.Syscall;
import CtyCompiler2016.IR.MethodStructure.MethodStructure;
import CtyCompiler2016.IR.Register.ExprOperand;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;
import CtyCompiler2016.Tool.SyscallID;

import java.util.Collections;
import java.util.List;

/**
 * Created by SkytimChen on 4/4/16.
 */
public class GetIntMethod extends StaticMethod {
	private GetIntMethod(Type returnType, List<Type> paraTypeList, List<String> paraNameList) {
		super(returnType, paraTypeList, paraNameList);
	}

	public static GetIntMethod instance;

	public static void resetStatic() {
		instance = make();
	}

	private static GetIntMethod make() {
		return new GetIntMethod(IntType.getInstance(), Collections.emptyList(), Collections.emptyList());
	}

	// For IR

	@Override
	protected int inituID() {
		return -1;
	}

	@Override
	public VirtualRegister makeCallIR(CFGMaker maker, List<ExprOperand> paraExprList) {
		Syscall callInfo = Syscall.make(SyscallID.ReadInt, maker.makeOperand(paraExprList));
		return CallInstruction.make(maker, callInfo);
	}

	@Override
	public MethodStructure makeIR(IRStructure structure) {
		return null;
	}
}
