package CtyCompiler2016.AST.Constituent.Method.SystemMethod.Global;

import CtyCompiler2016.AST.Constituent.Method.StaticMethod;
import CtyCompiler2016.AST.Type.BasicType.StringType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.IR.IRStructure;
import CtyCompiler2016.IR.MethodStructure.BuildIn.BuildInStringConcatenate;
import CtyCompiler2016.IR.MethodStructure.MethodStructure;

import java.util.Arrays;
import java.util.List;

/**
 * Created by SkytimChen on 4/15/16.
 */
public class StringConcatenate extends StaticMethod {
	private StringConcatenate(Type returnType, List<Type> paraTypeList, List<String> paraNameList) {
		super(returnType, paraTypeList, paraNameList);
	}

	public static StringConcatenate instance;

	public static void resetStatic() {
		instance = make();
	}

	private static StringConcatenate make() {
		return new StringConcatenate(StringType.getInstance(), Arrays.asList(StringType.getInstance(), StringType.getInstance()), Arrays.asList("a", "b"));
	}

	// For IR

	@Override
	public MethodStructure makeIR(IRStructure structure) {
		return BuildInStringConcatenate.make(this);
	}
}
