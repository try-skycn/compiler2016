package CtyCompiler2016.AST.Constituent.Method.SystemMethod.Global;

import CtyCompiler2016.AST.Constituent.Method.StaticMethod;
import CtyCompiler2016.AST.Type.BasicType.StringType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.IR.IRStructure;
import CtyCompiler2016.IR.MethodStructure.BuildIn.BuildInGetString;
import CtyCompiler2016.IR.MethodStructure.MethodStructure;

import java.util.Collections;
import java.util.List;

/**
 * Created by SkytimChen on 4/4/16.
 */
public class GetStringMethod extends StaticMethod {
	private GetStringMethod(Type returnType, List<Type> paraTypeList, List<String> paraNameList) {
		super(returnType, paraTypeList, paraNameList);
	}

	public static GetStringMethod instance;

	public static void resetStatic() {
		instance = make();
	}

	private static GetStringMethod make() {
		return new GetStringMethod(StringType.getInstance(), Collections.emptyList(), Collections.emptyList());
	}

	// For IR

	@Override
	public MethodStructure makeIR(IRStructure structure) {
		return BuildInGetString.make(this);
	}
}
