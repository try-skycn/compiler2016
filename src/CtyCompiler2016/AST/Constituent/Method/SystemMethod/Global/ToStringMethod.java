package CtyCompiler2016.AST.Constituent.Method.SystemMethod.Global;

import CtyCompiler2016.AST.Constituent.Method.StaticMethod;
import CtyCompiler2016.AST.Type.BasicType.IntType;
import CtyCompiler2016.AST.Type.BasicType.StringType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.IR.IRStructure;
import CtyCompiler2016.IR.MethodStructure.BuildIn.BuildInToString;
import CtyCompiler2016.IR.MethodStructure.MethodStructure;

import java.util.Collections;
import java.util.List;

/**
 * Created by SkytimChen on 4/4/16.
 */
public class ToStringMethod extends StaticMethod {
	private ToStringMethod(Type returnType, List<Type> paraTypeList, List<String> paraNameList) {
		super(returnType, paraTypeList, paraNameList);
	}

	public static ToStringMethod instance;

	public static void resetStatic() {
		instance = make();
	}

	private static ToStringMethod make() {
		return new ToStringMethod(StringType.getInstance(), Collections.singletonList(IntType.getInstance()), Collections.singletonList("num"));
	}

	// For IR

	@Override
	public MethodStructure makeIR(IRStructure structure) {
		return BuildInToString.make(this);
	}
}
