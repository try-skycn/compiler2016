package CtyCompiler2016.AST.Constituent.Method.SystemMethod.Global;

import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Constituent.Expression.Special.MethodCallExpression;
import CtyCompiler2016.AST.Constituent.Method.Method;
import CtyCompiler2016.AST.Constituent.Method.StaticMethod;
import CtyCompiler2016.AST.Type.BasicType.StringType;
import CtyCompiler2016.AST.Type.BasicType.VoidType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.IRStructure;
import CtyCompiler2016.IR.MethodStructure.BuildIn.BuildInPrintln;
import CtyCompiler2016.IR.MethodStructure.MethodStructure;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;

import java.util.Collections;
import java.util.List;

public class PrintlnMethod extends StaticMethod {
	private PrintlnMethod(Type returnType, List<Type> paraTypeList, List<String> paraNameList) {
		super(returnType, paraTypeList, paraNameList);
	}

	public static PrintlnMethod instance;

	public static void resetStatic() {
		instance = make();
	}

	private static PrintlnMethod make() {
		return new PrintlnMethod(VoidType.getInstance(), Collections.singletonList(StringType.getInstance()), Collections.singletonList("str"));
	}

	// For IR

	@Override
	public VirtualRegister makeRawCallIR(CFGMaker maker, List<Expression> parameterList) {
		Expression elem = parameterList.get(0);
		if (elem instanceof MethodCallExpression) {
			Method method = ((MethodCallExpression) elem).getMethod();
			if (method instanceof ToStringMethod) {
				return PrintIntln.instance.makeRawCallIR(maker, ((MethodCallExpression) elem).getParameterList());
			} else if (method instanceof StringConcatenate) {
				Expression leftExpr = ((MethodCallExpression) elem).getParameterList().get(0);
				Expression rightExpr = ((MethodCallExpression) elem).getParameterList().get(1);
				PrintMethod.instance.makeRawCallIR(maker, Collections.singletonList(leftExpr));
				return makeRawCallIR(maker, Collections.singletonList(rightExpr));
			}
		}
		return makeCallIR(maker, Collections.singletonList(elem.makeIR(maker)));
	}

	@Override
	public MethodStructure makeIR(IRStructure structure) {
		return BuildInPrintln.make(this);
	}
}
