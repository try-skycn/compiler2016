package CtyCompiler2016.AST.Constituent.Method.SystemMethod.Global;

import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Constituent.Expression.Special.MethodCallExpression;
import CtyCompiler2016.AST.Constituent.Method.Method;
import CtyCompiler2016.AST.Constituent.Method.StaticMethod;
import CtyCompiler2016.AST.Type.BasicType.StringType;
import CtyCompiler2016.AST.Type.BasicType.VoidType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.IRStructure;
import CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction.CallInstruction;
import CtyCompiler2016.IR.Instruction.Tool.Syscall;
import CtyCompiler2016.IR.MethodStructure.MethodStructure;
import CtyCompiler2016.IR.Register.ExprOperand;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;
import CtyCompiler2016.Tool.SyscallID;

import java.util.Collections;
import java.util.List;

public class PrintMethod extends StaticMethod {
	private PrintMethod(Type returnType, List<Type> paraTypeList, List<String> paraNameList) {
		super(returnType, paraTypeList, paraNameList);
	}

	public static PrintMethod instance;

	public static void resetStatic() {
		instance = make();
	}

	private static PrintMethod make() {
		return new PrintMethod(VoidType.getInstance(), Collections.singletonList(StringType.getInstance()), Collections.singletonList("str"));
	}

	// For IR

	@Override
	protected int inituID() {
		return -1;
	}

	@Override
	public VirtualRegister makeCallIR(CFGMaker maker, List<ExprOperand> paraExprList) {
		Syscall callInfo = Syscall.make(SyscallID.PrintString, maker.makeOperand(paraExprList));
		return CallInstruction.make(maker, callInfo);
	}

	@Override
	public VirtualRegister makeRawCallIR(CFGMaker maker, List<Expression> parameterList) {
		Expression elem = parameterList.get(0);
		if (elem instanceof MethodCallExpression) {
			Method method = ((MethodCallExpression) elem).getMethod();
			if (method instanceof ToStringMethod) {
				return PrintIntMethod.instance.makeRawCallIR(maker, ((MethodCallExpression) elem).getParameterList());
			} else if (method instanceof StringConcatenate) {
				Expression leftExpr = ((MethodCallExpression) elem).getParameterList().get(0);
				Expression rightExpr = ((MethodCallExpression) elem).getParameterList().get(1);
				makeRawCallIR(maker, Collections.singletonList(leftExpr));
				return makeRawCallIR(maker, Collections.singletonList(rightExpr));
			}
		}
		return makeCallIR(maker, Collections.singletonList(elem.makeIR(maker)));
	}

	@Override
	public MethodStructure makeIR(IRStructure structure) {
		return null;
	}
}
