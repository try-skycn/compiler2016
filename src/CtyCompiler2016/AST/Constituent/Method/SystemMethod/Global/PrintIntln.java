package CtyCompiler2016.AST.Constituent.Method.SystemMethod.Global;

import CtyCompiler2016.AST.Constituent.Method.StaticMethod;
import CtyCompiler2016.AST.Type.BasicType.IntType;
import CtyCompiler2016.AST.Type.BasicType.VoidType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.IR.IRStructure;
import CtyCompiler2016.IR.MethodStructure.BuildIn.BuildInPrintIntln;
import CtyCompiler2016.IR.MethodStructure.MethodStructure;

import java.util.Collections;
import java.util.List;

/**
 * Created by SkytimChen on 4/27/16.
 */
public class PrintIntln extends StaticMethod {
	private PrintIntln(Type returnType, List<Type> paraTypeList, List<String> paraNameList) {
		super(returnType, paraTypeList, paraNameList);
	}

	public static PrintIntln instance;

	public static void resetStatic() {
		instance = make();
	}

	private static PrintIntln make() {
		return new PrintIntln(VoidType.getInstance(), Collections.singletonList(IntType.getInstance()), Collections.singletonList("x"));
	}

	// For IR

	@Override
	public MethodStructure makeIR(IRStructure structure) {
		return BuildInPrintIntln.make(this);
	}
}
