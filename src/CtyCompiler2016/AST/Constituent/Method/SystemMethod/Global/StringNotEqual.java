package CtyCompiler2016.AST.Constituent.Method.SystemMethod.Global;

import CtyCompiler2016.AST.Constituent.Method.StaticMethod;
import CtyCompiler2016.AST.Type.BasicType.BoolType;
import CtyCompiler2016.AST.Type.BasicType.StringType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.IR.IRStructure;
import CtyCompiler2016.IR.MethodStructure.BuildIn.BuildInStringCompare;
import CtyCompiler2016.IR.MethodStructure.BuildInMethod;
import CtyCompiler2016.IR.MethodStructure.MethodStructure;

import java.util.Arrays;
import java.util.List;

/**
 * Created by SkytimChen on 4/15/16.
 */
public class StringNotEqual extends StaticMethod {
	private StringNotEqual(Type returnType, List<Type> paraTypeList, List<String> paraNameList) {
		super(returnType, paraTypeList, paraNameList);
	}

	public static StringNotEqual instance;

	public static void resetStatic() {
		instance = make();
	}

	private static StringNotEqual make() {
		return new StringNotEqual(BoolType.getInstance(), Arrays.asList(StringType.getInstance(), StringType.getInstance()), Arrays.asList("a", "b"));
	}

	// For IR

	@Override
	public MethodStructure makeIR(IRStructure structure) {
		return BuildInStringCompare.make(this, BuildInMethod.BuildInMethodName.StringNotEqual);
	}
}
