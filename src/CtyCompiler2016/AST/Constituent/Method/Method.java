package CtyCompiler2016.AST.Constituent.Method;

import CtyCompiler2016.AST.ASTInterface;
import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Constituent.Frame.FrameFieldMaker;
import CtyCompiler2016.AST.Constituent.Method.SystemMethod.ForArray.SizeMethod;
import CtyCompiler2016.AST.Constituent.Method.SystemMethod.ForString.LengthMethod;
import CtyCompiler2016.AST.Constituent.Method.SystemMethod.ForString.OrdMethod;
import CtyCompiler2016.AST.Constituent.Method.SystemMethod.ForString.ParseIntMethod;
import CtyCompiler2016.AST.Constituent.Method.SystemMethod.ForString.SubstringMethod;
import CtyCompiler2016.AST.Constituent.Method.SystemMethod.Global.*;
import CtyCompiler2016.AST.Constituent.Statement.CompoundStatement;
import CtyCompiler2016.AST.Table.FieldTable.FieldList;
import CtyCompiler2016.AST.Table.FieldTable.FieldManager;
import CtyCompiler2016.AST.Table.FieldTable.FieldScope;
import CtyCompiler2016.AST.Table.FieldTable.FieldValue.FieldValue;
import CtyCompiler2016.AST.Table.FieldTable.FieldValue.FrameFieldValue;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.IRStructure;
import CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction.CallInstruction;
import CtyCompiler2016.IR.Instruction.Tool.ProcedureCall;
import CtyCompiler2016.IR.MethodStructure.MethodStructure;
import CtyCompiler2016.IR.Register.ExprOperand;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;
import CtyCompiler2016.Printer.AstPrinter;
import CtyCompiler2016.Tool.UIDManager;

import java.util.*;

public abstract class Method implements ASTInterface, FrameFieldMaker, FieldScope {
	private final Type returnType;
	private final List<Type> paraTypeList = new ArrayList<>();
	private CompoundStatement methodBody = null;

	Method(Type returnType) {
		this.returnType = returnType;
	}

	public void setMethodBody(CompoundStatement methodBody) {
		this.methodBody = methodBody;
	}

	void addPara(Type type, String name) {
		putField(name, makeField(type));
		paraTypeList.add(type);
	}

	public boolean matchParaList(List<Type> incomingList) {
		if (paraTypeList.size() == incomingList.size()) {
			for (int i = 0; i < incomingList.size(); i++) {
				if (paraTypeList.get(i) != incomingList.get(i)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	public Type getReturnType() {
		return returnType;
	}

	public List<Type> getParaTypeList() {
		return paraTypeList;
	}

	// For MethodNames

	private final Set<String> nameSet = new HashSet<>();

	public void setName(String name) {
		nameSet.add(name);
	}

	public Set<String> getNameSet() {
		return nameSet;
	}

	// For FieldMaker, FieldScope

	private static final int frameIndex = 0;
	private final FieldList paraList = new FieldList();
	private final FieldManager paraTable = new FieldManager();

	public int getParaCount() {
		return paraList.getSize();
	}

	@Override
	public FrameFieldValue makeField(Type type) {
		return FrameFieldValue.make(this, paraList.add(type));
	}

	@Override
	public Type getFieldType(int index) {
		return paraList.getType(index);
	}

	@Override
	public int getFrameIndex() {
		return frameIndex;
	}

	@Override
	public FieldList getPool() {
		return paraList;
	}

	@Override
	public void putField(String key, FieldValue value) {
		paraTable.putField(key, value);
	}

	@Override
	public FieldValue fetchField(String key) {
		return paraTable.fetchField(key);
	}

	@Override
	public Set<Map.Entry<String, FieldValue>> fieldEntrySet() {
		return paraTable.fieldEntrySet();
	}

	// For ASTInterface

	private static UIDManager<Method> methodList;
	public final int uID = inituID();

	protected int inituID() {
		return methodList.add(this);
	}

	public static void resetStatic() {
		methodList = new UIDManager<>();

		SizeMethod.resetStatic();

		LengthMethod.resetStatic();
		OrdMethod.resetStatic();
		ParseIntMethod.resetStatic();
		SubstringMethod.resetStatic();

		GetIntMethod.resetStatic();
		GetStringMethod.resetStatic();
		PrintlnMethod.resetStatic();
		PrintMethod.resetStatic();
		PrintIntMethod.resetStatic();
		PrintIntln.resetStatic();
		StringConcatenate.resetStatic();
		StringEqual.resetStatic();
		StringGreater.resetStatic();
		StringGreaterEqual.resetStatic();
		StringLess.resetStatic();
		StringLessEqual.resetStatic();
		StringNotEqual.resetStatic();
		ToStringMethod.resetStatic();
	}

	public static List<Method> getMethodList() {
		return methodList.getList();
	}

	@Override
	public void printAST(AstPrinter printer, int tab) {
		printer.println("[Overload](method#" + uID + ")return " + returnType.toString(), tab);
		printer.println("[parameter](pool#0)" + FieldList.deepToString(paraList), tab + 1);
		if (methodBody != null) {
			printer.println("[body]", tab + 1);
			methodBody.printAST(printer, tab + 2);
		}
	}

	// For IR

	public VirtualRegister makeCallIR(CFGMaker maker, List<ExprOperand> paraExprList) {
		return CallInstruction.make(maker, ProcedureCall.make(uID, maker.makeOperand(paraExprList)));
	}

	public VirtualRegister makeRawCallIR(CFGMaker maker, List<Expression> parameterList) {
		List<ExprOperand> paraList = new LinkedList<>();
		for (Expression expression : parameterList) {
			ExprOperand para = expression.makeIR(maker);
			paraList.add(para);
		}
		return makeCallIR(maker, paraList);
	}

	public MethodStructure makeIR(IRStructure structure) {
		CFGMaker maker = CFGMaker.make(this, structure);
		if (methodBody != null) {
			methodBody.makeIR(maker);
		}
		return maker.finish();
	}
}
