package CtyCompiler2016.AST.Type;

import CtyCompiler2016.AST.Type.BasicType.StringType;

/**
 * Created by SkytimChen on 3/14/16.
 */
public abstract class Type {

	public static int pointerSize = 4;
	private ArrayType arrayType = null;

	@Override
	public abstract String toString();

	public abstract int sizeof();

	ArrayType getArrayType() {
		if (arrayType == null) {
			arrayType = new ArrayType(this);
		}
		return arrayType;
	}

	public static void resetStatic() {
		StringType.resetStatic();
		ArrayType.resetStatic();
		ClassType.resetStatic();
	}
}
