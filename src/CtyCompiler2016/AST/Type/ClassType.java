package CtyCompiler2016.AST.Type;

import CtyCompiler2016.AST.ASTInterface;
import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Constituent.Expression.Field.FrameFieldExpression;
import CtyCompiler2016.AST.Constituent.Expression.Special.MethodCallExpression;
import CtyCompiler2016.AST.Constituent.Method.MemberMethod;
import CtyCompiler2016.AST.Constituent.Method.Method;
import CtyCompiler2016.AST.Constituent.Method.StaticMethod;
import CtyCompiler2016.AST.Constituent.Method.SystemMethod.Global.*;
import CtyCompiler2016.AST.Constituent.Statement.CompoundStatement;
import CtyCompiler2016.AST.Constituent.Statement.SuperStatement;
import CtyCompiler2016.AST.Table.ClassTable.ClassScope;
import CtyCompiler2016.AST.Table.FieldTable.*;
import CtyCompiler2016.AST.Table.FieldTable.FieldValue.ClassFieldValue;
import CtyCompiler2016.AST.Table.FieldTable.FieldValue.FieldValue;
import CtyCompiler2016.AST.Table.FieldTable.FieldValue.StaticFieldValue;
import CtyCompiler2016.AST.Table.MethodTable.MethodMaker;
import CtyCompiler2016.AST.Table.MethodTable.MethodManager;
import CtyCompiler2016.AST.Table.MethodTable.MethodScope;
import CtyCompiler2016.AST.Type.BasicType.IntType;
import CtyCompiler2016.AST.Type.BasicType.VoidType;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.Printer.AstPrinter;

import java.util.*;

/**
 * Created by SkytimChen on 3/31/16.
 */
public class ClassType extends Type implements ASTInterface, ClassScope, FieldMaker, FieldScope, MethodMaker, MethodScope {
	private final String name;
	private final ClassType enclosingClass;
	private Method defaultConstructor = null;
	private FrameFieldExpression thisPointer = null;

	private ClassType(String name, ClassType enclosingClass) {
		this.name = name;
		this.enclosingClass = enclosingClass;
	}

	public void substantiateConstructor() {
		if (isGlobal()) {
			// Check for main
			Method mainMethod = fetchMethod("main", Collections.emptyList());
			CompilerError.check(mainMethod.getReturnType() instanceof IntType, ErrorType.NoMainMethod);
			addFieldInit(MethodCallExpression.make(mainMethod, Collections.emptyList()));
		}
		if (defaultConstructor != null) {
			defaultConstructor.setName(toString() + ":constructor");
			CompoundStatement stmt = CompoundStatement.define(null);
			stmt.make(Collections.singletonList(SuperStatement.make(initList)));
			defaultConstructor.setMethodBody(stmt);
		}
	}

	public Method getDefaultConstructor() {
		return defaultConstructor;
	}

	public static ClassType define(String name, ClassType enclosingClass) {
		ClassType resultType = new ClassType(name.intern(), enclosingClass);
		//MemberMethod defaultConstructor = MemberMethod.empty(resultType, resultType, new ArrayList<>(), new ArrayList<>());
		//resultType.defaultConstructor = defaultConstructor;
		//resultType.thisPointer = defaultConstructor.thisPointer;
		return resultType;
	}

	public static ClassType globalClass;

	public static void resetStatic() {
		globalClass = makeGlobal();
	}

	private static ClassType makeGlobal() {
		ClassType tmpInstance = new ClassType(null, null);
		tmpInstance.putMethod("print", PrintMethod.instance);
		tmpInstance.putMethod("println", PrintlnMethod.instance);
		tmpInstance.putMethod("print", PrintIntMethod.instance);
		tmpInstance.putMethod("println", PrintIntln.instance);
		tmpInstance.putMethod("getString", GetStringMethod.instance);
		tmpInstance.putMethod("getInt", GetIntMethod.instance);
		tmpInstance.putMethod("toString", ToStringMethod.instance);
		tmpInstance.putMethod("$StringConcatenate", StringConcatenate.instance);
		tmpInstance.putMethod("$StringLess", StringLess.instance);
		tmpInstance.putMethod("$StringLessEqual", StringLessEqual.instance);
		tmpInstance.putMethod("$StringGreater", StringGreater.instance);
		tmpInstance.putMethod("$StringGreaterEqual", StringGreaterEqual.instance);
		tmpInstance.putMethod("$StringEqual", StringEqual.instance);
		tmpInstance.putMethod("$StringNotEqual", StringNotEqual.instance);
		tmpInstance.defaultConstructor = StaticMethod.make(VoidType.getInstance(), Collections.emptyList(), Collections.emptyList());
		return tmpInstance;
	}

	public boolean isGlobal() {
		return name == null;
	}

	public FrameFieldExpression getThisPointer() {
		return thisPointer;
	}

	// For Type

	@Override
	public String toString() {
		if (name != null) {
			return enclosingClass.toString() + "." + name;
		} else {
			return "#Mning";
		}
	}

	@Override
	public int sizeof() {
		return Type.pointerSize;
	}

	// For ClassScope

	private final Map<String, ClassType> classTable = new HashMap<>();

	@Override
	public void putClass(String key, ClassType value) {
		CompilerError.check(fetchClass(key) == null, ErrorType.Collision);
		classTable.put(key, value);
	}

	@Override
	public ClassType fetchClass(String key) {
		return classTable.get(key);
	}

	// For FieldMaker, FieldScope

	private final ClassFieldList fieldList = new ClassFieldList();
	private final FieldManager fieldTable = new FieldManager();
	private final List<Expression> initList = new ArrayList<>();

	public int getOffset(int index) {
		return fieldList.getOffset(index);
	}

	public int allocateSize() {
		return fieldList.getTotSize();
	}

	@Override
	public FieldValue makeField(Type type) {
		int index = fieldList.add(type);
		if (isGlobal()) {
			return StaticFieldValue.make(index);
		} else {
			return ClassFieldValue.make(this, index);
		}
	}

	public void addFieldInit(Expression expr) {
		initList.add(expr);
	}

	@Override
	public Type getFieldType(int index) {
		return fieldList.getType(index);
	}

	@Override
	public void putField(String key, FieldValue value) {
		fieldTable.putField(key, value);
	}

	@Override
	public FieldValue fetchField(String key) {
		return fieldTable.fetchField(key);
	}

	@Override
	public Set<Map.Entry<String, FieldValue>> fieldEntrySet() {
		return fieldTable.fieldEntrySet();
	}

	// For MethodMaker, MethodScope

	private final MethodManager methodManager = new MethodManager();

	@Override
	public Method makeMethod(Type returnType, List<String> nameList, List<Type> typeList) {
		if (isGlobal()) {
			return StaticMethod.make(returnType, typeList, nameList);
		} else {
			return MemberMethod.make(this, returnType, typeList, nameList);
		}
	}

	@Override
	public void putMethod(String key, Method value) {
		methodManager.putMethod(key, value);
	}

	@Override
	public Method fetchMethod(String key, List<Type> typeList) {
		return methodManager.fetchMethod(key, typeList);
	}

	// For ASTInterface

	@Override
	public void printAST(AstPrinter printer, int tab) {
		printer.println("[Class]" + toString(), tab);
		for (Map.Entry<String, ClassType> entry : classTable.entrySet()) {
			entry.getValue().printAST(printer, tab + 1);
			printer.println();
		}
		printer.println("[Field Pool]" + FieldList.deepToString(fieldList), tab + 1);
		if (defaultConstructor != null) {
			printer.println("[Default Constructor]", tab + 1);
			defaultConstructor.printAST(printer, tab + 2);
			printer.println();
		}
		for (Map.Entry<String, FieldValue> entry : fieldTable.fieldEntrySet()) {
			FieldValue value = entry.getValue();
			if (value instanceof ClassFieldValue) {
				ClassFieldValue classFieldValue = (ClassFieldValue) value;
				printer.println("[Field]#" + classFieldValue.index + ": " + classFieldValue.getType().toString() + " " + entry.getKey(), tab + 1);
				printer.println();
			}
		}
		methodManager.printAST(printer, tab + 1);
	}
}
