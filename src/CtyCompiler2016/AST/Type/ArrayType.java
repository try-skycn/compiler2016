package CtyCompiler2016.AST.Type;

import CtyCompiler2016.AST.Constituent.Method.Method;
import CtyCompiler2016.AST.Constituent.Method.SystemMethod.ForArray.SizeMethod;
import CtyCompiler2016.AST.Table.MethodTable.MethodManager;
import CtyCompiler2016.AST.Table.MethodTable.MethodScope;

import java.util.List;

/**
 * Created by SkytimChen on 3/14/16.
 */
public class ArrayType extends Type implements MethodScope {

	private final Type type;

	ArrayType(Type type) {
		this.type = type;
	}

	public static ArrayType getInstance(Type type) {
		return type.getArrayType();
	}

	public Type getOriginalType() {
		return type;
	}

	@Override
	public String toString() {
		return type.toString() + "[]";
	}

	@Override
	public int sizeof() {
		return Type.pointerSize;
	}

	// For MethodScope

	private static MethodManager methodManager;

	public static void resetStatic() {
		methodManager = new MethodManager();
		methodManager.putMethod("size", SizeMethod.instance);
	}

	@Override
	public void putMethod(String key, Method value) {
		methodManager.putMethod(key, value);
	}

	@Override
	public Method fetchMethod(String key, List<Type> typeList) {
		return methodManager.fetchMethod(key, typeList);
	}
}
