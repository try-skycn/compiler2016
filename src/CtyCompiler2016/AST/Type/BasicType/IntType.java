package CtyCompiler2016.AST.Type.BasicType;

/**
 * Created by SkytimChen on 3/14/16.
 */
public class IntType extends BasicType {

	private static IntType intInstance = new IntType();

	private IntType() {}

	@Override
	public String toString() {
		return "int";
	}

	@Override
	public int sizeof() {
		return 4;
	}

	public static IntType getInstance() {
		return intInstance;
	}
}
