package CtyCompiler2016.AST.Type.BasicType;

import CtyCompiler2016.AST.Constituent.Method.Method;
import CtyCompiler2016.AST.Constituent.Method.SystemMethod.ForString.LengthMethod;
import CtyCompiler2016.AST.Constituent.Method.SystemMethod.ForString.OrdMethod;
import CtyCompiler2016.AST.Constituent.Method.SystemMethod.ForString.ParseIntMethod;
import CtyCompiler2016.AST.Constituent.Method.SystemMethod.ForString.SubstringMethod;
import CtyCompiler2016.AST.Table.MethodTable.MethodManager;
import CtyCompiler2016.AST.Table.MethodTable.MethodScope;
import CtyCompiler2016.AST.Type.Type;

import java.util.List;

/**
 * Created by SkytimChen on 3/14/16.
 */
public class StringType extends BasicType implements MethodScope {

	private static StringType stringInstance = new StringType();

	private StringType() {}

	@Override
	public String toString() {
		return "string";
	}

	@Override
	public int sizeof() {
		return Type.pointerSize;
	}

	public static StringType getInstance() {
		return stringInstance;
	}

	// For MethodScope

	private static MethodManager methodManager;

	public static void resetStatic() {
		methodManager = new MethodManager();
		methodManager.putMethod("length", LengthMethod.instance);
		methodManager.putMethod("substring", SubstringMethod.instance);
		methodManager.putMethod("parseInt", ParseIntMethod.instance);
		methodManager.putMethod("ord", OrdMethod.instance);
	}

	@Override
	public void putMethod(String key, Method value) {
		methodManager.putMethod(key, value);
	}

	@Override
	public Method fetchMethod(String key, List<Type> typeList) {
		return methodManager.fetchMethod(key, typeList);
	}

}
