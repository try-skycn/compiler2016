package CtyCompiler2016.AST.Type.BasicType;

/**
 * Created by SkytimChen on 3/14/16.
 */
public class VoidType extends BasicType {

	private static VoidType voidInstance = new VoidType();

	private VoidType() {}

	@Override
	public String toString() {
		return "void";
	}

	@Override
	public int sizeof() {
		return 1;
	}

	public static VoidType getInstance() {
		return voidInstance;
	}
}
