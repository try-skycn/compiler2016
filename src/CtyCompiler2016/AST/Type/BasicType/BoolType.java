package CtyCompiler2016.AST.Type.BasicType;

/**
 * Created by SkytimChen on 3/14/16.
 */
public class BoolType extends BasicType {

	private static BoolType booleanInstance = new BoolType();

	private BoolType() {}

	@Override
	public String toString() {
		return "boolean";
	}

	@Override
	public int sizeof() {
		return 1;
	}

	public static BoolType getInstance() {
		return booleanInstance;
	}

}
