package CtyCompiler2016.AST;

import CtyCompiler2016.Printer.AstPrinter;

/**
 * Created by SkytimChen on 3/30/16.
 */
public interface ASTInterface {
	void printAST(AstPrinter printer, int tab);
}
