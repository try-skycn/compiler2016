package CtyCompiler2016.AST.Constructor;

import CtyCompiler2016.AST.Constituent.Method.Method;
import CtyCompiler2016.AST.Table.FieldTable.FieldValue.FieldValue;
import CtyCompiler2016.AST.Type.*;
import CtyCompiler2016.AST.Type.BasicType.BoolType;
import CtyCompiler2016.AST.Type.BasicType.IntType;
import CtyCompiler2016.AST.Type.BasicType.StringType;
import CtyCompiler2016.AST.Type.BasicType.VoidType;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.Parser.MningBaseListener;
import CtyCompiler2016.Parser.MningParser;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by SkytimChen on 3/25/16.
 */
class MemberConstructor extends MningBaseListener {
	private final Environment environment;
	private final Property property;

	static void construct(ParseTree tree, ParseTreeWalker walker, Environment environment, Property property) {
		MemberConstructor constructor = new MemberConstructor(environment, property);
		walker.walk(constructor, tree);
	}

	private MemberConstructor(Environment environment, Property property) {
		this.environment = environment;
		this.property = property;
	}

	@Override
	public void enterClassDeclaration(MningParser.ClassDeclarationContext ctx) {
		environment.enterClass(property.fetchClass(ctx));
	}

	@Override
	public void exitClassDeclaration(MningParser.ClassDeclarationContext ctx) {
		environment.exitClass();
	}

	// Type checker
	@Override
	public void exitTypeSpecifier(MningParser.TypeSpecifierContext ctx) {
		Type result = property.fetchType(ctx.plainTypeSpecifier());
		int arrayDim = ctx.arrayDimCounter.size();
		for (int i = 0; i < arrayDim; i++) {
			result = ArrayType.getInstance(result);
		}
		property.putType(ctx, result);
	}

	@Override
	public void exitClassTypeSpecifier(MningParser.ClassTypeSpecifierContext ctx) {
		property.putType(ctx, property.fetchType(ctx.classType()));
	}

	@Override
	public void exitBasicTypeSpecifier(MningParser.BasicTypeSpecifierContext ctx) {
		property.putType(ctx, property.fetchType(ctx.basicType()));
	}

	@Override
	public void exitIntType(MningParser.IntTypeContext ctx) {
		property.putType(ctx, IntType.getInstance());
	}

	@Override
	public void exitBoolType(MningParser.BoolTypeContext ctx) {
		property.putType(ctx, BoolType.getInstance());
	}

	@Override
	public void exitStringType(MningParser.StringTypeContext ctx) {
		property.putType(ctx, StringType.getInstance());
	}

	@Override
	public void exitVoidType(MningParser.VoidTypeContext ctx) {
		property.putType(ctx, VoidType.getInstance());
	}

	@Override
	public void exitClassType(MningParser.ClassTypeContext ctx) {
		ListIterator<TerminalNode> idIter = ctx.ID().listIterator();
		ClassType tmpClass = environment.fetchClass(idIter.next().getText());
		CompilerError.check(tmpClass != null, ErrorType.UnknownType);
		while (idIter.hasNext()) {
			tmpClass = tmpClass.fetchClass(idIter.next().getText());
			CompilerError.check(tmpClass != null, ErrorType.UnknownType);
		}
		property.putType(ctx, tmpClass);
	}

	// Method checker

	private List<String> paraNameList = null;
	private List<Type> paraTypeList = null;

	@Override
	public void enterFunctionDeclaration(MningParser.FunctionDeclarationContext ctx) {
		paraNameList = new ArrayList<>();
		paraTypeList = new ArrayList<>();
	}

	@Override
	public void exitParameter(MningParser.ParameterContext ctx) {
		paraNameList.add(ctx.ID().getText());
		paraTypeList.add(property.fetchType(ctx.typeSpecifier()));
	}

	@Override
	public void exitFunctionDeclaration(MningParser.FunctionDeclarationContext ctx) {
		Type returnType = property.fetchType(ctx.typeSpecifier());
		String name = ctx.ID().getText();
		Method method = environment.makeMethod(returnType, paraNameList, paraTypeList);
		environment.putMethod(name, method);
		property.putMethod(ctx, method);
		paraNameList = null;
		paraTypeList = null;
	}

	// Field checker

	@Override
	public void exitClassFieldDeclaration(MningParser.ClassFieldDeclarationContext ctx) {
		Type type = property.fetchType(ctx.typeSpecifier());
		for (MningParser.VarInitContext context : ctx.varInit()) {
			FieldValue value = environment.makeField(type);
			environment.putField(context.ID().getText(), value);
			property.putField(context, value);
		}
	}
}
