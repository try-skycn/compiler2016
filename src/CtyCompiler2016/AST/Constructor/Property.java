package CtyCompiler2016.AST.Constructor;

import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Constituent.Method.Method;
import CtyCompiler2016.AST.Constituent.Statement.Statement;
import CtyCompiler2016.AST.Table.FieldTable.FieldValue.FieldValue;
import CtyCompiler2016.AST.Type.ClassType;
import CtyCompiler2016.AST.Type.Type;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

import java.util.List;

/**
 * Created by SkytimChen on 3/31/16.
 */
public class Property {
	private final ParseTreeProperty<ClassType> classTable = new ParseTreeProperty<>();
	private final ParseTreeProperty<Type> typeTable = new ParseTreeProperty<>();
	private final ParseTreeProperty<Method> methodTable = new ParseTreeProperty<>();
	private final ParseTreeProperty<Expression> exprTable = new ParseTreeProperty<>();
	private final ParseTreeProperty<List<Expression>> exprListTable = new ParseTreeProperty<>();
	private final ParseTreeProperty<Statement> stmtTable = new ParseTreeProperty<>();
	private final ParseTreeProperty<FieldValue> fieldTable = new ParseTreeProperty<>();

	void putClass(ParseTree node, ClassType classType) {
		classTable.put(node, classType);
	}

	ClassType fetchClass(ParseTree node) {
		return classTable.get(node);
	}

	void putType(ParseTree node, Type type) {
		typeTable.put(node, type);
	}

	Type fetchType(ParseTree node) {
		return typeTable.get(node);
	}

	void putMethod(ParseTree node, Method method) {
		methodTable.put(node, method);
	}

	Method fetchMethod(ParseTree node) {
		return methodTable.get(node);
	}

	void putExpr(ParseTree node, Expression expr) {
		exprTable.put(node, expr);
	}

	Expression fetchExpr(ParseTree node) {
		return exprTable.get(node);
	}

	void putExprList(ParseTree node, List<Expression> exprList) {
		exprListTable.put(node, exprList);
	}

	List<Expression> fetchExprList(ParseTree node) {
		return exprListTable.get(node);
	}

	void putStmt(ParseTree node, Statement stmt) {
		stmtTable.put(node, stmt);
	}

	Statement fetchStmt(ParseTree node) {
		return stmtTable.get(node);
	}

	void putField(ParseTree node, FieldValue value) {
		fieldTable.put(node, value);
	}

	FieldValue fetchField(ParseTree node) {
		return fieldTable.get(node);
	}
}
