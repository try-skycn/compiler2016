package CtyCompiler2016.AST.Constructor;

import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Constituent.Expression.Field.FrameFieldExpression;
import CtyCompiler2016.AST.Constituent.Frame.FrameFieldMaker;
import CtyCompiler2016.AST.Constituent.Frame.FramePool;
import CtyCompiler2016.AST.Constituent.Method.MemberMethod;
import CtyCompiler2016.AST.Constituent.Method.Method;
import CtyCompiler2016.AST.Constituent.Statement.Iterator.IterationStatement;
import CtyCompiler2016.AST.Table.ClassTable.ClassScope;
import CtyCompiler2016.AST.Table.FieldTable.FieldMaker;
import CtyCompiler2016.AST.Table.FieldTable.FieldScope;
import CtyCompiler2016.AST.Table.FieldTable.FieldValue.FieldValue;
import CtyCompiler2016.AST.Table.MethodTable.MethodMaker;
import CtyCompiler2016.AST.Table.MethodTable.MethodScope;
import CtyCompiler2016.AST.Type.ClassType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.Printer.AstPrinter;

import java.util.*;

/**
 * Created by SkytimChen on 3/31/16.
 */
public class Environment implements ClassScope, FieldMaker, FieldScope, MethodMaker, MethodScope {
	private final ClassType globalClass;

	Environment() {
		Expression.resetStatic();
		Method.resetStatic();
		Type.resetStatic();
		this.globalClass = ClassType.globalClass;
		enterClass(globalClass);
	}

	public ClassType getGlobalClass() {
		return globalClass;
	}

	// For ASTInterface

	public void printAST(AstPrinter printer) {
		globalClass.printAST(printer, 0);
	}

	// For Class, ClassScope

	private final Stack<ClassType> classStack = new Stack<>();

	void enterClass(ClassType classType) {
		classStack.push(classType);
		enterFieldMaker(classType);
		enterFieldScope(classType);
		enterMethodMaker(classType);
		enterMethodScope(classType);
	}

	ClassType fetchCurrentClass() {
		return classStack.peek();
	}

	FrameFieldExpression fetchThisPointer() {
		FrameFieldExpression thisPointer = null;
		Method method = fetchCurrentMethod();
		if (method == null) {
			thisPointer = fetchCurrentClass().getThisPointer();
		} else if (method instanceof MemberMethod) {
			thisPointer = ((MemberMethod) method).thisPointer;
		}
		CompilerError.check(thisPointer != null, ErrorType.UnknownScope);
		return thisPointer;
	}

	void exitClass() {
		exitMethodScope();
		exitMethodMaker();
		exitFieldScope();
		exitFieldMaker();
		classStack.pop();
	}

	@Override
	public void putClass(String key, ClassType value) {
		fetchCurrentClass().putClass(key, value);
	}

	@Override
	public ClassType fetchClass(String key) {
		ClassType classType = fetchCurrentClass().fetchClass(key);
		if (classType != null) return classType;
		classType = globalClass.fetchClass(key);
		if (classType != null) return classType;
		CompilerError.errorHandle(ErrorType.UnknownType);
		return null;
	}

	ClassType defineClass(String name) {
		ClassType classType = ClassType.define(name, fetchCurrentClass());
		putClass(name, classType);
		return classType;
	}

	// For Field, FieldMaker, FieldScope

	private final Stack<FieldMaker> fieldMakerStack = new Stack<>();
	private final Stack<FieldScope> fieldScopeStack = new Stack<>();
	private final Map<String, Stack<FieldValue>> fieldMap = new HashMap<>();

	private void pushField(String key, FieldValue value) {
		if (!fieldMap.containsKey(key)) {
			fieldMap.put(key, new Stack<>());
		}
		fieldMap.get(key).push(value);
	}

	private void popField(String key) {
		fieldMap.get(key).pop();
	}

	void enterFieldMaker(FieldMaker fieldMaker) {
		fieldMakerStack.push(fieldMaker);
	}

	FieldMaker fetchFieldMaker() {
		return fieldMakerStack.peek();
	}

	void exitFieldMaker() {
		fieldMakerStack.pop();
	}

	FrameFieldMaker makeFrameFieldMaker() {
		FieldMaker fieldMaker = fetchFieldMaker();
		if (fieldMaker instanceof FrameFieldMaker) {
			return FramePool.make((FrameFieldMaker) fieldMaker);
		} else {
			CompilerError.errorHandle(ErrorType.UnknownError);
			return null;
		}
	}

	void enterFieldScope(FieldScope fieldScope) {
		for (Map.Entry<String, FieldValue> entry : fieldScope.fieldEntrySet()) {
			pushField(entry.getKey(), entry.getValue());
		}
		fieldScopeStack.push(fieldScope);
	}

	FieldScope fetchFieldScope() {
		return fieldScopeStack.peek();
	}

	void exitFieldScope() {
		FieldScope fieldScope = fieldScopeStack.pop();
		for (Map.Entry<String, FieldValue> entry : fieldScope.fieldEntrySet()) {
			popField(entry.getKey());
		}
	}

	@Override
	public FieldValue makeField(Type type) {
		return fetchFieldMaker().makeField(type);
	}

	@Override
	public Type getFieldType(int index) {
		return null;
	}

	@Override
	public void putField(String key, FieldValue value) {
		fetchFieldScope().putField(key, value);
		pushField(key, value);
	}

	@Override
	public FieldValue fetchField(String key) {
		Stack<FieldValue> stack = fieldMap.get(key);
		CompilerError.check(stack != null && !stack.isEmpty(), ErrorType.UnknownExpression);
		return stack.peek();
	}

	@Override
	public Set<Map.Entry<String, FieldValue>> fieldEntrySet() {
		return null;
	}

	// For Method, MethodMaker, MethodScope

	private final Stack<MethodMaker> methodMakerStack = new Stack<>();
	private final Stack<MethodScope> methodScopeStack = new Stack<>();

	void enterMethodMaker(MethodMaker methodMaker) {
		methodMakerStack.push(methodMaker);
	}

	MethodMaker fetchMethodMaker() {
		return methodMakerStack.peek();
	}

	void exitMethodMaker() {
		methodMakerStack.pop();
	}

	void enterMethodScope(MethodScope methodScope) {
		methodScopeStack.push(methodScope);
	}

	MethodScope fetchMethodScope() {
		return methodScopeStack.peek();
	}

	void exitMethodScope() {
		methodScopeStack.pop();
	}

	@Override
	public Method makeMethod(Type returnType, List<String> nameList, List<Type> typeList) {
		return fetchMethodMaker().makeMethod(returnType, nameList, typeList);
	}

	@Override
	public void putMethod(String key, Method value) {
		fetchMethodScope().putMethod(key, value);
	}

	@Override
	public Method fetchMethod(String key, List<Type> typeList) {
		Method method = fetchMethodScope().fetchMethod(key, typeList);
		if (method != null) return method;
		method = globalClass.fetchMethod(key, typeList);
		if (method != null) return method;
		CompilerError.errorHandle(ErrorType.UnknownMethod);
		return null;
	}

	// For Method

	private Method currentMethod = null;

	void enterMethod(Method method) {
		currentMethod = method;
		enterFieldMaker(method);
		enterFieldScope(method);
	}

	Method fetchCurrentMethod() {
		return currentMethod;
	}

	void exitMethod() {
		exitFieldScope();
		exitFieldMaker();
		currentMethod = null;
	}

	// For Statement

	private Stack<IterationStatement> loopStack = new Stack<>();

	void enterLoop(IterationStatement loop) {
		loopStack.push(loop);
	}

	IterationStatement fetchCurrentLoop() {
		return loopStack.isEmpty() ? null : loopStack.peek();
	}

	void exitLoop() {
		loopStack.pop();
	}
}
