package CtyCompiler2016.AST.Constructor;

import CtyCompiler2016.AST.Type.ClassType;
import CtyCompiler2016.Parser.MningBaseListener;
import CtyCompiler2016.Parser.MningParser;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

/**
 * Created by SkytimChen on 3/24/16.
 */
class TableConstructor extends MningBaseListener {
	private final Environment environment;
	private final Property property;

	static void construct(ParseTree tree, ParseTreeWalker walker, Environment environment, Property property) {
		TableConstructor constructor = new TableConstructor(environment, property);
		walker.walk(constructor, tree);
	}

	private TableConstructor(Environment environment, Property property) {
		this.environment = environment;
		this.property = property;
	}

	@Override
	public void enterClassDeclaration(MningParser.ClassDeclarationContext ctx) {
		String name = ctx.ID().getText();
		ClassType classType = environment.defineClass(name);
		property.putClass(ctx, classType);
		environment.enterClass(classType);
	}

	@Override
	public void exitClassDeclaration(MningParser.ClassDeclarationContext ctx) {
		environment.exitClass();
	}
}
