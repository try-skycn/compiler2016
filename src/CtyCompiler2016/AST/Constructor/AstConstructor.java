package CtyCompiler2016.AST.Constructor;

import CtyCompiler2016.AST.Constituent.Expression.Binary.BinaryExpression;
import CtyCompiler2016.AST.Constituent.Expression.Constant.BoolConstantExpression;
import CtyCompiler2016.AST.Constituent.Expression.Constant.IntConstantExpression;
import CtyCompiler2016.AST.Constituent.Expression.Constant.NullConstantExpression;
import CtyCompiler2016.AST.Constituent.Expression.Constant.StringConstanceExpression;
import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Constituent.Expression.Field.*;
import CtyCompiler2016.AST.Constituent.Expression.Special.MethodCallExpression;
import CtyCompiler2016.AST.Constituent.Expression.Special.New.NewArrayExpression;
import CtyCompiler2016.AST.Constituent.Expression.Special.New.PlainNewExpression;
import CtyCompiler2016.AST.Constituent.Expression.Unary.PostfixExpression;
import CtyCompiler2016.AST.Constituent.Expression.Unary.PrefixExpression;
import CtyCompiler2016.AST.Constituent.Frame.FrameFieldMaker;
import CtyCompiler2016.AST.Constituent.Method.Method;
import CtyCompiler2016.AST.Constituent.Method.StaticMethod;
import CtyCompiler2016.AST.Constituent.Statement.CompoundStatement;
import CtyCompiler2016.AST.Constituent.Statement.Iterator.ForStatement;
import CtyCompiler2016.AST.Constituent.Statement.Iterator.IterationStatement;
import CtyCompiler2016.AST.Constituent.Statement.Jump.BreakStatement;
import CtyCompiler2016.AST.Constituent.Statement.Jump.ContinueStatement;
import CtyCompiler2016.AST.Constituent.Statement.Jump.ReturnStatement;
import CtyCompiler2016.AST.Constituent.Statement.SelectStatement;
import CtyCompiler2016.AST.Constituent.Statement.Statement;
import CtyCompiler2016.AST.Constituent.Statement.SuperStatement;
import CtyCompiler2016.AST.Table.FieldTable.FieldManager;
import CtyCompiler2016.AST.Table.FieldTable.FieldValue.ClassFieldValue;
import CtyCompiler2016.AST.Table.FieldTable.FieldValue.FieldValue;
import CtyCompiler2016.AST.Table.FieldTable.FieldValue.StaticFieldValue;
import CtyCompiler2016.AST.Table.MethodTable.MethodScope;
import CtyCompiler2016.AST.Type.ArrayType;
import CtyCompiler2016.AST.Type.ClassType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.Parser.MningBaseListener;
import CtyCompiler2016.Parser.MningParser;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by SkytimChen on 3/29/16.
 */
class AstConstructor extends MningBaseListener {
	private final Environment environment;
	private final Property property;

	static void construct(ParseTree tree, ParseTreeWalker walker, Environment environment, Property property) {
		AstConstructor constructor = new AstConstructor(environment, property);
		walker.walk(constructor, tree);
	}

	private AstConstructor(Environment environment, Property property) {
		this.environment = environment;
		this.property = property;
	}

	@Override
	public void enterClassDeclaration(MningParser.ClassDeclarationContext ctx) {
		environment.enterClass(property.fetchClass(ctx));
	}

	@Override
	public void exitClassDeclaration(MningParser.ClassDeclarationContext ctx) {
		environment.exitClass();
		property.fetchClass(ctx).substantiateConstructor();
	}

	@Override
	public void enterFunctionDeclaration(MningParser.FunctionDeclarationContext ctx) {
		environment.enterMethod(property.fetchMethod(ctx));
	}

	@Override
	public void exitFunctionDeclaration(MningParser.FunctionDeclarationContext ctx) {
		Statement stmt = property.fetchStmt(ctx.compoundStatement());
		if (stmt instanceof CompoundStatement) {
			property.fetchMethod(ctx).setMethodBody(((CompoundStatement) stmt));
		} else {
			CompilerError.errorHandle(ErrorType.UnknownError);
		}
		environment.exitMethod();
	}

	@Override
	public void exitClassFieldDeclaration(MningParser.ClassFieldDeclarationContext ctx) {
		for (MningParser.VarInitContext context : ctx.varInit()) {
			if (context.expression() != null) {
				FieldValue value = property.fetchField(context);
				Expression rightExpression = property.fetchExpr(context.expression());
				FieldExpression leftExpression = null;
				if (value instanceof ClassFieldValue) {
					leftExpression = ComponentExpression.make(environment.fetchThisPointer(), ((ClassFieldValue) value));
				} else if (value instanceof StaticFieldValue) {
					if (value instanceof StaticFieldExpression) {
						leftExpression = ((StaticFieldExpression) value);
					} else {
						CompilerError.errorHandle(ErrorType.UnknownError);
					}
				} else {
					CompilerError.errorHandle(ErrorType.UnknownError);
				}
				environment.fetchCurrentClass().addFieldInit(BinaryExpression.make(leftExpression, "=", rightExpression));
			}
		}
	}

	// For Constant

	@Override
	public void exitNullConstant(MningParser.NullConstantContext ctx) {
		property.putExpr(ctx, NullConstantExpression.make());
	}

	@Override
	public void exitIntConstant(MningParser.IntConstantContext ctx) {
		property.putExpr(ctx, IntConstantExpression.make(ctx.NUMBER_LITERAL().getText()));
	}

	@Override
	public void exitStringConstant(MningParser.StringConstantContext ctx) {
		property.putExpr(ctx, StringConstanceExpression.make(ctx.STRING_LITERAL().getText()));
	}

	@Override
	public void exitBoolConstant(MningParser.BoolConstantContext ctx) {
		property.putExpr(ctx, BoolConstantExpression.make(ctx.boolLiteral().getText()));
	}

	@Override
	public void exitConstantExpr(MningParser.ConstantExprContext ctx) {
		property.putExpr(ctx, property.fetchExpr(ctx.constant()));
	}

	// For ID Expression

	@Override
	public void exitIdExpr(MningParser.IdExprContext ctx) {
		if (!(ctx.getParent() instanceof MningParser.MethodCallExprContext)) {
			String key = ctx.ID().getText();
			FieldValue value = environment.fetchField(key);
			if (value instanceof FieldExpression) {
				property.putExpr(ctx, ((FieldExpression) value));
			} else if (value instanceof ClassFieldValue) {
				FrameFieldExpression thisPointer = environment.fetchThisPointer();
				property.putExpr(ctx, ComponentExpression.make(thisPointer, ((ClassFieldValue) value)));
			} else {
				CompilerError.errorHandle(ErrorType.UnknownError);
			}
		}
	}

	@Override
	public void exitThisExpr(MningParser.ThisExprContext ctx) {
		property.putExpr(ctx, environment.fetchThisPointer());
	}

	@Override
	public void exitComponentExpr(MningParser.ComponentExprContext ctx) {
		if (!(ctx.getParent() instanceof MningParser.MethodCallExprContext)) {
			Expression thisPointer = property.fetchExpr(ctx.expression());
			Type type = thisPointer.getType();
			if (type instanceof ClassType) {
				ClassType classType = ((ClassType) type);
				FieldValue value = classType.fetchField(ctx.ID().getText());
				if (value instanceof ClassFieldValue) {
					property.putExpr(ctx, ComponentExpression.make(thisPointer, ((ClassFieldValue) value)));
				} else {
					CompilerError.errorHandle(ErrorType.UnknownComponent);
				}
			} else {
				CompilerError.errorHandle(ErrorType.UnknownComponent);
			}
		}
	}

	// MethodCall

	@Override
	public void exitMethodCallExpr(MningParser.MethodCallExprContext ctx) {
		List<Expression> paraList;
		if (ctx.expressionList() != null) {
			paraList =property.fetchExprList(ctx.expressionList());
		} else {
			paraList = new LinkedList<>();
		}
		List<Type> paraTypeList = new ArrayList<>();
		for (Expression expr : paraList) {
			paraTypeList.add(expr.getType());
		}

		MningParser.ExpressionContext methodNode = ctx.expression();
		if (methodNode instanceof MningParser.IdExprContext) {
			Method method = environment.fetchMethod(((MningParser.IdExprContext) methodNode).ID().getText(), paraTypeList);
			if (method instanceof StaticMethod) {
				property.putExpr(ctx, MethodCallExpression.make(method, paraList));
			} else {
				paraList.add(0, environment.fetchThisPointer());
				property.putExpr(ctx, MethodCallExpression.make(method, paraList));
			}
		} else if (methodNode instanceof MningParser.ComponentExprContext) {
			Expression expr = property.fetchExpr(((MningParser.ComponentExprContext) methodNode).expression());
			Type type = expr.getType();
			if (type instanceof MethodScope) {
				Method method = ((MethodScope) type).fetchMethod(((MningParser.ComponentExprContext) methodNode).ID().getText(), paraTypeList);
				CompilerError.check(method != null, ErrorType.UnknownMethod);
				paraList.add(0, expr);
				property.putExpr(ctx, MethodCallExpression.make(method, paraList));
			} else {
				CompilerError.errorHandle(ErrorType.UnknownExpression);
			}
		} else {
			CompilerError.errorHandle(ErrorType.UnknownExpression);
		}
	}

	// For ExpressionList

	@Override
	public void exitExpressionList(MningParser.ExpressionListContext ctx) {
		List<Expression> exprList = new LinkedList<>();
		for (MningParser.ExpressionContext context : ctx.expression()) {
			exprList.add(property.fetchExpr(context));
		}
		property.putExprList(ctx, exprList);
	}

	@Override
	public void exitInitExpression(MningParser.InitExpressionContext ctx) {
		Type type = property.fetchType(ctx.typeSpecifier());
		List<Expression> exprList = new LinkedList<>();
		for (MningParser.VarInitContext context : ctx.varInit()) {
			FieldValue value = environment.makeField(type);
			environment.putField(context.ID().getText(), value);
			if (value instanceof FrameFieldExpression) {
				if (context.expression() != null) {
					exprList.add(BinaryExpression.make(((FrameFieldExpression) value), "=", property.fetchExpr(context.expression())));
				}
			} else {
				CompilerError.errorHandle(ErrorType.UnknownError);
			}
		}
		property.putExprList(ctx, exprList);
	}

	// For Final Statement

	@Override
	public void exitFinalCompoundStatement(MningParser.FinalCompoundStatementContext ctx) {
		property.putStmt(ctx, property.fetchStmt(ctx.compoundStatement()));
	}

	@Override
	public void exitFinalForStatement(MningParser.FinalForStatementContext ctx) {
		property.putStmt(ctx, property.fetchStmt(ctx.forStatement()));
	}

	@Override
	public void exitFinalJumpStatement(MningParser.FinalJumpStatementContext ctx) {
		property.putStmt(ctx, property.fetchStmt(ctx.jumpStatement()));
	}

	@Override
	public void exitFinalSelectStatement(MningParser.FinalSelectStatementContext ctx) {
		property.putStmt(ctx, property.fetchStmt(ctx.selectStatement()));
	}

	@Override
	public void exitFinalSuperStatement(MningParser.FinalSuperStatementContext ctx) {
		property.putStmt(ctx, property.fetchStmt(ctx.superStatement()));
	}

	@Override
	public void exitFinalWhileStatement(MningParser.FinalWhileStatementContext ctx) {
		property.putStmt(ctx, property.fetchStmt(ctx.whileStatement()));
	}

	// For Statement

	@Override
	public void enterBodyStatement(MningParser.BodyStatementContext ctx) {
		environment.enterFieldScope(new FieldManager());
	}

	@Override
	public void exitBodyStatement(MningParser.BodyStatementContext ctx) {
		property.putStmt(ctx, property.fetchStmt(ctx.statement()));
		environment.exitFieldScope();
	}

	@Override
	public void exitSuperExpressionList(MningParser.SuperExpressionListContext ctx) {
		property.putStmt(ctx, SuperStatement.make(property.fetchExprList(ctx.expressionList())));
	}

	@Override
	public void exitSuperInitExpression(MningParser.SuperInitExpressionContext ctx) {
		property.putStmt(ctx, SuperStatement.make(property.fetchExprList(ctx.initExpression())));
	}

	@Override
	public void exitSuperEmpty(MningParser.SuperEmptyContext ctx) {
		property.putStmt(ctx, SuperStatement.make());
	}

	@Override
	public void enterCompoundStatement(MningParser.CompoundStatementContext ctx) {
		FrameFieldMaker frameFieldMaker = environment.makeFrameFieldMaker();
		environment.enterFieldMaker(frameFieldMaker);
		if (!(ctx.getParent() instanceof MningParser.FunctionDeclarationContext) && !(ctx.getParent() instanceof MningParser.BodyStatementContext)) {
			environment.enterFieldScope(new FieldManager());
		}
		property.putStmt(ctx, CompoundStatement.define(frameFieldMaker));
	}

	@Override
	public void exitCompoundStatement(MningParser.CompoundStatementContext ctx) {
		List<Statement> stmtList = new LinkedList<>();
		for (MningParser.StatementContext context : ctx.statement()) {
			stmtList.add(property.fetchStmt(context));
		}
		Statement currentStatement = property.fetchStmt(ctx);
		if (currentStatement instanceof CompoundStatement) {
			((CompoundStatement) currentStatement).make(stmtList);
		} else {
			CompilerError.errorHandle(ErrorType.UnknownType);
		}
		if (!(ctx.getParent() instanceof MningParser.FunctionDeclarationContext) && !(ctx.getParent() instanceof MningParser.BodyStatementContext)) {
			environment.exitFieldScope();
		}
		environment.exitFieldMaker();
	}

	@Override
	public void exitSelectStatement(MningParser.SelectStatementContext ctx) {
		Expression condition = property.fetchExpr(ctx.expression());
		Statement thenStmt = property.fetchStmt(ctx.bodyStatement(0));
		Statement elseStmt = null;
		if (ctx.bodyStatement().size() > 1) {
			elseStmt = property.fetchStmt(ctx.bodyStatement(1));
		}
		property.putStmt(ctx, SelectStatement.make(condition, thenStmt, elseStmt));
	}

	@Override
	public void enterForStatement(MningParser.ForStatementContext ctx) {
		ForStatement forStatement = ForStatement.define();
		property.putStmt(ctx, forStatement);
		environment.enterLoop(forStatement);
		environment.enterFieldScope(new FieldManager());
	}

	@Override
	public void exitForStatement(MningParser.ForStatementContext ctx) {
		Statement preStmt = property.fetchStmt(ctx.superStatement());
		Expression condition;
		List<Expression> postStmt;
		if (ctx.expression() != null) {
			condition = property.fetchExpr(ctx.expression());
		} else {
			condition = BoolConstantExpression.make(true);
		}
		if (ctx.expressionList() != null) {
			postStmt = property.fetchExprList(ctx.expressionList());
		} else {
			postStmt = new ArrayList<>();
		}
		Statement body = property.fetchStmt(ctx.bodyStatement());
		Statement stmt = property.fetchStmt(ctx);
		if (stmt instanceof ForStatement && preStmt instanceof SuperStatement) {
			((ForStatement) stmt).make(((SuperStatement) preStmt), condition, SuperStatement.make(postStmt), body);
		} else {
			CompilerError.errorHandle(ErrorType.UnknownError);
		}
		environment.exitFieldScope();
		environment.exitLoop();
	}

	@Override
	public void enterWhileStatement(MningParser.WhileStatementContext ctx) {
		IterationStatement iterationStatement = IterationStatement.define();
		property.putStmt(ctx, iterationStatement);
		environment.enterLoop(iterationStatement);
	}

	@Override
	public void exitWhileStatement(MningParser.WhileStatementContext ctx) {
		Expression condition = property.fetchExpr(ctx.expression());
		Statement body = property.fetchStmt(ctx.bodyStatement());
		Statement stmt = property.fetchStmt(ctx);
		if (stmt instanceof IterationStatement) {
			((IterationStatement) stmt).make(condition, body);
		} else {
			CompilerError.errorHandle(ErrorType.UnknownError);
		}
	}

	@Override
	public void exitContinueStatement(MningParser.ContinueStatementContext ctx) {
		IterationStatement currentLoop = environment.fetchCurrentLoop();
		CompilerError.check(currentLoop != null, ErrorType.UnknownStatement);
		property.putStmt(ctx, ContinueStatement.make(currentLoop));
	}

	@Override
	public void exitBreakStatement(MningParser.BreakStatementContext ctx) {
		IterationStatement currentLoop = environment.fetchCurrentLoop();
		CompilerError.check(currentLoop != null, ErrorType.UnknownStatement);
		property.putStmt(ctx, BreakStatement.make(currentLoop));
	}

	@Override
	public void exitReturnStatement(MningParser.ReturnStatementContext ctx) {
		Method currentMethod = environment.fetchCurrentMethod();
		if (ctx.expression() == null) {
			property.putStmt(ctx, ReturnStatement.make(currentMethod));
		} else {
			Expression returnValue = property.fetchExpr(ctx.expression());
			property.putStmt(ctx, ReturnStatement.make(currentMethod, returnValue));
		}
	}

	// For BinaryExpression

	@Override
	public void exitMultiExpr(MningParser.MultiExprContext ctx) {
		property.putExpr(ctx, BinaryExpression.make(property.fetchExpr(ctx.expression(0)), ctx.oper.getText(), property.fetchExpr(ctx.expression(1))));
	}

	@Override
	public void exitAddExpr(MningParser.AddExprContext ctx) {
		property.putExpr(ctx, BinaryExpression.make(property.fetchExpr(ctx.expression(0)), ctx.oper.getText(), property.fetchExpr(ctx.expression(1))));
	}

	@Override
	public void exitShiftExpr(MningParser.ShiftExprContext ctx) {
		property.putExpr(ctx, BinaryExpression.make(property.fetchExpr(ctx.expression(0)), ctx.oper.getText(), property.fetchExpr(ctx.expression(1))));
	}

	@Override
	public void exitCompareExpr(MningParser.CompareExprContext ctx) {
		property.putExpr(ctx, BinaryExpression.make(property.fetchExpr(ctx.expression(0)), ctx.oper.getText(), property.fetchExpr(ctx.expression(1))));
	}

	@Override
	public void exitEquivExpr(MningParser.EquivExprContext ctx) {
		property.putExpr(ctx, BinaryExpression.make(property.fetchExpr(ctx.expression(0)), ctx.oper.getText(), property.fetchExpr(ctx.expression(1))));
	}

	@Override
	public void exitBitwiseAndExpr(MningParser.BitwiseAndExprContext ctx) {
		property.putExpr(ctx, BinaryExpression.make(property.fetchExpr(ctx.expression(0)), ctx.oper.getText(), property.fetchExpr(ctx.expression(1))));
	}

	@Override
	public void exitBitwiseXorExor(MningParser.BitwiseXorExorContext ctx) {
		property.putExpr(ctx, BinaryExpression.make(property.fetchExpr(ctx.expression(0)), ctx.oper.getText(), property.fetchExpr(ctx.expression(1))));
	}

	@Override
	public void exitBiteiseOrExpr(MningParser.BiteiseOrExprContext ctx) {
		property.putExpr(ctx, BinaryExpression.make(property.fetchExpr(ctx.expression(0)), ctx.oper.getText(), property.fetchExpr(ctx.expression(1))));
	}

	@Override
	public void exitAndExpr(MningParser.AndExprContext ctx) {
		property.putExpr(ctx, BinaryExpression.make(property.fetchExpr(ctx.expression(0)), ctx.oper.getText(), property.fetchExpr(ctx.expression(1))));
	}

	@Override
	public void exitOrExor(MningParser.OrExorContext ctx) {
		property.putExpr(ctx, BinaryExpression.make(property.fetchExpr(ctx.expression(0)), ctx.oper.getText(), property.fetchExpr(ctx.expression(1))));
	}

	@Override
	public void exitAssignExpr(MningParser.AssignExprContext ctx) {
		property.putExpr(ctx, BinaryExpression.make(property.fetchExpr(ctx.expression(0)), ctx.oper.getText(), property.fetchExpr(ctx.expression(1))));
	}

	// For UnaryExpression

	@Override
	public void exitPostfixExpr(MningParser.PostfixExprContext ctx) {
		property.putExpr(ctx, PostfixExpression.make(ctx.oper.getText(), property.fetchExpr(ctx.expression())));
	}

	@Override
	public void exitPrefixExpr(MningParser.PrefixExprContext ctx) {
		property.putExpr(ctx, PrefixExpression.make(ctx.oper.getText(), property.fetchExpr(ctx.expression())));
	}

	// For SpecialExpression

	@Override
	public void exitParenExpr(MningParser.ParenExprContext ctx) {
		property.putExpr(ctx, property.fetchExpr(ctx.expression()));
	}

	@Override
	public void exitPlainNewExpr(MningParser.PlainNewExprContext ctx) {
		property.putExpr(ctx, PlainNewExpression.make(property.fetchType(ctx.classType())));
	}

	@Override
	public void exitNewArrayExpr(MningParser.NewArrayExprContext ctx) {
		Type baseType = property.fetchType(ctx.typeSpecifier());
		for (int i = 0; i < ctx.dimSquares.size(); i++) {
			baseType = ArrayType.getInstance(baseType);
		}
		property.putExpr(ctx, NewArrayExpression.make(baseType, property.fetchExpr(ctx.expression())));
	}

	@Override
	public void exitSubscriptExpr(MningParser.SubscriptExprContext ctx) {
		property.putExpr(ctx, SubscriptExpression.make(property.fetchExpr(ctx.expression(0)), property.fetchExpr(ctx.expression(1))));
	}
}
