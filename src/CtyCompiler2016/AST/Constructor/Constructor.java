package CtyCompiler2016.AST.Constructor;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

/**
 * Created by SkytimChen on 3/30/16.
 */
public class Constructor {
	public static Environment construct(ParseTree tree) {
		ParseTreeWalker walker = new ParseTreeWalker();
		Environment environment = new Environment();
		Property property = new Property();
		TableConstructor.construct(tree, walker, environment, property);
		MemberConstructor.construct(tree, walker, environment, property);
		AstConstructor.construct(tree, walker, environment, property);
		environment.getGlobalClass().substantiateConstructor();
		return environment;
	}
}
