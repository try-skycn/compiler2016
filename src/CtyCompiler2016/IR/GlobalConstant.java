package CtyCompiler2016.IR;

import CtyCompiler2016.IR.Register.VirtualRegister.SpaceRegister;
import CtyCompiler2016.IR.Register.VirtualRegister.StringRegister;
import CtyCompiler2016.Tool.StringTransform;

/**
 * Created by SkytimChen on 4/27/16.
 */
public class GlobalConstant {
	private final StringRegister newline;
	private final SpaceRegister stringBuffer;

	private static final int stringBufferSize = 1 << 10;

	public GlobalConstant(IRStructure structure) {
		newline = structure.makeStaticString(StringTransform.withEscape("\n"));
		stringBuffer = structure.makeStaticSpace(stringBufferSize);
	}

	public SpaceRegister getStringBuffer() {
		return stringBuffer;
	}

	public StringRegister getNewline() {
		return newline;
	}
}
