package CtyCompiler2016.IR;

import CtyCompiler2016.AST.Constituent.Expression.Expression;
import CtyCompiler2016.AST.Constituent.Method.Method;
import CtyCompiler2016.AST.Type.ClassType;
import CtyCompiler2016.IR.MethodStructure.MethodStructure;
import CtyCompiler2016.IR.Register.VirtualRegister.*;
import CtyCompiler2016.Mips.MipsAddr.MipsLabel;
import CtyCompiler2016.Mips.MipsComment;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Printer.IRPrinter;
import CtyCompiler2016.Tool.UIDManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by SkytimChen on 4/17/16.
 */
public class IRStructure {
	private final Map<String, StaticRegister> stringAddress = new HashMap<>();

	public StaticRegister putString(String info) {
		if (!stringAddress.containsKey(info)) {
			stringAddress.put(info, makeStaticString(info));
		}
		return stringAddress.get(info);
	}

	private final UIDManager<StaticRegister> staticRegManager = new UIDManager<>();

	public SpaceRegister makeStaticSpace(int size) {
		return new SpaceRegister(staticRegManager, size);
	}

	public StringRegister makeStaticString(String initVal) {
		return new StringRegister(staticRegManager, initVal);
	}

	public WordRegister makeStaticWord(int initVal) {
		return new WordRegister(staticRegManager, initVal);
	}

	private int entryUID;
	private Collection<MethodStructure> methodIRs;

	private IRStructure() {
		VirtualRegister.resetStaticIR();
		Expression.resetStaticIR(this);
		globalConstant = new GlobalConstant(this);
	}

	public static IRStructure make(ClassType globalClass) {
		IRStructure structure = new IRStructure();
		structure.construct(globalClass);
		return structure;
	}

	private void construct(ClassType globalClass) {
		methodIRs = new ArrayList<>();
		for (Method method : Method.getMethodList()) {
			MethodStructure structure = method.makeIR(this);
			if (structure != null) {
				methodIRs.add(structure);
			}
		}
		entryUID = globalClass.getDefaultConstructor().uID;
	}

	public int getEntryUID() {
		return entryUID;
	}

	// For GlobalConstant

	private final GlobalConstant globalConstant;

	public GlobalConstant getGlobalConstant() {
		return globalConstant;
	}

	// For IRPrinter

	public void printIR(IRPrinter printer) {
		for (StaticRegister register : staticRegManager.getList()) {
			register.printIR(printer);
		}
		printer.println();
		for (MethodStructure methodIR : methodIRs) {
			methodIR.printIR(printer);
			printer.println();
		}
		printer.println("Entry: method #" + entryUID);
	}

	// For NaiveMips

	public void printDataSegment(MipsFile file) {
		for (StaticRegister staticRegister : staticRegManager.getList()) {
			staticRegister.initStaticLabel(file);
			file.append(MipsComment.emptyLine);
		}
	}

	public void printNaiveMips(MipsFile file) {
		file.beginData();
		printDataSegment(file);
		file.beginText();
		for (MethodStructure method : methodIRs) {
			boolean isEntry = method.getMethodUID() == entryUID;
			if (isEntry) {
				file.append(MipsLabel.main());
			}
			method.printNaiveMips(file, globalConstant, isEntry);
			file.append(MipsComment.emptyLine);
		}
	}

	// For AdvancedMips

	public void printAdvancedMips(MipsFile file) {
		file.beginData();
		printDataSegment(file);
		file.beginText();
		for (MethodStructure method : methodIRs) {
			boolean isEntry = method.getMethodUID() == entryUID;
			if (isEntry) {
				file.append(MipsLabel.main());
			}
			method.printAdvancedMips(file, globalConstant, isEntry);
			file.append(MipsComment.emptyLine);
		}
	}
}
