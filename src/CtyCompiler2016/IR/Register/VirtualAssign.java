package CtyCompiler2016.IR.Register;

import CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction.AssignInstruction;
import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

/**
 * Created by SkytimChen on 4/15/16.
 */
public class VirtualAssign implements VirtualOperand {
	private final VirtualRegister register;
	private final AssignInstruction definition;

	private VirtualAssign(VirtualRegister register, AssignInstruction definition) {
		this.register = register;
		this.definition = definition;
	}

	public static VirtualAssign make(VirtualRegister register) {
		return new VirtualAssign(register, null);
	}

	public static VirtualAssign make(AssignInstruction instruction) {
		return new VirtualAssign(instruction.getDest(), instruction);
	}

	// Getter

	public VirtualRegister getRegister() {
		return register;
	}

	public AssignInstruction getDefinition() {
		return definition;
	}

	@Override
	public String toString() {
		return register.toString();
	}

	// For NaiveMips

	@Override
	public MipsReg toNaiveMips(MipsFile file, MipsReg dest, int spOffset) {
		loadNaiveMips(file, dest, spOffset);
		return dest;
	}

	@Override
	public void loadNaiveMips(MipsFile file, MipsReg dest, int spOffset) {
		register.loadNaiveMips(file, dest, spOffset);
	}

	// For AdvancedMips

	@Override
	public MipsReg toAdvancedMips(MipsFile file, MethodProfile profile, MipsReg suggest) {
		return register.loadAdvancedMips(file, profile, suggest);
	}

	@Override
	public MipsReg loadAdvancedMips(MipsFile file, MethodProfile profile, MipsReg suggest) {
		return register.loadAdvancedMips(file, profile, suggest);
	}
}
