package CtyCompiler2016.IR.Register;

import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsOperand.MipsOperand;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

/**
 * Created by SkytimChen on 4/15/16.
 */
public interface VirtualOperand {
	MipsOperand toNaiveMips(MipsFile file, MipsReg dest, int spOffset);
	void loadNaiveMips(MipsFile file, MipsReg dest, int spOffset);
	MipsOperand toAdvancedMips(MipsFile file, MethodProfile profile, MipsReg suggest);
	MipsReg loadAdvancedMips(MipsFile file, MethodProfile profile, MipsReg suggest);
}
