package CtyCompiler2016.IR.Register.VirtualRegister;

import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.Mips.MipsAddr.MipsAddr;
import CtyCompiler2016.Mips.MipsData.MipsDataCode;
import CtyCompiler2016.Mips.MipsData.MipsDataLine;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad.MipsLoadWord;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsStore.MipsStoreWord;
import CtyCompiler2016.Mips.MipsReg.MipsReg;
import CtyCompiler2016.Printer.IRPrinter;
import CtyCompiler2016.Tool.UIDManager;

public class WordRegister extends StaticRegister {
	private final int initVal;

	public WordRegister(UIDManager<StaticRegister> manager, int initVal) {
		super(manager);
		this.initVal = initVal;
	}

	// For IRPrinter

	@Override
	public void printIR(IRPrinter printer) {
		super.printIR(printer);
		printer.println(" " + initVal);
	}

	// For NaiveMips

	@Override
	public void loadNaiveMips(MipsFile file, MipsReg dest, int spOffset) {
		file.append(new MipsLoadWord(dest, staticLabel()));
	}

	@Override
	public void storeNaiveMips(MipsFile file, MipsReg src, int spOffset) {
		file.append(new MipsStoreWord(src, staticLabel()));
	}

	@Override
	public void initStaticLabel(MipsFile file) {
		file.append(staticLabel());
		file.append(new MipsDataLine(MipsDataCode.Word, initVal));
	}

	// For AdvancedMips

	@Override
	public boolean globalOccupied() {
		return true;
	}

	@Override
	public boolean preDefined() {
		return true;
	}

	@Override
	public boolean needReg() {
		return false;
	}

	@Override
	public void syncToMem(MipsFile file, MethodProfile profile) {
		MipsReg reg = getAllocatedReg(profile);
		if (reg != null) {
			file.append(new MipsStoreWord(reg, getAllocatedMem(profile)));
		}
	}

	@Override
	public MipsAddr getAllocatedMem(MethodProfile profile) {
		return staticLabel();
	}
}
