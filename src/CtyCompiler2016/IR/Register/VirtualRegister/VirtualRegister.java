package CtyCompiler2016.IR.Register.VirtualRegister;

import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction.MoveInstruction;
import CtyCompiler2016.IR.RegAlloc.Profile.MemoryProfile;
import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.IR.Register.ExprLeft;
import CtyCompiler2016.IR.Register.ExprOperand;
import CtyCompiler2016.Mips.MipsAddr.MipsAddr;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad.MipsLoadWord;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsMove;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsStore.MipsStoreWord;
import CtyCompiler2016.Mips.MipsReg.MipsReg;
import CtyCompiler2016.Tool.UIDManager;

public abstract class VirtualRegister implements ExprOperand, ExprLeft {
	private static UIDManager<VirtualRegister> virtualRegisterList;
	private final int uID = virtualRegisterList.add(this);

	public static void resetStaticIR() {
		virtualRegisterList = new UIDManager<>();
	}

	VirtualRegister() {
	}

	// Getter

	@Override
	public int hashCode() {
		return uID;
	}

	@Override
	public boolean equals(Object obj) {
		return (obj instanceof VirtualRegister) && (uID == ((VirtualRegister) obj).uID);
	}

	@Override
	public String toString() {
		return "$" + uID;
	}

	// For IR

	@Override
	public void consume(CFGMaker maker, ExprOperand src) {
		MoveInstruction.make(maker, this, src);
	}

	@Override
	public VirtualRegister load(CFGMaker maker) {
		return this;
	}

	// For NaiveMips

	public abstract void loadNaiveMips(MipsFile file, MipsReg dest, int spOffset);
	public abstract void storeNaiveMips(MipsFile file, MipsReg src, int spOffset);

	// For AdvancedMips

	public boolean globalOccupied() {
		return false;
	}

	public boolean needReg() {
		return true;
	}

	public boolean needMem() {
		return true;
	}

	public boolean preDefined() {
		return false;
	}

	public MipsReg defaultReg() {
		return null;
	}

	public MipsAddr defaultMem(MemoryProfile memoryProfile) {
		return null;
	}

	public MipsAddr addressConstant() {
		return null;
	}

	public MipsReg getAllocatedReg(MethodProfile profile) {
		return profile.getAllocatedReg(this);
	}

	public MipsAddr getAllocatedMem(MethodProfile profile) {
		return profile.getAllocatedMem(this);
	}

	public void syncToReg(MipsFile file, MethodProfile profile) {
		MipsReg reg = getAllocatedReg(profile);
		MipsAddr addr = getAllocatedMem(profile);
		if (reg != null && addr != null) {
			file.append(new MipsLoadWord(reg, addr));
		}
	}

	public void syncToMem(MipsFile file, MethodProfile profile) {
	}

	public MipsReg loadAdvancedMips(MipsFile file, MethodProfile profile, MipsReg suggest) {
		MipsReg reg = getAllocatedReg(profile);
		if (reg == null) {
			file.append(new MipsLoadWord(suggest, getAllocatedMem(profile)));
			return suggest;
		} else {
			return reg;
		}
	}

	public MipsReg suggestReg(MethodProfile profile, MipsReg suggest) {
		MipsReg reg = getAllocatedReg(profile);
		return reg == null ? suggest : reg;
	}

	public void storeAdvancedMips(MipsFile file, MethodProfile profile, MipsReg current) {
		MipsReg reg = getAllocatedReg(profile);
		if (reg == null) {
			file.append(new MipsStoreWord(current, getAllocatedMem(profile)));
		} else if (!current.equals(reg)) {
			file.append(new MipsMove(reg, current));
		}
	}
}
