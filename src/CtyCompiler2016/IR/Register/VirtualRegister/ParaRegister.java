package CtyCompiler2016.IR.Register.VirtualRegister;

import CtyCompiler2016.IR.RegAlloc.Profile.MemoryProfile;
import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.Mips.MipsAddr.MipsAddr;
import CtyCompiler2016.Mips.MipsAddr.MipsRegAddr;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad.MipsLoadWord;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsMove;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsStore.MipsStoreWord;
import CtyCompiler2016.Mips.MipsOperand.MipsImm;
import CtyCompiler2016.Mips.MipsReg.MipsArgReg;
import CtyCompiler2016.Mips.MipsReg.MipsFp;
import CtyCompiler2016.Mips.MipsReg.MipsReg;
import CtyCompiler2016.Tool.UIDManager;

public class ParaRegister extends VirtualRegister {
	private final int paraID;

	public ParaRegister(UIDManager<ParaRegister> paraManager) {
		super();
		paraID = paraManager.add(this);
	}

	public static ParaRegister make(UIDManager<ParaRegister> paraManager) {
		return new ParaRegister(paraManager);
	}

	@Override
	public String toString() {
		return "$a" + paraID;
	}

	// For NaiveMips

	@Override
	public void loadNaiveMips(MipsFile file, MipsReg dest, int spOffset) {
		if (paraID < MipsArgReg.argRegLimit) {
			file.append(new MipsMove(dest, MipsArgReg.get(paraID)));
		} else {
			file.append(new MipsLoadWord(dest, new MipsRegAddr(MipsFp.instance, new MipsImm(4, paraID - MipsArgReg.argRegLimit))));
		}
	}

	@Override
	public void storeNaiveMips(MipsFile file, MipsReg src, int spOffset) {
		if (paraID < MipsArgReg.argRegLimit) {
			file.append(new MipsMove(MipsArgReg.get(paraID), src));
		} else {
			file.append(new MipsStoreWord(src, new MipsRegAddr(MipsFp.instance, new MipsImm(4, paraID - MipsArgReg.argRegLimit))));
		}
	}

	// For AdvancedMips

	private boolean isExceeded() {
		return paraID >= MipsArgReg.argRegLimit;
	}

	@Override
	public boolean needReg() {
		return isExceeded();
	}

	@Override
	public boolean needMem() {
		return false;
	}

	@Override
	public boolean preDefined() {
		return true;
	}

	@Override
	public MipsReg defaultReg() {
		return isExceeded() ? null : MipsArgReg.get(paraID);
	}

	@Override
	public MipsAddr defaultMem(MemoryProfile memoryProfile) {
		return isExceeded() ? memoryProfile.getExceedArgPos(paraID) : null;
	}

	@Override
	public MipsReg getAllocatedReg(MethodProfile profile) {
		return needReg() ? super.getAllocatedReg(profile) : MipsArgReg.get(paraID);
	}

	@Override
	public MipsAddr getAllocatedMem(MethodProfile profile) {
		return isExceeded() ? profile.memoryProfile.getExceedArgPos(paraID) : null;
	}

	@Override
	public MipsReg loadAdvancedMips(MipsFile file, MethodProfile profile, MipsReg suggest) {
		if (!isExceeded() && profile.isCalling()) {
			file.append(new MipsLoadWord(suggest, profile.memoryProfile.getArgPos(paraID)));
			return suggest;
		} else {
			return super.loadAdvancedMips(file, profile, suggest);
		}
	}
}
