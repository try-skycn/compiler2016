package CtyCompiler2016.IR.Register.VirtualRegister;

import CtyCompiler2016.Mips.MipsAddr.MipsLabel;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Printer.IRPrinter;
import CtyCompiler2016.Tool.UIDManager;

public abstract class StaticRegister extends VirtualRegister {
	private final int staticID;

	StaticRegister(UIDManager<StaticRegister> manager) {
		staticID = manager.add(this);
	}

	@Override
	public String toString() {
		return "$s" + staticID;
	}

	// For IRPrinter

	public void printIR(IRPrinter printer) {
		printer.print(toString() + " =");
	}

	// For NaiveMips

	public MipsLabel staticLabel() {
		return MipsLabel.staticMem(staticID);
	}

	public abstract void initStaticLabel(MipsFile file);

	// For AdvancedMips

	@Override
	public boolean needMem() {
		return false;
	}
}
