package CtyCompiler2016.IR.Register.VirtualRegister;

import CtyCompiler2016.Mips.MipsAddr.MipsRegAddr;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad.MipsLoadWord;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsStore.MipsStoreWord;
import CtyCompiler2016.Mips.MipsOperand.MipsImm;
import CtyCompiler2016.Mips.MipsReg.MipsReg;
import CtyCompiler2016.Mips.MipsReg.MipsSp;
import CtyCompiler2016.Tool.UIDManager;

public class LocalRegister extends VirtualRegister {
	private final int localID;

	public LocalRegister(UIDManager<LocalRegister> localList) {
		localID = localList.add(this);
	}

	@Override
	public String toString() {
		return "$t" + localID;
	}

	// For NaiveMips

	@Override
	public void loadNaiveMips(MipsFile file, MipsReg dest, int spOffset) {
		file.append(new MipsLoadWord(dest, new MipsRegAddr(MipsSp.instance, new MipsImm(4 * localID + spOffset))));
	}

	@Override
	public void storeNaiveMips(MipsFile file, MipsReg src, int spOffset) {
		file.append(new MipsStoreWord(src, new MipsRegAddr(MipsSp.instance, new MipsImm(4 * localID + spOffset))));
	}
}
