package CtyCompiler2016.IR.Register.VirtualRegister;

import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.Mips.MipsAddr.MipsAddr;
import CtyCompiler2016.Mips.MipsData.MipsDataCode;
import CtyCompiler2016.Mips.MipsData.MipsDataLine;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoadAddress;
import CtyCompiler2016.Mips.MipsReg.MipsReg;
import CtyCompiler2016.Printer.IRPrinter;
import CtyCompiler2016.Tool.UIDManager;

public class SpaceRegister extends StaticRegister {
	private final int size;

	public SpaceRegister(UIDManager<StaticRegister> manager, int size) {
		super(manager);
		this.size = size;
	}

	// For IRPrinter

	@Override
	public void printIR(IRPrinter printer) {
		super.printIR(printer);
		printer.println(" space " + size);
	}

	// For NaiveMips

	@Override
	public void loadNaiveMips(MipsFile file, MipsReg dest, int spOffset) {
		file.append(new MipsLoadAddress(dest, staticLabel()));
	}

	@Override
	public void storeNaiveMips(MipsFile file, MipsReg src, int spOffset) {
		CompilerError.errorHandle(ErrorType.UnknownError);
	}

	@Override
	public void initStaticLabel(MipsFile file) {
		file.append(staticLabel());
		file.append(new MipsDataLine(MipsDataCode.Space, size));
		file.append(MipsDataLine.align());
	}

	// For AdvancedMips

	@Override
	public boolean needReg() {
		return false;
	}

	@Override
	public MipsAddr addressConstant() {
		return staticLabel();
	}

	@Override
	public MipsReg getAllocatedReg(MethodProfile profile) {
		CompilerError.errorHandle(ErrorType.UnknownError);
		return null;
	}

	@Override
	public MipsAddr getAllocatedMem(MethodProfile profile) {
		CompilerError.errorHandle(ErrorType.UnknownError);
		return null;
	}

	@Override
	public MipsReg loadAdvancedMips(MipsFile file, MethodProfile profile, MipsReg suggest) {
		file.append(new MipsLoadAddress(suggest, staticLabel()));
		return suggest;
	}

	@Override
	public void storeAdvancedMips(MipsFile file, MethodProfile profile, MipsReg current) {
		CompilerError.errorHandle(ErrorType.UnknownError);
	}
}
