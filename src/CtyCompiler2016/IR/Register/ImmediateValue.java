package CtyCompiler2016.IR.Register;

import CtyCompiler2016.AST.Type.BasicType.IntType;
import CtyCompiler2016.AST.Type.BasicType.VoidType;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad.MipsLoadImmediate;
import CtyCompiler2016.Mips.MipsOperand.MipsImm;
import CtyCompiler2016.Mips.MipsOperand.MipsOperand;
import CtyCompiler2016.Mips.MipsReg.MipsReg;

/**
 * Created by SkytimChen on 4/13/16.
 */
public class ImmediateValue implements VirtualOperand, ExprOperand {
	private int value;

	private ImmediateValue(int value) {
		this.value = value;
	}

	public ImmediateValue negate() {
		value = - value;
		return this;
	}

	public static ImmediateValue zero() {
		return new ImmediateValue(0);
	}

	public static ImmediateValue one() {
		return new ImmediateValue(1);
	}

	public static ImmediateValue boolFalse() {
		return new ImmediateValue(0);
	}

	public static ImmediateValue boolTrue() {
		return new ImmediateValue(1);
	}

	public static ImmediateValue pointerSize() {
		return new ImmediateValue(Type.pointerSize);
	}

	public static ImmediateValue intSize() {
		return new ImmediateValue(IntType.getInstance().sizeof());
	}

	public static ImmediateValue byteSie() {
		return new ImmediateValue(VoidType.getInstance().sizeof());
	}

	public static ImmediateValue make(int value) {
		return new ImmediateValue(value);
	}

	// Getter

	public int getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "{" + Integer.toString(value) + "}";
	}

	// For NaiveMips

	public MipsImm toNaiveMips() {
		return new MipsImm(value);
	}

	@Override
	public MipsImm toNaiveMips(MipsFile file, MipsReg dest, int spOffset) {
		return toNaiveMips();
	}

	@Override
	public void loadNaiveMips(MipsFile file, MipsReg dest, int spOffset) {
		file.append(new MipsLoadImmediate(dest, toNaiveMips()));
	}

	// For AdvancedMips

	public MipsImm toAdvancedMips() {
		return new MipsImm(value);
	}

	@Override
	public MipsOperand toAdvancedMips(MipsFile file, MethodProfile profile, MipsReg suggest) {
		return toAdvancedMips();
	}

	@Override
	public MipsReg loadAdvancedMips(MipsFile file, MethodProfile profile, MipsReg suggest) {
		file.append(new MipsLoadImmediate(suggest, toAdvancedMips()));
		return suggest;
	}
}
