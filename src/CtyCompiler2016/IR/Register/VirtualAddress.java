package CtyCompiler2016.IR.Register;

import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction.LoadInstruction;
import CtyCompiler2016.IR.Instruction.FlowInstruction.ImpactInstruction.StoreInstruction;
import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;
import CtyCompiler2016.Mips.MipsAddr.MipsRegAddr;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad.MipsLoadMem;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsStore.MipsStore;
import CtyCompiler2016.Mips.MipsReg.MipsReg;
import CtyCompiler2016.Mips.MipsReg.MipsTReg;

import java.util.Collection;
import java.util.Collections;

/**
 * Created by SkytimChen on 4/13/16.
 */
public class VirtualAddress implements ExprResult, ExprLeft {
	private final VirtualAssign address;
	private final ImmediateValue offset;
	private final int memSize;

	private VirtualAddress(VirtualAssign address, ImmediateValue offset, int memSize) {
		this.address = address;
		this.offset = offset;
		this.memSize = memSize;
	}

	public static VirtualAddress make(VirtualAssign address, ImmediateValue offset, Type type) {
		return new VirtualAddress(address, offset, type.sizeof());
	}

	// Getter

	public VirtualAssign getAddress() {
		return address;
	}

	public ImmediateValue getOffset() {
		return offset;
	}

	public Collection<VirtualAssign> getUpExposure() {
		return Collections.singletonList(address);
	}

	@Override
	public String toString() {
		return "i" + memSize * 8 + " " + offset + "(" + address.toString() + ")";
	}

	// For IR

	@Override
	public void consume(CFGMaker maker, ExprOperand src) {
		StoreInstruction.make(maker, src, this);
	}

	@Override
	public VirtualRegister load(CFGMaker maker) {
		return LoadInstruction.make(maker, this);
	}

	// For NaiveMips

	public void loadNaiveMips(MipsFile file, MipsReg dest, int spOffset) {
		MipsReg reg = MipsTReg.get(2);
		MipsRegAddr addr = new MipsRegAddr(address.toNaiveMips(file, reg, spOffset), offset.toNaiveMips());
		file.append(MipsLoadMem.make(dest, addr, memSize));
	}

	public void storeNaiveMips(MipsFile file, MipsReg src, int spOffset) {
		MipsReg reg = MipsTReg.get(2);
		MipsRegAddr addr = new MipsRegAddr(address.toNaiveMips(file, reg, spOffset), offset.toNaiveMips());
		file.append(MipsStore.make(src, addr, memSize));
	}

	// For AdvancedMips

	public int getMemSize() {
		return memSize;
	}

	public MipsRegAddr loadAdvancedMips(MipsFile file, MethodProfile profile, MipsReg suggest) {
		MipsReg reg = address.toAdvancedMips(file, profile, suggest);
		return new MipsRegAddr(reg, offset.toAdvancedMips());
	}
}
