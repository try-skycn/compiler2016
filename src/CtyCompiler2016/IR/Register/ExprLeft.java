package CtyCompiler2016.IR.Register;

import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;

/**
 * Created by SkytimChen on 5/5/16.
 */
public interface ExprLeft {
	void consume(CFGMaker maker, ExprOperand src);
	VirtualRegister load(CFGMaker maker);
}
