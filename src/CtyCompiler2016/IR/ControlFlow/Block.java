package CtyCompiler2016.IR.ControlFlow;

import CtyCompiler2016.IR.Instruction.ControlInstruction.ControlInstruction;
import CtyCompiler2016.IR.Instruction.FlowInstruction.FlowInstruction;
import CtyCompiler2016.IR.Instruction.Instruction;
import CtyCompiler2016.IR.RegAlloc.Graph.InterferenceGraph;
import CtyCompiler2016.IR.RegAlloc.Graph.LiveSet;
import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.IR.Register.VirtualAssign;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;
import CtyCompiler2016.Mips.MipsComment;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Printer.IRPrinter;

import java.util.*;

public class Block {
	private Label label;
	private final List<FlowInstruction> instructionList;
	private final ControlInstruction jump;

	Block(Label label, Map<VirtualRegister, VirtualAssign> defMap, List<FlowInstruction> instructionList, ControlInstruction jump) {
		this.label = label;
		this.instructionList = instructionList;
		this.jump = jump;
		this.defMap = defMap;
	}

	public static void make(Label label, Map<VirtualRegister, VirtualAssign> defMap, List<FlowInstruction> instructionList, ControlInstruction jump) {
		Block newBlock = new Block(label, defMap, instructionList, jump);
		label.link(newBlock);
		for (Label dest : jump.getDests()) {
			dest.addSource(newBlock);
		}
	}

	public Label getLabel() {
		return label;
	}

	public ControlInstruction getJump() {
		return jump;
	}

	public Collection<Label> getDests() {
		return jump.getDests();
	}

	// For IRPrinter

	public void printIR(IRPrinter printer) {
		for (Instruction instruction : instructionList) {
			printer.printInstruction(instruction);
		}
		printer.printInstruction(jump);
	}

	// For NaiveMips

	public void printNaiveMips(MipsFile file, Label nextLabel) {
		for (FlowInstruction instruction : instructionList) {
			file.append(MipsComment.make(instruction.toString()));
			instruction.printNaiveMips(file);
		}
		file.append(MipsComment.make(jump.toString()));
		jump.printNaiveMips(file, nextLabel);
	}

	// For RegAlloc

	private final Map<VirtualRegister, VirtualAssign> defMap;
	private final Set<VirtualRegister> liveInSet = new HashSet<>();
	private final Set<VirtualRegister> liveOutSet = new HashSet<>();

	private void searchDef(VirtualRegister register, Collection<Block> searchedBlocks) {
		liveOutSet.add(register);
		searchedBlocks.add(this);
		if (!defMap.containsKey(register) && !liveInSet.contains(register)) {
			liveInSet.add(register);
			for (Block block : label.getSources()) {
				if (!searchedBlocks.contains(block)) {
					block.searchDef(register, searchedBlocks);
				}
			}
		}
	}

	public void awakeUpExposure(VirtualAssign assign) {
		if (assign.getDefinition() == null && !liveInSet.contains(assign.getRegister())) {
			VirtualRegister register = assign.getRegister();
			Collection<Block> searchedBlocks = new HashSet<>();
			liveInSet.add(register);
			for (Block block : label.getSources()) {
				block.searchDef(register, searchedBlocks);
			}
		}
	}

	public void awake(MethodProfile profile) {
		for (FlowInstruction instruction : instructionList) {
			instruction.awake(profile, this);
		}
		jump.awake(profile, this);
	}

	public LiveSet<VirtualRegister> makeInterferenceGraph(InterferenceGraph<VirtualRegister> graph) {
		LiveSet<VirtualRegister> liveNow = new LiveSet<>(graph, liveOutSet);
		jump.getUpExposure().forEach(assign -> liveNow.add(assign.getRegister()));
		for (ListIterator<FlowInstruction> instIter = instructionList.listIterator(instructionList.size()); instIter.hasPrevious(); ) {
			FlowInstruction instruction = instIter.previous();
			VirtualRegister dest = instruction.getDest();
			if (dest != null) {
				liveNow.kill(dest);
			}
			instruction.getUpExposure().forEach(assign -> liveNow.add(assign.getRegister()));
		}
		return liveNow;
	}

	public void printAdvancedMips(MipsFile file, MethodProfile profile, Label nextLabel) {
		for (FlowInstruction instruction : instructionList) {
			file.append(MipsComment.make(instruction.toString()));
			instruction.printAdvancedMips(file, profile);
		}
		file.append(MipsComment.make(jump.toString()));
		jump.printAdvancedMips(file, profile, nextLabel);
	}
}
