package CtyCompiler2016.IR.ControlFlow;

import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.Mips.MipsAddr.MipsLabel;
import CtyCompiler2016.Mips.MipsComment;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Tool.UIDManager;

import java.util.Collection;
import java.util.LinkedList;

/**
 * Created by SkytimChen on 4/16/16.
 */
public class Label {
	private final int methodID;
	public final int uID;

	private Block block;
	private final Collection<Block> sources = new LinkedList<>();

	private Label(int methodID, UIDManager<Label> labelList) {
		this.methodID = methodID;
		uID = labelList.add(this);
	}

	public static Label make(int methodID, UIDManager<Label> labelList) {
		return new Label(methodID, labelList);
	}

	void link(Block dest) {
		this.block = dest;
	}

	void addSource(Block block) {
		sources.add(block);
	}

	public Block getBlock() {
		return block;
	}

	public Collection<Block> getSources() {
		return sources;
	}

	@Override
	public String toString() {
		if (uID == 0) {
			return "Entry#" + methodID;
		} else {
			return "Label#" + methodID + ":" + uID;
		}
	}

	// For NaiveMips

	public MipsLabel toMipsLabel() {
		return MipsLabel.make(methodID, uID);
	}

	public void printNaiveMips(MipsFile file, Label nextLabel) {
		file.append(MipsComment.make(toString()));
		file.append(toMipsLabel());
		block.printNaiveMips(file, nextLabel);
	}

	// For AdvancedMips

	public void printAdvancedMips(MipsFile file, MethodProfile profile, Label nextLabel) {
		file.append(MipsComment.make(toString()));
		file.append(toMipsLabel());
		block.printAdvancedMips(file, profile, nextLabel);
	}
}
