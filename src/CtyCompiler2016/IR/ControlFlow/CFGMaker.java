package CtyCompiler2016.IR.ControlFlow;

import CtyCompiler2016.AST.Constituent.Method.Method;
import CtyCompiler2016.AST.Type.Type;
import CtyCompiler2016.IR.GlobalConstant;
import CtyCompiler2016.IR.IRStructure;
import CtyCompiler2016.IR.Instruction.ControlInstruction.*;
import CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction.AssignInstruction;
import CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction.BinaryInstruction;
import CtyCompiler2016.IR.Instruction.FlowInstruction.FlowInstruction;
import CtyCompiler2016.IR.Instruction.InstructionCode;
import CtyCompiler2016.IR.MethodStructure.ProgramMethod;
import CtyCompiler2016.IR.Register.*;
import CtyCompiler2016.IR.Register.VirtualRegister.LocalRegister;
import CtyCompiler2016.IR.Register.VirtualRegister.ParaRegister;
import CtyCompiler2016.IR.Register.VirtualRegister.StaticRegister;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;
import CtyCompiler2016.Tool.UIDManager;

import java.util.*;

public class CFGMaker {
	private final int methodID;
	private final List<Label> methodBody = new LinkedList<>();

	private final UIDManager<Label> labelManager = new UIDManager<>();
	private final Label methodEnd;

	private CFGMaker(Method method, IRStructure structure) {
		this.structure = structure;
		this.methodID = method.uID;
		makePara(method.getParaCount());
		// For MethodNames
		nameSet = method.getNameSet();
		curLabel = makeLabel();
		methodEnd = makeLabel();
	}

	public static CFGMaker make(Method method, IRStructure structure) {
		return new CFGMaker(method, structure);
	}

	public ProgramMethod finish() {
		putVoidReturn();
		endLabel(methodEnd, new MethodEndInstruction());
		return new ProgramMethod(methodID, nameSet, paraManager.getList(), localManager.getList(), staticSet, methodBody);
	}

	// For MethodNames

	private final Set<String> nameSet;

	// For Register

	private final UIDManager<ParaRegister> paraManager = new UIDManager<>();
	private final UIDManager<LocalRegister> localManager = new UIDManager<>();
	private final Set<StaticRegister> staticSet = new HashSet<>();
	private final Map<Integer, LocalRegister> localIDMap = new HashMap<>();

	private void makePara(int total) {
		for (int i = 0; i < total; i++) {
			new ParaRegister(paraManager);
		}
	}

	public ParaRegister getPara(int id) {
		return paraManager.get(id);
	}

	public LocalRegister makeLocal() {
		return new LocalRegister(localManager);
	}

	public LocalRegister getLocal(int id) {
		if (!localIDMap.containsKey(id)) {
			localIDMap.put(id, makeLocal());
		}
		return localIDMap.get(id);
	}

	public void useStatic(StaticRegister staticRegister) {
		staticSet.add(staticRegister);
	}

	// For IRStructure

	private final IRStructure structure;

	private final Map<VirtualRegister, VirtualAssign> defMap = new HashMap<>();

	public GlobalConstant getGlobalConstant() {
		return structure.getGlobalConstant();
	}

	public void putDef(VirtualRegister register, AssignInstruction instruction) {
		defMap.put(register, VirtualAssign.make(instruction));
	}

	public VirtualAssign getDef(VirtualRegister register) {
		VirtualAssign result = defMap.get(register);
		return result == null ? VirtualAssign.make(register) : result;
	}

	public VirtualOperand makeOperand(ExprOperand operand) {
		if (operand instanceof ImmediateValue) {
			return ((ImmediateValue) operand);
		} else {
			return getDef(((VirtualRegister) operand));
		}
	}

	public List<VirtualOperand> makeOperand(List<ExprOperand> exprOperandList) {
		List<VirtualOperand> virtualOperandList = new ArrayList<>();
		for (ExprOperand exprOperand : exprOperandList) {
			virtualOperandList.add(makeOperand(exprOperand));
		}
		return virtualOperandList;
	}

	public VirtualAddress makeAddress(VirtualRegister startAddress, ExprOperand offset, Type type) {
		if (offset instanceof ImmediateValue) {
			return VirtualAddress.make(getDef(startAddress), ((ImmediateValue) offset), type);
		} else {
			VirtualRegister locatedAddress = BinaryInstruction.assign(this, InstructionCode.Add, startAddress, offset);
			return VirtualAddress.make(getDef(locatedAddress), ImmediateValue.zero(), type);
		}
	}

	private void clearDependency() {
		defMap.clear();
	}

	// For Instruction

	private List<FlowInstruction> instructionList = new LinkedList<>();

	public void append(FlowInstruction instruction) {
		instructionList.add(instruction);
	}

	private List<FlowInstruction> getInstructionList() {
		List<FlowInstruction> result = instructionList;
		instructionList = new LinkedList<>();
		return result;
	}

	// For Label

	private Label curLabel;
	private Stack<Label> continueDestStack = new Stack<>();
	private Stack<Label> breakDestStack = new Stack<>();

	public Label makeLabel() {
		return Label.make(methodID, labelManager);
	}

	private void endLabel(Label label, ControlInstruction jump) {
		if (label != null) {
			Block.make(label, defMap, getInstructionList(), jump);
			methodBody.add(label);
		}
	}

	private void endCurrentLabel(ControlInstruction jump) {
		endLabel(curLabel, jump);
		curLabel = null;
		clearDependency();
	}

	public void setLoop(Label continueDest, Label breakDest) {
		continueDestStack.add(continueDest);
		breakDestStack.add(breakDest);
	}

	public void endLoop() {
		continueDestStack.pop();
		breakDestStack.pop();
	}

	private Label getContinueDest() {
		return continueDestStack.peek();
	}

	private Label getBreakDest() {
		return breakDestStack.peek();
	}

	private Label getReturnDest() {
		return methodEnd;
	}

	public void putLabel(Label label) {
		endCurrentLabel(new JumpInstruction(label));
		curLabel = label;
	}

	public void putJump(Label dest) {
		endCurrentLabel(new JumpInstruction(dest));
	}

	public void putBranch(ExprOperand condition, Label ifTrue, Label ifFalse) {
		endCurrentLabel(BranchInstruction.make(makeOperand(condition), ifTrue, ifFalse));
	}

	public void putContinue() {
		putJump(getContinueDest());
	}

	public void putBreak() {
		putJump(getBreakDest());
	}

	public void putReturn(ExprOperand result) {
		endCurrentLabel(new ReturnInstruction(makeOperand(result), getReturnDest()));
	}

	public void putVoidReturn() {
		putJump(getReturnDest());
	}
}
