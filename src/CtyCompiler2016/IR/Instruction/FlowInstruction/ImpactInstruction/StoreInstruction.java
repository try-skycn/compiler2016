package CtyCompiler2016.IR.Instruction.FlowInstruction.ImpactInstruction;

import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Instruction.FlowInstruction.FlowInstruction;
import CtyCompiler2016.IR.Instruction.InstructionCode;
import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.IR.Register.ExprOperand;
import CtyCompiler2016.IR.Register.VirtualAddress;
import CtyCompiler2016.IR.Register.VirtualAssign;
import CtyCompiler2016.IR.Register.VirtualOperand;
import CtyCompiler2016.Mips.MipsAddr.MipsRegAddr;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsStore.MipsStore;
import CtyCompiler2016.Mips.MipsReg.MipsReg;
import CtyCompiler2016.Mips.MipsReg.MipsTReg;
import CtyCompiler2016.Mips.MipsReg.MipsVReg;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by SkytimChen on 4/15/16.
 */
public class StoreInstruction extends FlowInstruction {
	private final VirtualOperand src;
	private final VirtualAddress dest;

	private StoreInstruction(CFGMaker maker, VirtualOperand src, VirtualAddress dest) {
		super(maker, InstructionCode.Store);
		this.src = src;
		this.dest = dest;
	}

	public static void make(CFGMaker maker, ExprOperand src, VirtualAddress dest) {
		new StoreInstruction(maker, maker.makeOperand(src), dest);
	}

	@Override
	public Collection<VirtualAssign> getUpExposure() {
		Collection<VirtualAssign> result = new ArrayList<>();
		result.addAll(dest.getUpExposure());
		if (src instanceof VirtualAssign) {
			result.add(((VirtualAssign) src));
		}
		return result;
	}

	@Override
	public String toString() {
		return super.toString() + src.toString() + " -> " +  dest.toString();
	}

	// For NaiveMips

	@Override
	public void printNaiveMips(MipsFile file) {
		MipsReg reg = MipsTReg.get(0);
		src.loadNaiveMips(file, reg, 0);
		dest.storeNaiveMips(file, reg, 0);
	}

	// For AdvancedMips

	@Override
	public void printAdvancedMips(MipsFile file, MethodProfile profile) {
		MipsRegAddr addr = dest.loadAdvancedMips(file, profile, MipsVReg.get(1));
		MipsReg srcReg = src.loadAdvancedMips(file, profile, MipsVReg.get(0));
		file.append(MipsStore.make(srcReg, addr, dest.getMemSize()));
	}
}
