package CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction;

import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Instruction.InstructionCode;
import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.IR.Register.VirtualAddress;
import CtyCompiler2016.IR.Register.VirtualAssign;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;
import CtyCompiler2016.Mips.MipsAddr.MipsRegAddr;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad.MipsLoadMem;
import CtyCompiler2016.Mips.MipsReg.MipsReg;
import CtyCompiler2016.Mips.MipsReg.MipsTReg;
import CtyCompiler2016.Mips.MipsReg.MipsVReg;

import java.util.Collection;

/**
 * Created by SkytimChen on 4/17/16.
 */
public class LoadInstruction extends AssignInstruction {
	private final VirtualAddress src;

	private LoadInstruction(CFGMaker maker, VirtualRegister dest, VirtualAddress src) {
		super(maker, InstructionCode.Load, dest);
		this.src = src;
	}

	public static VirtualRegister make(CFGMaker maker, VirtualAddress src) {
		return (new LoadInstruction(maker, maker.makeLocal(), src)).getDest();
	}

	@Override
	public Collection<VirtualAssign> getUpExposure() {
		return src.getUpExposure();
	}

	@Override
	public String toString() {
		return super.toString() + " " + src.toString();
	}

	// For NaiveMipa

	@Override
	public void printNaiveMips(MipsFile file) {
		MipsReg reg = MipsTReg.get(0);
		src.loadNaiveMips(file, reg, 0);
		dest.storeNaiveMips(file, reg, 0);
	}

	// For AdvancedMips

	@Override
	public void printAdvancedMips(MipsFile file, MethodProfile profile) {
		MipsRegAddr addr = src.loadAdvancedMips(file, profile, MipsVReg.get(0));
		MipsReg destReg = dest.suggestReg(profile, MipsVReg.get(0));
		file.append(MipsLoadMem.make(destReg, addr, src.getMemSize()));
		dest.storeAdvancedMips(file, profile, destReg);
	}
}
