package CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction;

import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Instruction.InstructionCode;
import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.IR.Register.ExprOperand;
import CtyCompiler2016.IR.Register.ImmediateValue;
import CtyCompiler2016.IR.Register.VirtualAssign;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsDest.MipsUnary.MipsNOT;
import CtyCompiler2016.Mips.MipsInst.MipsDest.MipsUnary.MipsNegateValue;
import CtyCompiler2016.Mips.MipsReg.MipsReg;
import CtyCompiler2016.Mips.MipsReg.MipsTReg;
import CtyCompiler2016.Mips.MipsReg.MipsVReg;

import java.util.Collection;
import java.util.Collections;

/**
 * Created by SkytimChen on 4/17/16.
 */
public class UnaryInstruction extends AssignInstruction {
	private final VirtualAssign src;

	private UnaryInstruction(CFGMaker maker, InstructionCode instructionCode, VirtualRegister dest, VirtualAssign src) {
		super(maker, instructionCode, dest);
		this.src = src;
	}

	public static VirtualRegister assign(CFGMaker maker, InstructionCode instructionCode, VirtualRegister src) {
		return (new UnaryInstruction(maker, instructionCode, maker.makeLocal(), maker.getDef(src))).getDest();
	}

	public static ExprOperand make(CFGMaker maker, InstructionCode instructionCode, ExprOperand src) {
		if (src instanceof ImmediateValue) {
			return ImmediateValue.make(instructionCode.calc(((ImmediateValue) src).getValue()));
		} else {
			return assign(maker, instructionCode, ((VirtualRegister) src));
		}
	}

	@Override
	public Collection<VirtualAssign> getUpExposure() {
		return Collections.singletonList(src);
	}

	@Override
	public String toString() {
		return super.toString() + " " + src.toString();
	}

	// For NaiveMips

	private void printMips(MipsFile file, MipsReg destReg, MipsReg reg) {
		switch (instructionCode) {
			case Negate:
				file.append(new MipsNegateValue(destReg, reg));
				break;
			case Not:
				file.append(new MipsNOT(destReg, reg));
				break;
		}
	}

	@Override
	public void printNaiveMips(MipsFile file) {
		MipsReg reg = MipsTReg.get(0);
		src.loadNaiveMips(file, reg, 0);
		printMips(file, reg, reg);
		dest.storeNaiveMips(file, reg, 0);
	}

	// For AdvancedMips

	@Override
	public void printAdvancedMips(MipsFile file, MethodProfile profile) {
		MipsReg reg = src.loadAdvancedMips(file, profile, MipsVReg.get(0));
		MipsReg destReg = dest.suggestReg(profile, MipsVReg.get(0));
		printMips(file, destReg, reg);
		dest.storeAdvancedMips(file, profile, destReg);
	}
}
