package CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction;

import CtyCompiler2016.IR.ControlFlow.Block;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Instruction.InstructionCode;
import CtyCompiler2016.IR.Instruction.Tool.Call;
import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.IR.Register.VirtualAssign;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsReg.MipsVReg;

import java.util.Collection;

/**
 * Created by SkytimChen on 4/17/16.
 */
public class CallInstruction extends AssignInstruction {
	private final Call callInfo;

	private CallInstruction(CFGMaker maker, VirtualRegister dest, Call callInfo) {
		super(maker, InstructionCode.Call, dest);
		this.callInfo = callInfo;
	}

	public static VirtualRegister make(CFGMaker maker, Call callInfo) {
		return (new CallInstruction(maker, maker.makeLocal(), callInfo)).getDest();
	}

	@Override
	public Collection<VirtualAssign> getUpExposure() {
		return callInfo.getUpExposure();
	}

	@Override
	public String toString() {
		return super.toString() + " " + callInfo.toString();
	}

	// For NaiveMips

	@Override
	public void printNaiveMips(MipsFile file) {
		callInfo.printNaiveMips(file);
		dest.storeNaiveMips(file, MipsVReg.get(0), 0);
	}

	// For RegAlloc

	@Override
	public void awake(MethodProfile profile, Block block) {
		super.awake(profile, block);
		callInfo.awake(profile);
	}

	// For AdvancedMips

	@Override
	public void printAdvancedMips(MipsFile file, MethodProfile profile) {
		callInfo.printAdvancedMips(file, profile);
		dest.storeAdvancedMips(file, profile, MipsVReg.get(0));
	}
}
