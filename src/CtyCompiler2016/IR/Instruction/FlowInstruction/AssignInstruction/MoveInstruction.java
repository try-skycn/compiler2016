package CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction;

import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Instruction.InstructionCode;
import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.IR.Register.ExprOperand;
import CtyCompiler2016.IR.Register.VirtualAssign;
import CtyCompiler2016.IR.Register.VirtualOperand;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad.MipsLoadImmediate;
import CtyCompiler2016.Mips.MipsOperand.MipsImm;
import CtyCompiler2016.Mips.MipsOperand.MipsOperand;
import CtyCompiler2016.Mips.MipsReg.MipsReg;
import CtyCompiler2016.Mips.MipsReg.MipsTReg;
import CtyCompiler2016.Mips.MipsReg.MipsVReg;

import java.util.Collection;
import java.util.Collections;

/**
 * Created by SkytimChen on 4/17/16.
 */
public class MoveInstruction extends AssignInstruction {
	private final VirtualOperand src;

	private MoveInstruction(CFGMaker maker, VirtualRegister dest, VirtualOperand src) {
		super(maker, InstructionCode.Move, dest);
		this.src = src;
	}

	public static VirtualRegister alias(CFGMaker maker, ExprOperand src) {
		return (new MoveInstruction(maker, maker.makeLocal(), maker.makeOperand(src))).getDest();
	}

	public static void make(CFGMaker maker, VirtualRegister dest, ExprOperand src) {
		new MoveInstruction(maker, dest, maker.makeOperand(src));
	}

	@Override
	public Collection<VirtualAssign> getUpExposure() {
		return src instanceof VirtualAssign ? Collections.singletonList(((VirtualAssign) src)) : Collections.emptyList();
	}

	@Override
	public String toString() {
		return super.toString() + " " + src.toString();
	}

	// For NaiveMips

	@Override
	public void printNaiveMips(MipsFile file) {
		MipsReg reg = MipsTReg.get(0);
		src.loadNaiveMips(file, reg, 0);
		dest.storeNaiveMips(file, reg, 0);
	}

	// For AdvancedMips

	@Override
	public void printAdvancedMips(MipsFile file, MethodProfile profile) {
		MipsOperand srcOperand = src.toAdvancedMips(file, profile, MipsVReg.get(0));
		if (srcOperand instanceof MipsReg) {
			dest.storeAdvancedMips(file, profile, ((MipsReg) srcOperand));
		} else if (srcOperand instanceof MipsImm) {
			MipsReg destReg = dest.suggestReg(profile, MipsVReg.get(0));
			file.append(new MipsLoadImmediate(destReg, ((MipsImm) srcOperand)));
			dest.storeAdvancedMips(file, profile, destReg);
		} else {
			CompilerError.errorHandle(ErrorType.UnknownError);
		}
	}
}
