package CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction;

import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Instruction.InstructionCode;
import CtyCompiler2016.IR.Instruction.Tool.Syscall;
import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.IR.Register.ExprOperand;
import CtyCompiler2016.IR.Register.VirtualAssign;
import CtyCompiler2016.IR.Register.VirtualOperand;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsReg.MipsVReg;
import CtyCompiler2016.Tool.SyscallID;

import java.util.Collection;
import java.util.Collections;

/**
 * Created by SkytimChen on 4/17/16.
 */
public class NewInstruction extends AssignInstruction {
	private final VirtualOperand length;

	private NewInstruction(CFGMaker maker, VirtualRegister dest, VirtualOperand length) {
		super(maker, InstructionCode.New, dest);
		this.length = length;
	}

	public static VirtualRegister make(CFGMaker maker, ExprOperand length) {
		return (new NewInstruction(maker, maker.makeLocal(), maker.makeOperand(length))).getDest();
	}

	@Override
	public Collection<VirtualAssign> getUpExposure() {
		return length instanceof VirtualAssign ? Collections.singletonList(((VirtualAssign) length)) : Collections.emptyList();
	}

	@Override
	public String toString() {
		return super.toString() + " malloc " + length.toString();
	}

	// For NaiveMips

	@Override
	public void printNaiveMips(MipsFile file) {
		Syscall.make(SyscallID.Malloc, Collections.singletonList(length)).printNaiveMips(file);
		dest.storeNaiveMips(file, MipsVReg.get(0), 0);
	}

	// For AdvancedMips

	@Override
	public void printAdvancedMips(MipsFile file, MethodProfile profile) {
		Syscall.make(SyscallID.Malloc, Collections.singletonList(length)).printAdvancedMips(file, profile);
		dest.storeAdvancedMips(file, profile, MipsVReg.get(0));
	}
}
