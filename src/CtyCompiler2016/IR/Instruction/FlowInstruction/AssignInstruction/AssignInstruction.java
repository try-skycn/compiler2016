package CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction;

import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Instruction.FlowInstruction.FlowInstruction;
import CtyCompiler2016.IR.Instruction.InstructionCode;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;

/**
 * Created by SkytimChen on 4/15/16.
 */
public abstract class AssignInstruction extends FlowInstruction {
	final VirtualRegister dest;

	AssignInstruction(CFGMaker maker, InstructionCode instructionCode, VirtualRegister dest) {
		super(maker, instructionCode);
		this.dest = dest;
		maker.putDef(dest, this);
	}

	@Override
	public VirtualRegister getDest() {
		return dest;
	}

	@Override
	public String toString() {
		return super.toString() + " " + dest.toString() + " <-";
	}
}
