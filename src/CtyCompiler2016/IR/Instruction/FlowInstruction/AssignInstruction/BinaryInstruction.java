package CtyCompiler2016.IR.Instruction.FlowInstruction.AssignInstruction;

import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Instruction.InstructionCode;
import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.IR.Register.ExprOperand;
import CtyCompiler2016.IR.Register.ImmediateValue;
import CtyCompiler2016.IR.Register.VirtualAssign;
import CtyCompiler2016.IR.Register.VirtualOperand;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsDest.MipsBinary.*;
import CtyCompiler2016.Mips.MipsOperand.MipsOperand;
import CtyCompiler2016.Mips.MipsReg.MipsReg;
import CtyCompiler2016.Mips.MipsReg.MipsTReg;
import CtyCompiler2016.Mips.MipsReg.MipsVReg;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by SkytimChen on 4/17/16.
 */
public class BinaryInstruction extends AssignInstruction {
	private final VirtualAssign srcLeft;
	private final VirtualOperand srcRight;

	private BinaryInstruction(CFGMaker maker, InstructionCode instructionCode, VirtualRegister dest, VirtualAssign srcLeft, VirtualOperand srcRight) {
		super(maker, instructionCode, dest);
		this.srcLeft = srcLeft;
		this.srcRight = srcRight;
	}

	private static ImmediateValue calc(InstructionCode instructionCode, ImmediateValue srcLeft, ImmediateValue srcRight) {
		return ImmediateValue.make(instructionCode.calc(srcLeft.getValue(), srcRight.getValue()));
	}

	public static VirtualRegister assign(CFGMaker maker, InstructionCode code, VirtualRegister srcLeft, ExprOperand srcRight) {
		return (new BinaryInstruction(maker, code, maker.makeLocal(), maker.getDef(srcLeft), maker.makeOperand(srcRight))).getDest();
	}

	public static ExprOperand make(CFGMaker maker, InstructionCode instructionCode, ExprOperand srcLeft, ExprOperand srcRight) {
		if (srcLeft instanceof ImmediateValue) {
			if (srcRight instanceof ImmediateValue) {
				return calc(instructionCode, ((ImmediateValue) srcLeft), ((ImmediateValue) srcRight));
			} else {
				VirtualRegister regRight = ((VirtualRegister) srcRight);
				switch (instructionCode) {
					case LeftShift:
					case RightShift:
					case Subtract:
					case Divide:
					case Remainder:
					{
						VirtualRegister tmp = MoveInstruction.alias(maker, srcLeft);
						return assign(maker, instructionCode, tmp, srcRight);
					}
					default:
						return assign(maker, instructionCode.reverse(), regRight, srcLeft);
				}
			}
		} else {
			return assign(maker, instructionCode, ((VirtualRegister) srcLeft), srcRight);
		}
	}

	@Override
	public Collection<VirtualAssign> getUpExposure() {
		return srcRight instanceof VirtualAssign ? Arrays.asList(srcLeft, ((VirtualAssign) srcRight)) : Collections.singletonList(srcLeft);
	}

	@Override
	public String toString() {
		return super.toString() + " " + srcLeft.toString() + ", " + srcRight.toString();
	}

	// For NaiveMips

	public void printMips(MipsFile file, MipsReg destReg, MipsReg lsrc, MipsOperand rsrc) {
		switch (instructionCode) {
			case Add:
				file.append(new MipsAddition(destReg, lsrc, rsrc));
				break;
			case Subtract:
				file.append(new MipsSubtract(destReg, lsrc, rsrc));
				break;
			case Multiply:
				file.append(new MipsMultiply(destReg, lsrc, rsrc));
				break;
			case Divide:
				file.append(new MipsDivide(destReg, lsrc, rsrc));
				break;
			case Remainder:
				file.append(new MipsRemainder(destReg, lsrc, rsrc));
				break;
			case Xor:
				file.append(new MipsXOR(destReg, lsrc, rsrc));
				break;
			case Or:
				file.append(new MipsOr(destReg, lsrc, rsrc));
				break;
			case And:
				file.append(new MipsAND(destReg, lsrc, rsrc));
				break;
			case Equal:
				file.append(new MipsSetEqual(destReg, lsrc, rsrc));
				break;
			case NotEqual:
				file.append(new MipsSetNotEqual(destReg, lsrc, rsrc));
				break;
			case Less:
				file.append(new MipsSetLessThan(destReg, lsrc, rsrc));
				break;
			case LessEqual:
				file.append(new MipsSetLessThanEqual(destReg, lsrc, rsrc));
				break;
			case Greater:
				file.append(new MipsSetGreaterThan(destReg, lsrc, rsrc));
				break;
			case GreaterEqual:
				file.append(new MipsSetGreaterThanEqual(destReg, lsrc, rsrc));
				break;
			case LeftShift:
				file.append(new MipsShiftLeftLogical(destReg, lsrc, rsrc));
				break;
			case RightShift:
				file.append(new MipsShiftRightLogical(destReg, lsrc, rsrc));
				break;
		}
	}

	@Override
	public void printNaiveMips(MipsFile file) {
		MipsReg lsrc = srcLeft.toNaiveMips(file, MipsTReg.get(0), 0);
		MipsOperand rsrc = srcRight.toNaiveMips(file, MipsTReg.get(1), 0);
		MipsReg destReg = MipsTReg.get(0);
		printMips(file, destReg, lsrc, rsrc);
		dest.storeNaiveMips(file, destReg, 0);
	}

	// For AdvancedMips

	@Override
	public void printAdvancedMips(MipsFile file, MethodProfile profile) {
		MipsReg lsrc = srcLeft.toAdvancedMips(file, profile, MipsVReg.get(0));
		MipsOperand rsrc = srcRight.toAdvancedMips(file, profile, MipsVReg.get(1));
		MipsReg destReg = dest.suggestReg(profile, MipsVReg.get(0));
		printMips(file, destReg, lsrc, rsrc);
		dest.storeAdvancedMips(file, profile, destReg);
	}
}
