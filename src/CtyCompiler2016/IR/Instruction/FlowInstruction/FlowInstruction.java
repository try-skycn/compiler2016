package CtyCompiler2016.IR.Instruction.FlowInstruction;

import CtyCompiler2016.IR.ControlFlow.CFGMaker;
import CtyCompiler2016.IR.Instruction.Instruction;
import CtyCompiler2016.IR.Instruction.InstructionCode;
import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;
import CtyCompiler2016.Mips.MipsFile;

/**
 * Created by SkytimChen on 4/23/16.
 */
public abstract class FlowInstruction extends Instruction {
	protected FlowInstruction(CFGMaker maker, InstructionCode instructionCode) {
		super(instructionCode);
		maker.append(this);
	}

	public VirtualRegister getDest() {
		return null;
	}

	// For NaiveMips

	public void printNaiveMips(MipsFile file) {
	}

	// For AdvancedMips

	public void printAdvancedMips(MipsFile file, MethodProfile profile) {
	}
}
