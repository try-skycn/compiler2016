package CtyCompiler2016.IR.Instruction;

import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;

/**
 * Created by SkytimChen on 4/15/16.
 */
public enum InstructionCode {
	// Memory accessment
	Load,
	Store,
	// New
	New,
	// Move
	Move,
	// Call
	Call,
	// Arithmetic
	Add,
	Subtract,
	Multiply,
	Divide,
	Remainder,
	LeftShift,
	RightShift,
	Xor,
	Or,
	And,
	Equal,
	NotEqual,
	Less,
	LessEqual,
	Greater,
	GreaterEqual,
	Negate,
	Not,
	// Jump
	Jump,
	Branch,
	MethodEnd,
	// Empty
	NOP;

	public int calc(int l, int r) {
		switch (this) {
			case Add:
				return l + r;
			case Subtract:
				return l - r;
			case Multiply:
				return l * r;
			case Divide:
				return l / r;
			case Remainder:
				return l % r;
			case Xor:
				return l ^ r;
			case Or:
				return l | r;
			case And:
				return l & r;
			case Equal:
				return (l == r) ? 1 : 0;
			case NotEqual:
				return (l != r) ? 1 : 0;
			case Less:
				return (l < r) ? 1 : 0;
			case LessEqual:
				return (l <= r) ? 1 : 0;
			case Greater:
				return (l > r) ? 1 : 0;
			case GreaterEqual:
				return (l >= r) ? 1 : 0;
			case LeftShift:
				return l << r;
			case RightShift:
				return l >> r;
			default:
				CompilerError.errorHandle(ErrorType.UnknownError);
				return 0;
		}
	}

	public int calc(int x) {
		switch (this) {
			case Negate:
				return -x;
			case Not:
				return ~x;
			default:
				CompilerError.errorHandle(ErrorType.UnknownError);
				return 0;
		}
	}

	public InstructionCode reverse() {
		switch (this) {
			case Less:
				return Greater;
			case LessEqual:
				return GreaterEqual;
			case Greater:
				return Less;
			case GreaterEqual:
				return LessEqual;
			case Add:
			case Multiply:
			case Xor:
			case Or:
			case And:
			case Equal:
			case NotEqual:
				return this;
			default:
				CompilerError.errorHandle(ErrorType.UnknownError);
				return null;
		}
	}
}
