package CtyCompiler2016.IR.Instruction.ControlInstruction;

import CtyCompiler2016.IR.ControlFlow.Label;
import CtyCompiler2016.IR.Instruction.Instruction;
import CtyCompiler2016.IR.Instruction.InstructionCode;
import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.IR.Register.VirtualAssign;
import CtyCompiler2016.Mips.MipsFile;

import java.util.Collection;
import java.util.Collections;

/**
 * Created by SkytimChen on 4/16/16.
 */
public abstract class ControlInstruction extends Instruction {
	ControlInstruction(InstructionCode instructionCode) {
		super(instructionCode);
	}

	public abstract Collection<Label> getDests();

	@Override
	public Collection<VirtualAssign> getUpExposure() {
		return Collections.emptyList();
	}

	// For NaiveMips

	public void printNaiveMips(MipsFile file, Label nextLabel) {
	}

	// For AdvancedMips

	public void printAdvancedMips(MipsFile file, MethodProfile profile, Label nextLabel) {
	}
}
