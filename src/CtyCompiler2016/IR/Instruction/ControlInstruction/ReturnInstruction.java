package CtyCompiler2016.IR.Instruction.ControlInstruction;

import CtyCompiler2016.IR.ControlFlow.Label;
import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.IR.Register.VirtualAssign;
import CtyCompiler2016.IR.Register.VirtualOperand;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsMove;
import CtyCompiler2016.Mips.MipsReg.MipsReg;
import CtyCompiler2016.Mips.MipsReg.MipsVReg;

import java.util.Collection;
import java.util.Collections;

/**
 * Created by SkytimChen on 4/16/16.
 */
public class ReturnInstruction extends JumpInstruction {
	private final VirtualOperand result;

	public ReturnInstruction(VirtualOperand result, Label dest) {
		super(dest);
		this.result = result;
	}

	@Override
	public Collection<VirtualAssign> getUpExposure() {
		return result instanceof VirtualAssign ? Collections.singletonList(((VirtualAssign) result)) : Collections.emptyList();
	}

	@Override
	public String toString() {
		return "[Return]" + result.toString() + " " + super.toString();
	}

	// For NaiveMips

	@Override
	public void printNaiveMips(MipsFile file, Label nextLabel) {
		result.loadNaiveMips(file, MipsVReg.get(0), 0);
		super.printNaiveMips(file, nextLabel);
	}

	// For AdvancedMips

	@Override
	public void printAdvancedMips(MipsFile file, MethodProfile profile, Label nextLabel) {
		MipsVReg v0 = MipsVReg.get(0);
		MipsReg reg = result.loadAdvancedMips(file, profile, v0);
		if (!reg.equals(v0)) {
			file.append(new MipsMove(v0, reg));
		}
		super.printAdvancedMips(file, profile, nextLabel);
	}
}
