package CtyCompiler2016.IR.Instruction.ControlInstruction;

import CtyCompiler2016.IR.ControlFlow.Label;
import CtyCompiler2016.IR.Instruction.InstructionCode;

import java.util.Collection;
import java.util.Collections;

/**
 * Created by SkytimChen on 4/18/16.
 */
public class MethodEndInstruction extends ControlInstruction {
	public MethodEndInstruction() {
		super(InstructionCode.MethodEnd);
	}

	@Override
	public Collection<Label> getDests() {
		return Collections.emptyList();
	}
}
