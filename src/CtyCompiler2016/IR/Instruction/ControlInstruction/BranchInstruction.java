package CtyCompiler2016.IR.Instruction.ControlInstruction;

import CtyCompiler2016.IR.ControlFlow.Label;
import CtyCompiler2016.IR.Instruction.InstructionCode;
import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.IR.Register.ImmediateValue;
import CtyCompiler2016.IR.Register.VirtualAssign;
import CtyCompiler2016.IR.Register.VirtualOperand;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsBranchOnCondition.MipsBranchOnEqualZero;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsBranchOnCondition.MipsBranchOnNotEqualZero;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsJump;
import CtyCompiler2016.Mips.MipsReg.MipsReg;
import CtyCompiler2016.Mips.MipsReg.MipsTReg;
import CtyCompiler2016.Mips.MipsReg.MipsVReg;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by SkytimChen on 4/16/16.
 */
public class BranchInstruction extends ControlInstruction {
	private final VirtualAssign condition;
	private final Label ifTrue, ifFalse;

	private BranchInstruction(VirtualAssign condition, Label ifTrue, Label ifFalse) {
		super(InstructionCode.Branch);
		this.condition = condition;
		this.ifTrue = ifTrue;
		this.ifFalse = ifFalse;
	}

	public static ControlInstruction make(VirtualOperand condition, Label ifTrue, Label ifFalse) {
		if (condition instanceof ImmediateValue) {
			if (((ImmediateValue) condition).getValue() == 0) {
				return new JumpInstruction(ifFalse);
			} else {
				return new JumpInstruction(ifTrue);
			}
		} else {
			return new BranchInstruction(((VirtualAssign) condition), ifTrue, ifFalse);
		}
	}

	@Override
	public Collection<Label> getDests() {
		return Arrays.asList(ifTrue, ifFalse);
	}

	@Override
	public Collection<VirtualAssign> getUpExposure() {
		return Collections.singletonList(condition);
	}

	@Override
	public String toString() {
		return super.toString() + condition.toString() + " [then]" + ifTrue.toString() + " [else]" + ifFalse.toString();
	}

	// For NaiveMips

	public void printMips(MipsFile file, MipsReg reg, Label nextLabel) {
		if (nextLabel == ifTrue) {
			file.append(new MipsBranchOnEqualZero(reg, ifFalse.toMipsLabel()));
		} else if (nextLabel == ifFalse) {
			file.append(new MipsBranchOnNotEqualZero(reg, ifTrue.toMipsLabel()));
		} else {
			file.append(new MipsBranchOnEqualZero(reg, ifFalse.toMipsLabel()));
			file.append(new MipsJump(ifTrue.toMipsLabel()));
		}
	}

	@Override
	public void printNaiveMips(MipsFile file, Label nextLabel) {
		MipsReg reg = MipsTReg.get(0);
		condition.loadNaiveMips(file, reg, 0);
		printMips(file, reg, nextLabel);
	}

	// For AdvancedMips

	@Override
	public void printAdvancedMips(MipsFile file, MethodProfile profile, Label nextLabel) {
		MipsReg reg = condition.loadAdvancedMips(file, profile, MipsVReg.get(0));
		printMips(file, reg, nextLabel);
	}
}
