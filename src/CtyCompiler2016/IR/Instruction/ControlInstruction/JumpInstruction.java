package CtyCompiler2016.IR.Instruction.ControlInstruction;

import CtyCompiler2016.IR.ControlFlow.Label;
import CtyCompiler2016.IR.Instruction.InstructionCode;
import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsJump;

import java.util.Collection;
import java.util.Collections;

/**
 * Created by SkytimChen on 4/16/16.
 */
public class JumpInstruction extends ControlInstruction {
	private final Label dest;

	public JumpInstruction(Label dest) {
		super(InstructionCode.Jump);
		this.dest = dest;
	}

	@Override
	public Collection<Label> getDests() {
		return Collections.singleton(dest);
	}

	public Label getDest() {
		return dest;
	}

	@Override
	public String toString() {
		return super.toString() + dest.toString();
	}

	// For NaiveMips

	@Override
	public void printNaiveMips(MipsFile file, Label nextLabel) {
		if (dest != nextLabel) {
			file.append(new MipsJump(dest.toMipsLabel()));
		}
	}

	// For AdvancedMips

	@Override
	public void printAdvancedMips(MipsFile file, MethodProfile profile, Label nextLabel) {
		if (dest != nextLabel) {
			file.append(new MipsJump(dest.toMipsLabel()));
		}
	}
}
