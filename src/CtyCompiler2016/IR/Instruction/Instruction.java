package CtyCompiler2016.IR.Instruction;

import CtyCompiler2016.IR.ControlFlow.Block;
import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.IR.Register.VirtualAssign;

import java.util.Collection;

/**
 * Created by SkytimChen on 4/15/16.
 */
public abstract class Instruction {
	protected final InstructionCode instructionCode;

	protected Instruction(InstructionCode instructionCode) {
		this.instructionCode = instructionCode;
	}

	public abstract Collection<VirtualAssign> getUpExposure();

	@Override
	public String toString() {
		return "[" + instructionCode.toString() + "]";
	}

	// For RegAlloc

	public void awake(MethodProfile profile, Block block) {
		getUpExposure().forEach(block::awakeUpExposure);
	}
}
