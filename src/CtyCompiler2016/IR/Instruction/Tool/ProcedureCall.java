package CtyCompiler2016.IR.Instruction.Tool;

import CtyCompiler2016.IR.Register.VirtualOperand;
import CtyCompiler2016.Mips.MipsAddr.MipsLabel;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsJumpAndLink;

import java.util.List;

/**
 * Created by SkytimChen on 4/15/16.
 */
public class ProcedureCall extends Call {
	private final int methodUID;

	private ProcedureCall(int methodUID, List<VirtualOperand> paraList) {
		super(paraList);
		this.methodUID = methodUID;
	}

	public static ProcedureCall make(int methodUID, List<VirtualOperand> paraList) {
		return new ProcedureCall(methodUID, paraList);
	}

	public int getMethodUID() {
		return methodUID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("method#");
		builder.append(methodUID);
		if (paraList.size() > 0) {
			builder.append("(");
			int cnt = 0;
			for (VirtualOperand virtualOperand : paraList) {
				if (cnt > 0) {
					builder.append(", ");
				}
				builder.append(virtualOperand.toString());
				cnt++;
			}
			builder.append(")");
		}
		return builder.toString();
	}

	// For NaiveMips

	@Override
	void callMips(MipsFile file) {
		file.append(new MipsJumpAndLink(MipsLabel.entry(methodUID)));
	}
}
