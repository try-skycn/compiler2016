package CtyCompiler2016.IR.Instruction.Tool;

import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.IR.Register.VirtualAssign;
import CtyCompiler2016.IR.Register.VirtualOperand;
import CtyCompiler2016.Mips.MipsAddr.MipsRegAddr;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsDest.MipsBinary.MipsAddition;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad.MipsLoadWord;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsMove;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsStore.MipsStoreWord;
import CtyCompiler2016.Mips.MipsOperand.MipsImm;
import CtyCompiler2016.Mips.MipsReg.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by SkytimChen on 4/23/16.
 */
public abstract class Call {
	final List<VirtualOperand> paraList;

	Call(List<VirtualOperand> paraList) {
		this.paraList = paraList;
	}

	public Collection<VirtualAssign> getUpExposure() {
		Collection<VirtualAssign> result = new ArrayList<>();
		for (VirtualOperand para : paraList) {
			if (para instanceof VirtualAssign) {
				result.add(((VirtualAssign) para));
			}
		}
		return result;
	}

	// For NaiveMips

	abstract void callMips(MipsFile file);

	public void printNaiveMips(MipsFile file) {
		int regArgCnt = Integer.min(paraList.size(), MipsArgReg.argRegLimit);
		int exArgCnt = Integer.max(paraList.size() - MipsArgReg.argRegLimit, 0);
		int spOffset = 4 * (6 + exArgCnt);

		MipsVReg v0 = MipsVReg.get(0);

		// Push Stack
		file.append(new MipsAddition(MipsSp.instance, MipsSp.instance, new MipsImm(- spOffset)));
		for (int i = 0; i < 4; i++) {
			file.append(new MipsStoreWord(MipsArgReg.get(i), new MipsRegAddr(MipsSp.instance, new MipsImm(4 * (exArgCnt + 6 - i - 1)))));
		}
		file.append(new MipsStoreWord(MipsFp.instance, new MipsRegAddr(MipsSp.instance, new MipsImm(4 * (exArgCnt + 1)))));
		file.append(new MipsStoreWord(MipsRa.instance, new MipsRegAddr(MipsSp.instance, new MipsImm(4 * exArgCnt))));

		// Load Arguments
		for (int i = 0; i < regArgCnt; i++) {
			paraList.get(i).loadNaiveMips(file, MipsSReg.get(i), spOffset);
		}
		for (int i = 0; i < regArgCnt; i++) {
			file.append(new MipsMove(MipsArgReg.get(i), MipsSReg.get(i)));
		}
		for (int i = 0; i < exArgCnt; i++) {
			paraList.get(i + MipsArgReg.argRegLimit).loadNaiveMips(file, v0, spOffset);
			file.append(new MipsStoreWord(v0, new MipsRegAddr(MipsSp.instance, new MipsImm(4 * i))));
		}

		// Call
		callMips(file);

		// Pop Stack
		file.append(new MipsLoadWord(MipsRa.instance, new MipsRegAddr(MipsSp.instance, new MipsImm(4 * exArgCnt))));
		file.append(new MipsLoadWord(MipsFp.instance, new MipsRegAddr(MipsSp.instance, new MipsImm(4 * (exArgCnt + 1)))));
		for (int i = 0; i < 4; i++) {
			file.append(new MipsLoadWord(MipsArgReg.get(i), new MipsRegAddr(MipsSp.instance, new MipsImm(4 * (exArgCnt + 6 - i - 1)))));
		}
		file.append(new MipsAddition(MipsSp.instance, MipsSp.instance, new MipsImm(spOffset)));
	}

	// For RegAlloc

	public void awake(MethodProfile profile) {
		profile.setCalleeArg(paraList.size());
	}

	// For AdvancedMips

	public void printAdvancedMips(MipsFile file, MethodProfile profile) {
		// For Load
		profile.syncStaticToMem(file);
		profile.saveCallerRegs(file);

		profile.beginCall();
		MipsVReg v0 = MipsVReg.get(0);
		int paraID = 0;
		for (VirtualOperand para : paraList) {
			if (paraID < MipsArgReg.argRegLimit) {
				MipsArgReg argReg = MipsArgReg.get(paraID);
				MipsReg reg = para.loadAdvancedMips(file, profile, argReg);
				if (!reg.equals(argReg)) {
					file.append(new MipsMove(argReg, reg));
				}
			} else {
				int exceed = paraID - MipsArgReg.argRegLimit;
				MipsReg reg = para.loadAdvancedMips(file, profile, v0);
				file.append(new MipsStoreWord(reg, new MipsRegAddr(MipsSp.instance, new MipsImm(4, exceed))));
			}
			paraID++;
		}

		// Call
		callMips(file);

		// For Back
		profile.endCall();
		profile.loadCallerRegs(file);
		profile.syncStaticToReg(file);
	}
}
