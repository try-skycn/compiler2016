package CtyCompiler2016.IR.Instruction.Tool;

import CtyCompiler2016.IR.Register.VirtualOperand;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsSyscall;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad.MipsLoadImmediate;
import CtyCompiler2016.Mips.MipsOperand.MipsImm;
import CtyCompiler2016.Mips.MipsReg.MipsVReg;
import CtyCompiler2016.Tool.SyscallID;

import java.util.List;

/**
 * Created by SkytimChen on 4/21/16.
 */
public class Syscall extends Call {
	private final SyscallID syscallID;

	private Syscall(SyscallID syscallID, List<VirtualOperand> paraList) {
		super(paraList);
		this.syscallID = syscallID;
	}

	public static Syscall make(SyscallID syscallID, List<VirtualOperand> paraList) {
		return new Syscall(syscallID, paraList);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("syscall:");
		builder.append(syscallID);
		if (paraList.size() > 0) {
			builder.append("(");
			int cnt = 0;
			for (VirtualOperand virtualOperand : paraList) {
				if (cnt > 0) {
					builder.append(", ");
				}
				builder.append(virtualOperand.toString());
				cnt++;
			}
			builder.append(")");
		}
		return builder.toString();
	}

	// For NaiveMips

	@Override
	void callMips(MipsFile file) {
		file.append(new MipsLoadImmediate(MipsVReg.get(0), new MipsImm(syscallID.toInt())));
		file.append(new MipsSyscall());
	}
}
