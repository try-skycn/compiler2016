package CtyCompiler2016.IR.MethodStructure;

import CtyCompiler2016.IR.GlobalConstant;
import CtyCompiler2016.Mips.MipsComment;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Printer.IRPrinter;

/**
 * Created by SkytimChen on 4/22/16.
 */
public abstract class BuildInMethod extends MethodStructure {
	public enum BuildInMethodName {
		// For Array
		ArraySize,

		// For String
		StringLength,
		StringOrd,
		ParseInt,
		Substring,

		// For Global
		GetInt,
		GetString,
		Print,
		Println,
		StringConcatenate,
		StringEqual,
		StringNotEqual,
		StringGreater,
		StringGreaterEqual,
		StringLess,
		StringLessEqual,
		ToString;
	}

	protected final BuildInMethodName name;

	protected BuildInMethod(int methodUID, int paraCount, BuildInMethodName name) {
		super(methodUID, paraCount);
		this.name = name;
	}

	// For IRPrinter

	@Override
	public void printIR(IRPrinter printer) {
		super.printIR(printer);
		printer.println("\tBuild In Method: " + name);
	}

	// For NaiveMips

	@Override
	void printNames(MipsFile file) {
		file.append(MipsComment.make("Build in method: " + name));
	}

	@Override
	public abstract void methodBodyNaiveMips(MipsFile file, GlobalConstant globalConstant);

	// For AdvancedMips

	@Override
	protected void methodBodyAdvancedMips(MipsFile file, GlobalConstant globalConstant) {
		methodBodyNaiveMips(file, globalConstant);
	}
}
