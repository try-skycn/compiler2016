package CtyCompiler2016.IR.MethodStructure;

import CtyCompiler2016.IR.ControlFlow.Label;
import CtyCompiler2016.IR.GlobalConstant;
import CtyCompiler2016.IR.RegAlloc.Graph.InterferenceGraph;
import CtyCompiler2016.IR.RegAlloc.Graph.LiveSet;
import CtyCompiler2016.IR.RegAlloc.Profile.MethodProfile;
import CtyCompiler2016.IR.RegAlloc.Profile.MipsRegProfile;
import CtyCompiler2016.IR.RegAlloc.Profile.RegisterProfile;
import CtyCompiler2016.IR.Register.VirtualRegister.LocalRegister;
import CtyCompiler2016.IR.Register.VirtualRegister.ParaRegister;
import CtyCompiler2016.IR.Register.VirtualRegister.StaticRegister;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;
import CtyCompiler2016.Mips.MipsComment;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsDest.MipsBinary.MipsAddition;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsMove;
import CtyCompiler2016.Mips.MipsOperand.MipsImm;
import CtyCompiler2016.Mips.MipsReg.MipsFp;
import CtyCompiler2016.Mips.MipsReg.MipsSp;
import CtyCompiler2016.Printer.IRPrinter;

import java.util.*;

/**
 * Created by SkytimChen on 4/22/16.
 */
public class ProgramMethod extends MethodStructure {
	private final List<Label> labelList;
	private final List<ParaRegister> paraList;
	private final List<LocalRegister> localList;
	private final List<StaticRegister> staticList = new ArrayList<>();

	public ProgramMethod(int methodUID, Set<String> nameSet, List<ParaRegister> paraList, List<LocalRegister> localList, Set<StaticRegister> staticSet, List<Label> labelList) {
		super(methodUID, paraList.size());
		this.nameSet = nameSet;
		this.paraList = paraList;
		this.localList = localList;
		this.staticList.addAll(staticSet);
		this.labelList = labelList;
	}

	// For MethodNames

	private final Set<String> nameSet;

	// Getter

	public int getParaCount() {
		return paraList.size();
	}

	public List<ParaRegister> getParaList() {
		return paraList;
	}

	public List<LocalRegister> getLocalList() {
		return localList;
	}

	public List<StaticRegister> getStaticList() {
		return staticList;
	}

	// For IRPrinter

	@Override
	public void printIR(IRPrinter printer) {
		super.printIR(printer);
		for (String name : nameSet) {
			printer.println("Method name: " + name);
		}
		printer.println(localList.size() + " local registers");
		for (Label label : labelList) {
			printer.printLabel(label);
			label.getBlock().printIR(printer);
		}
	}

	// For NaiveMips

	@Override
	void printNames(MipsFile file) {
		for (String name : nameSet) {
			file.append(MipsComment.make("Method name: " + name));
		}
	}

	@Override
	public void methodBodyNaiveMips(MipsFile file, GlobalConstant globalConstant) {
		int localSize = localList.size();

		// For Load
		file.append(new MipsMove(MipsFp.instance, MipsSp.instance));
		file.append(new MipsAddition(MipsSp.instance, MipsSp.instance, new MipsImm(-4 * localSize)));

		ListIterator<Label> iter = labelList.listIterator();
		ListIterator<Label> next = labelList.listIterator();
		next.next();
		while (iter.hasNext()) {
			Label label = iter.next();
			label.printNaiveMips(file, next.hasNext() ? next.next() : null);
		}

		// For Back
		file.append(new MipsAddition(MipsSp.instance, MipsSp.instance, new MipsImm(4 * localSize)));
	}

	// For RegAlloc

	public List<VirtualRegister> getRegisterList() {
		List<VirtualRegister> nodes = new ArrayList<>();
		nodes.addAll(paraList);
		nodes.addAll(localList);
		nodes.addAll(staticList);
		return nodes;
	}

	public void awake(MethodProfile profile) {
		labelList.forEach(label -> label.getBlock().awake(profile));
	}

	public Map<VirtualRegister, Integer> getColoring(RegisterProfile registerProfile) {
		InterferenceGraph<VirtualRegister> graph = new InterferenceGraph<>();
		registerProfile.getColoringList().forEach(graph::makeKey);
		List<LiveSet<VirtualRegister>> liveSetList = new ArrayList<>();
		labelList.forEach(label -> liveSetList.add(label.getBlock().makeInterferenceGraph(graph)));
		registerProfile.getPreDefinedList().forEach(liveSetList.get(0)::kill);
		registerProfile.getGlobalOccupiedList().forEach(graph::addEdgeAll);
		return graph.coloring(MipsRegProfile.totalRegisters());
	}

	// For AdvancedMips

	@Override
	protected void methodBodyAdvancedMips(MipsFile file, GlobalConstant globalConstant) {
		MethodProfile profile = new MethodProfile(globalConstant, this);

		// For Load
		profile.pushStack(file);

		ListIterator<Label> iter = labelList.listIterator();
		ListIterator<Label> next = labelList.listIterator();
		next.next();
		while (iter.hasNext()) {
			Label label = iter.next();
			label.printAdvancedMips(file, profile, next.hasNext() ? next.next() : null);
		}

		// For Back
		profile.popStack(file);
	}
}
