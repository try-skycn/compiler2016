package CtyCompiler2016.IR.MethodStructure.BuildIn;

import CtyCompiler2016.AST.Constituent.Method.Method;
import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;
import CtyCompiler2016.IR.GlobalConstant;
import CtyCompiler2016.IR.MethodStructure.BuildInMethod;
import CtyCompiler2016.Mips.MipsAddr.MipsLabel;
import CtyCompiler2016.Mips.MipsAddr.MipsRegAddr;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsBranchOnCondition.MipsBranchOnEqualZero;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsJump;
import CtyCompiler2016.Mips.MipsInst.MipsDest.MipsBinary.*;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad.MipsLoadMem;
import CtyCompiler2016.Mips.MipsOperand.MipsImm;
import CtyCompiler2016.Mips.MipsReg.MipsArgReg;
import CtyCompiler2016.Mips.MipsReg.MipsReg;
import CtyCompiler2016.Mips.MipsReg.MipsVReg;

/**
 * Created by SkytimChen on 4/26/16.
 */
public class BuildInStringCompare extends BuildInMethod {
	private BuildInStringCompare(int methodID, int paraCount, BuildInMethodName name) {
		super(methodID, paraCount, name);
	}

	public static BuildInStringCompare make(Method method, BuildInMethodName name) {
		return new BuildInStringCompare(method.uID, method.getParaCount(), name);
	}

	// For NaiveMips

	@Override
	public void methodBodyNaiveMips(MipsFile file, GlobalConstant globalConstant) {
		MipsLabel begin = MipsLabel.name(methodUID, "Begin");
		MipsLabel end = MipsLabel.name(methodUID, "End");

		MipsReg a0 = MipsArgReg.get(0);
		MipsReg a1 = MipsArgReg.get(1);
		MipsReg v0 = MipsVReg.get(0);
		MipsReg v1 = MipsVReg.get(1);

		file.append(begin);
		file.append(MipsLoadMem.make(v0, new MipsRegAddr(a0), 1));
		file.append(MipsLoadMem.make(v1, new MipsRegAddr(a1), 1));
		file.append(new MipsBranchOnEqualZero(v0, end));
		file.append(new MipsBranchOnEqualZero(v1, end));
		file.append(new MipsAddition(a0, a0, new MipsImm(1)));
		file.append(new MipsAddition(a1, a1, new MipsImm(1)));
		file.append(new MipsJump(begin));
		file.append(end);
		switch (name) {
			case StringLess:
				file.append(new MipsSetLessThan(v0, v0, v1));
				break;
			case StringLessEqual:
				file.append(new MipsSetLessThanEqual(v0, v0, v1));
				break;
			case StringGreater:
				file.append(new MipsSetGreaterThan(v0, v0, v1));
				break;
			case StringGreaterEqual:
				file.append(new MipsSetGreaterThanEqual(v0, v0, v1));
				break;
			case StringEqual:
				file.append(new MipsSetEqual(v0, v0, v1));
				break;
			case StringNotEqual:
				file.append(new MipsSetNotEqual(v0, v0, v1));
				break;
			default:
				CompilerError.errorHandle(ErrorType.UnknownError);
		}
	}
}
