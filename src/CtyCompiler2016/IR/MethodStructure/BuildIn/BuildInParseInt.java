package CtyCompiler2016.IR.MethodStructure.BuildIn;

import CtyCompiler2016.AST.Constituent.Method.Method;
import CtyCompiler2016.IR.GlobalConstant;
import CtyCompiler2016.IR.MethodStructure.BuildInMethod;
import CtyCompiler2016.Mips.MipsAddr.MipsLabel;
import CtyCompiler2016.Mips.MipsAddr.MipsRegAddr;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsBranchOnCondition.MipsBranchOnEqualZero;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsBranchOnCondition.MipsBranchOnLess;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsBranchOnCondition.MipsBranchonGreater;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsJump;
import CtyCompiler2016.Mips.MipsInst.MipsDest.MipsBinary.MipsAddition;
import CtyCompiler2016.Mips.MipsInst.MipsDest.MipsBinary.MipsMultiply;
import CtyCompiler2016.Mips.MipsInst.MipsDest.MipsBinary.MipsSetEqual;
import CtyCompiler2016.Mips.MipsInst.MipsDest.MipsUnary.MipsNegateValue;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad.MipsLoadImmediate;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad.MipsLoadMem;
import CtyCompiler2016.Mips.MipsOperand.MipsImm;
import CtyCompiler2016.Mips.MipsReg.*;

/**
 * Created by SkytimChen on 4/25/16.
 */
public class BuildInParseInt extends BuildInMethod {
	private BuildInParseInt(int methodID, int paraCount) {
		super(methodID, paraCount, BuildInMethodName.ParseInt);
	}

	public static BuildInParseInt make(Method method) {
		return new BuildInParseInt(method.uID, method.getParaCount());
	}

	// For NaiveMips

	@Override
	public void methodBodyNaiveMips(MipsFile file, GlobalConstant globalConstant) {
		MipsLabel cond = MipsLabel.name(methodUID, "Cond");
		MipsLabel notNeg = MipsLabel.name(methodUID, "NotNeg");
		MipsLabel next = MipsLabel.name(methodUID, "Next");

		MipsReg a0 = MipsArgReg.get(0);
		MipsReg fp = MipsFp.instance;
		MipsReg v0 = MipsVReg.get(0);
		MipsReg v1 = MipsVReg.get(1);

		file.append(new MipsLoadImmediate(v0, new MipsImm(0)));
		file.append(MipsLoadMem.make(fp, new MipsRegAddr(a0), 1));
		file.append(new MipsSetEqual(v1, fp, new MipsImm((int)'-')));
		file.append(new MipsAddition(a0, a0, v1));

		file.append(cond);
		file.append(MipsLoadMem.make(fp, new MipsRegAddr(a0), 1));
		file.append(new MipsBranchOnLess(fp, new MipsImm((int)'0'), next));
		file.append(new MipsBranchonGreater(fp, new MipsImm((int)'9'), next));

		file.append(new MipsAddition(fp, fp, new MipsImm(- (int)'0')));
		file.append(new MipsBranchOnEqualZero(v1, notNeg));
		file.append(new MipsNegateValue(fp, fp));
		file.append(notNeg);
		file.append(new MipsMultiply(v0, v0, new MipsImm(10)));
		file.append(new MipsAddition(v0, v0, fp));
		file.append(new MipsAddition(a0, a0, new MipsImm(1)));
		file.append(new MipsJump(cond));

		file.append(next);
	}
}
