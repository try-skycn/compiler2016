package CtyCompiler2016.IR.MethodStructure.BuildIn;

import CtyCompiler2016.AST.Constituent.Method.Method;
import CtyCompiler2016.IR.GlobalConstant;
import CtyCompiler2016.IR.MethodStructure.BuildInMethod;
import CtyCompiler2016.Mips.MipsAddr.MipsLabel;
import CtyCompiler2016.Mips.MipsAddr.MipsRegAddr;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsBranchOnCondition.MipsBranchOnEqual;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsJump;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsSyscall;
import CtyCompiler2016.Mips.MipsInst.MipsDest.MipsBinary.MipsAddition;
import CtyCompiler2016.Mips.MipsInst.MipsDest.MipsBinary.MipsSubtract;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad.MipsLoadByte;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad.MipsLoadImmediate;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsMove;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsStore.MipsStoreByte;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsStore.MipsStoreWord;
import CtyCompiler2016.Mips.MipsOperand.MipsImm;
import CtyCompiler2016.Mips.MipsReg.MipsArgReg;
import CtyCompiler2016.Mips.MipsReg.MipsReg;
import CtyCompiler2016.Mips.MipsReg.MipsRegZero;
import CtyCompiler2016.Mips.MipsReg.MipsVReg;
import CtyCompiler2016.Tool.SyscallID;

/**
 * Created by SkytimChen on 4/27/16.
 */
public class BuildInSubstring extends BuildInMethod {
	private BuildInSubstring(int methodID, int paraCount) {
		super(methodID, paraCount, BuildInMethodName.Substring);
	}

	public static BuildInSubstring make(Method method) {
		return new BuildInSubstring(method.uID, method.getParaCount());
	}

	// For NaiveMips

	@Override
	public void methodBodyNaiveMips(MipsFile file, GlobalConstant globalConstant) {
		MipsLabel loop = MipsLabel.name(methodUID, "Loop");
		MipsLabel end = MipsLabel.name(methodUID, "End");

		MipsReg v0 = MipsVReg.get(0);
		MipsReg v1 = MipsVReg.get(1);
		MipsReg a0 = MipsArgReg.get(0);
		MipsReg a1 = MipsArgReg.get(1);
		MipsReg a2 = MipsArgReg.get(2);

		file.append(new MipsAddition(a2, a2, new MipsImm(1)));
		file.append(new MipsAddition(a1, a0, a1));
		file.append(new MipsAddition(a2, a0, a2));
		file.append(new MipsSubtract(v1, a2, a1));
		file.append(new MipsAddition(a0, v1, new MipsImm(5)));
		file.append(new MipsLoadImmediate(v0, new MipsImm(SyscallID.Malloc.toInt())));
		file.append(new MipsSyscall());
		file.append(new MipsStoreWord(v1, new MipsRegAddr(v0)));
		file.append(new MipsAddition(v0, v0, new MipsImm(4)));
		file.append(new MipsMove(a0, v0));

		file.append(loop);
		file.append(new MipsBranchOnEqual(a1, a2, end));
		file.append(new MipsLoadByte(v1, new MipsRegAddr(a1)));
		file.append(new MipsStoreByte(v1, new MipsRegAddr(a0)));
		file.append(new MipsAddition(a0, a0, new MipsImm(1)));
		file.append(new MipsAddition(a1, a1, new MipsImm(1)));
		file.append(new MipsJump(loop));

		file.append(end);
		file.append(new MipsStoreByte(MipsRegZero.instance, new MipsRegAddr(a0)));
	}
}
