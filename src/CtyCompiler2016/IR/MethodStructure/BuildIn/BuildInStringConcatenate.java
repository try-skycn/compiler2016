package CtyCompiler2016.IR.MethodStructure.BuildIn;

import CtyCompiler2016.AST.Constituent.Method.Method;
import CtyCompiler2016.IR.GlobalConstant;
import CtyCompiler2016.IR.MethodStructure.BuildInMethod;
import CtyCompiler2016.Mips.MipsAddr.MipsLabel;
import CtyCompiler2016.Mips.MipsAddr.MipsRegAddr;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsBranchOnCondition.MipsBranchOnEqualZero;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsJump;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsSyscall;
import CtyCompiler2016.Mips.MipsInst.MipsDest.MipsBinary.MipsAddition;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad.MipsLoadByte;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad.MipsLoadImmediate;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad.MipsLoadWord;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsMove;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsStore.MipsStoreByte;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsStore.MipsStoreWord;
import CtyCompiler2016.Mips.MipsOperand.MipsImm;
import CtyCompiler2016.Mips.MipsReg.MipsArgReg;
import CtyCompiler2016.Mips.MipsReg.MipsReg;
import CtyCompiler2016.Mips.MipsReg.MipsRegZero;
import CtyCompiler2016.Mips.MipsReg.MipsVReg;
import CtyCompiler2016.Tool.SyscallID;

/**
 * Created by SkytimChen on 4/27/16.
 */
public class BuildInStringConcatenate extends BuildInMethod {
	private BuildInStringConcatenate(int methodID, int paraCount) {
		super(methodID, paraCount, BuildInMethodName.StringConcatenate);
	}

	public static BuildInStringConcatenate make(Method method) {
		return new BuildInStringConcatenate(method.uID, method.getParaCount());
	}

	// For NaiveMips

	@Override
	public void methodBodyNaiveMips(MipsFile file, GlobalConstant globalConstant) {
		MipsLabel copyFirst = MipsLabel.name(methodUID, "CopyFirst");
		MipsLabel copySecond = MipsLabel.name(methodUID, "CopySecond");
		MipsLabel end = MipsLabel.name(methodUID, "End");

		MipsReg a0 = MipsArgReg.get(0);
		MipsReg a1 = MipsArgReg.get(1);
		MipsReg a2 = MipsArgReg.get(2);
		MipsReg v0 = MipsVReg.get(0);
		MipsReg v1 = MipsVReg.get(1);

		file.append(new MipsMove(v1, a0));
		file.append(new MipsLoadWord(v0, new MipsRegAddr(a0, new MipsImm(-4))));
		file.append(new MipsLoadWord(a0, new MipsRegAddr(a1, new MipsImm(-4))));
		file.append(new MipsAddition(a0, a0, v0));
		file.append(new MipsMove(a2, a0));
		file.append(new MipsAddition(a0, a0, new MipsImm(5)));
		file.append(new MipsLoadImmediate(v0, new MipsImm(SyscallID.Malloc.toInt())));
		file.append(new MipsSyscall());
		file.append(new MipsStoreWord(a2, new MipsRegAddr(v0)));
		file.append(new MipsAddition(v0, v0, new MipsImm(4)));
		file.append(new MipsMove(a0, v0));

		file.append(copyFirst);
		file.append(new MipsLoadByte(a2, new MipsRegAddr(v1)));
		file.append(new MipsBranchOnEqualZero(a2, copySecond));
		file.append(new MipsStoreByte(a2, new MipsRegAddr(a0)));
		file.append(new MipsAddition(a0, a0, new MipsImm(1)));
		file.append(new MipsAddition(v1, v1, new MipsImm(1)));
		file.append(new MipsJump(copyFirst));

		file.append(copySecond);
		file.append(new MipsLoadByte(v1, new MipsRegAddr(a1)));
		file.append(new MipsBranchOnEqualZero(v1, end));
		file.append(new MipsStoreByte(v1, new MipsRegAddr(a0)));
		file.append(new MipsAddition(a0, a0, new MipsImm(1)));
		file.append(new MipsAddition(a1, a1, new MipsImm(1)));
		file.append(new MipsJump(copySecond));

		file.append(end);
		file.append(new MipsStoreByte(MipsRegZero.instance, new MipsRegAddr(a0)));
	}
}
