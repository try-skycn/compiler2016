package CtyCompiler2016.IR.MethodStructure.BuildIn;

import CtyCompiler2016.AST.Constituent.Method.Method;
import CtyCompiler2016.IR.GlobalConstant;
import CtyCompiler2016.IR.MethodStructure.BuildInMethod;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsSyscall;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad.MipsLoadImmediate;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoadAddress;
import CtyCompiler2016.Mips.MipsOperand.MipsImm;
import CtyCompiler2016.Mips.MipsReg.MipsArgReg;
import CtyCompiler2016.Mips.MipsReg.MipsReg;
import CtyCompiler2016.Mips.MipsReg.MipsVReg;
import CtyCompiler2016.Tool.SyscallID;

/**
 * Created by SkytimChen on 4/27/16.
 */
public class BuildInPrintln extends BuildInMethod {
	private BuildInPrintln(int methodID, int paraCount) {
		super(methodID, paraCount, BuildInMethodName.Println);
	}

	public static BuildInPrintln make(Method method) {
		return new BuildInPrintln(method.uID, method.getParaCount());
	}

	// For NaiveMips

	@Override
	public void methodBodyNaiveMips(MipsFile file, GlobalConstant globalConstant) {
		MipsReg a0 = MipsArgReg.get(0);
		MipsReg v0 = MipsVReg.get(0);

		file.append(new MipsLoadImmediate(v0, new MipsImm(SyscallID.PrintString.toInt())));
		file.append(new MipsSyscall());
		file.append(new MipsLoadAddress(a0, globalConstant.getNewline().staticLabel()));
		file.append(new MipsSyscall());
	}
}
