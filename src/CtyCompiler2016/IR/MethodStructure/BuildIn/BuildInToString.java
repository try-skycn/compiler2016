package CtyCompiler2016.IR.MethodStructure.BuildIn;

import CtyCompiler2016.AST.Constituent.Method.Method;
import CtyCompiler2016.IR.GlobalConstant;
import CtyCompiler2016.IR.MethodStructure.BuildInMethod;
import CtyCompiler2016.Mips.MipsAddr.MipsLabel;
import CtyCompiler2016.Mips.MipsAddr.MipsRegAddr;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsBranchOnCondition.MipsBranchOnEqualZero;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsBranchOnCondition.MipsBranchOnLessEqual;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsBranchOnCondition.MipsBranchOnNotEqualZero;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsJump;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsSyscall;
import CtyCompiler2016.Mips.MipsInst.MipsDest.MipsBinary.*;
import CtyCompiler2016.Mips.MipsInst.MipsDest.MipsUnary.MipsNegateValue;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad.MipsLoadImmediate;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad.MipsLoadMem;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsMove;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsStore.MipsStore;
import CtyCompiler2016.Mips.MipsOperand.MipsImm;
import CtyCompiler2016.Mips.MipsReg.*;
import CtyCompiler2016.Tool.SyscallID;

/**
 * Created by SkytimChen on 4/26/16.
 */
public class BuildInToString extends BuildInMethod {
	private BuildInToString(int methodID, int paraCount) {
		super(methodID, paraCount, BuildInMethodName.ToString);
	}

	public static BuildInToString make(Method method) {
		return new BuildInToString(method.uID, method.getParaCount());
	}

	// For NaiveMips

	@Override
	public void methodBodyNaiveMips(MipsFile file, GlobalConstant globalConstant) {
		MipsLabel toString = MipsLabel.name(methodUID, "ToString");
		MipsLabel notZero = MipsLabel.name(methodUID, "NotZero");
		MipsLabel cond = MipsLabel.name(methodUID, "Cond");
		MipsLabel toNeg = MipsLabel.name(methodUID, "ToNeg");
		MipsLabel loadChar = MipsLabel.name(methodUID, "LoadChar");
		MipsLabel endLoop = MipsLabel.name(methodUID, "EndLoop");
		MipsLabel copy = MipsLabel.name(methodUID, "Copy");
		MipsLabel copyCond = MipsLabel.name(methodUID, "CopyCond");
		MipsLabel endToString = MipsLabel.name(methodUID, "EndToString");

		MipsReg reg0 = MipsRegZero.instance;
		MipsReg v0 = MipsVReg.get(0);
		MipsReg v1 = MipsVReg.get(1);
		MipsReg a0 = MipsArgReg.get(0);
		MipsReg a1 = MipsArgReg.get(1);
		MipsReg fp = MipsFp.instance;

		file.append(toString);
		file.append(new MipsMove(a1, a0));
		file.append(new MipsLoadImmediate(a0, new MipsImm(16)));
		file.append(new MipsLoadImmediate(v0, new MipsImm(SyscallID.Malloc.toInt())));
		file.append(new MipsSyscall());
		file.append(new MipsBranchOnNotEqualZero(a1, notZero));

		file.append(MipsLoadMem.make(reg0, new MipsRegAddr(v0, new MipsImm(5)), 1));
		file.append(new MipsLoadImmediate(a0, new MipsImm((int) '0')));
		file.append(MipsStore.make(a0, new MipsRegAddr(v0, new MipsImm(4)), 1));
		file.append(new MipsLoadImmediate(a0, new MipsImm(1)));
		file.append(MipsStore.make(a0, new MipsRegAddr(v0), 1));
		file.append(new MipsJump(endToString));

		file.append(notZero);
		file.append(new MipsSetLessThan(v1, a1, new MipsImm(0)));
		file.append(new MipsAddition(fp, v0 , new MipsImm(15)));
		file.append(MipsStore.make(reg0, new MipsRegAddr(fp), 1));

		file.append(cond);
		file.append(new MipsBranchOnEqualZero(a1, endLoop));
		file.append(new MipsRemainder(a0, a1, new MipsImm(10)));
		file.append(new MipsDivide(a1, a1, new MipsImm(10)));
		file.append(new MipsBranchOnEqualZero(v1, loadChar));
		file.append(new MipsBranchOnLessEqual(a0, new MipsImm(0), toNeg));
		file.append(new MipsAddition(a0, a0, new MipsImm(-10)));

		file.append(toNeg);
		file.append(new MipsNegateValue(a0, a0));

		file.append(loadChar);
		file.append(new MipsAddition(a0, a0, new MipsImm((int) '0')));
		file.append(new MipsAddition(fp, fp, new MipsImm(-1)));
		file.append(MipsStore.make(a0, new MipsRegAddr(fp), 1));
		file.append(new MipsJump(cond));

		file.append(endLoop);
		file.append(new MipsBranchOnEqualZero(v1, copy));
		file.append(new MipsLoadImmediate(a0, new MipsImm((int) '-')));
		file.append(new MipsAddition(fp, fp, new MipsImm(-1)));
		file.append(MipsStore.make(a0, new MipsRegAddr(fp), 1));

		file.append(copy);
		file.append(new MipsSubtract(v1, v0, fp));
		file.append(new MipsAddition(v1, v1, new MipsImm(15)));
		file.append(MipsStore.make(v1, new MipsRegAddr(v0), 4));
		file.append(new MipsAddition(v1, v0, new MipsImm(4)));

		file.append(copyCond);
		file.append(MipsLoadMem.make(a0, new MipsRegAddr(fp), 1));
		file.append(MipsStore.make(a0, new MipsRegAddr(v1), 1));
		file.append(new MipsAddition(fp, fp, new MipsImm(1)));
		file.append(new MipsAddition(v1, v1, new MipsImm(1)));
		file.append(new MipsBranchOnNotEqualZero(a0, copyCond));

		file.append(endToString);
		file.append(new MipsAddition(v0, v0, new MipsImm(4)));
	}
}
