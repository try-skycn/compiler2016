package CtyCompiler2016.IR.MethodStructure;

import CtyCompiler2016.IR.GlobalConstant;
import CtyCompiler2016.Mips.MipsAddr.MipsLabel;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsJumpRegister;
import CtyCompiler2016.Mips.MipsInst.MipsCtrl.MipsSyscall;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad.MipsLoadImmediate;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsMove;
import CtyCompiler2016.Mips.MipsOperand.MipsImm;
import CtyCompiler2016.Mips.MipsReg.*;
import CtyCompiler2016.Printer.IRPrinter;
import CtyCompiler2016.Tool.SyscallID;

/**
 * Created by SkytimChen on 4/18/16.
 */
public abstract class MethodStructure {
	protected final int methodUID;
	protected final int paraCount;

	MethodStructure(int methodUID, int paraCount) {
		this.methodUID = methodUID;
		this.paraCount = paraCount;
	}

	public int getMethodUID() {
		return methodUID;
	}

	// For IRPrinter

	public void printIR(IRPrinter printer) {
		printer.printMethod(methodUID, paraCount);
	}

	// For NaiveMips

	protected abstract void methodBodyNaiveMips(MipsFile file, GlobalConstant globalConstant);
	abstract void printNames(MipsFile file);

	private void printHeadMips(MipsFile file) {
		printNames(file);
		file.append(MipsLabel.entry(getMethodUID()));
	}

	private void printTailMips(MipsFile file, boolean isEntry) {
		if (isEntry) {
			file.append(new MipsMove(MipsArgReg.get(0), MipsVReg.get(0)));
			file.append(new MipsLoadImmediate(MipsVReg.get(0), new MipsImm(SyscallID.RetExit.toInt())));
			file.append(new MipsSyscall());
		} else {
			file.append(new MipsJumpRegister(MipsRa.instance));
		}
	}

	public void printNaiveMips(MipsFile file, GlobalConstant globalConstant, boolean isEntry) {
		// For Load
		printHeadMips(file);

		methodBodyNaiveMips(file, globalConstant);

		// For Back
		printTailMips(file, isEntry);
	}

	// For AdvancedMips

	protected abstract void methodBodyAdvancedMips(MipsFile file, GlobalConstant globalConstant);

	public void printAdvancedMips(MipsFile file, GlobalConstant globalConstant, boolean isEntry) {
		// For Load
		printHeadMips(file);

		methodBodyAdvancedMips(file, globalConstant);

		// For Back
		printTailMips(file, isEntry);
	}
}
