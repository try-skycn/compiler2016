package CtyCompiler2016.IR.RegAlloc.Profile;

import CtyCompiler2016.IR.GlobalConstant;
import CtyCompiler2016.IR.MethodStructure.ProgramMethod;
import CtyCompiler2016.IR.Register.VirtualRegister.StaticRegister;
import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;
import CtyCompiler2016.Mips.MipsAddr.MipsRegAddr;
import CtyCompiler2016.Mips.MipsFile;
import CtyCompiler2016.Mips.MipsInst.MipsDest.MipsBinary.MipsAddition;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsLoad.MipsLoadWord;
import CtyCompiler2016.Mips.MipsInst.MipsMove.MipsStore.MipsStoreWord;
import CtyCompiler2016.Mips.MipsOperand.MipsImm;
import CtyCompiler2016.Mips.MipsReg.*;

import java.util.*;

public class MethodProfile {
	private final GlobalConstant globalConstant;
	private final ProgramMethod methodInfo;
	public final RegisterProfile registerProfile;
	public final MipsRegProfile mipsRegProfile;
	public final MemoryProfile memoryProfile;


	public MethodProfile(GlobalConstant globalConstant, ProgramMethod methodInfo) {
		this.globalConstant = globalConstant;
		this.methodInfo = methodInfo;
		this.savedArgCount = Integer.min(methodInfo.getParaCount(), MipsArgReg.argRegLimit);

		awake();

		registerProfile = new RegisterProfile(methodInfo.getRegisterList());
		mipsRegProfile = new MipsRegProfile(methodInfo.getColoring(registerProfile));
		memoryProfile = new MemoryProfile(
				mipsRegProfile.getUsedCalleeRegs().size(),
				mipsRegProfile.getMemSize(),
				mipsRegProfile.getUsedCallerRegs().size(),
				this.savedArgCount,
				Integer.max(this.maxCalleeArg - MipsArgReg.argRegLimit, 0)
		);
	}

	private void awake() {
		methodInfo.awake(this);
	}

	// For MipsSp

	private int maxCalleeArg = 0;
	private final int savedArgCount;

	public void setCalleeArg(int argc) {
		maxCalleeArg = Integer.max(argc, maxCalleeArg);
	}

	// For Calling

	private boolean callingState = false;

	public void beginCall() {
		callingState = true;
	}

	public void endCall() {
		callingState = false;
	}

	public boolean isCalling() {
		return callingState;
	}

	// For Saving

	public void syncStaticToMem(MipsFile file) {
		for (StaticRegister staticRegister : methodInfo.getStaticList()) {
			staticRegister.syncToMem(file, this);
		}
	}

	public void syncStaticToReg(MipsFile file) {
		for (StaticRegister staticRegister : methodInfo.getStaticList()) {
			staticRegister.syncToReg(file, this);
		}
	}

	public void syncAllToReg(MipsFile file) {
		for (VirtualRegister register : registerProfile.getUniverseList()) {
			register.syncToReg(file, this);
		}
	}

	public void saveCalleeRegs(MipsFile file) {
		file.append(new MipsStoreWord(MipsRa.instance, memoryProfile.getRaPos()));
		List<MipsReg> usedCalleeRegs = mipsRegProfile.getUsedCalleeRegs();
		for (int i = 0, n = usedCalleeRegs.size(); i < n; i++) {
			file.append(new MipsStoreWord(usedCalleeRegs.get(i), memoryProfile.getSRegPos(i)));
		}
	}

	public void loadCalleeRegs(MipsFile file) {
		file.append(new MipsLoadWord(MipsRa.instance, memoryProfile.getRaPos()));
		List<MipsReg> usedCalleeRegs = mipsRegProfile.getUsedCalleeRegs();
		for (int i = 0, n = usedCalleeRegs.size(); i < n; i++) {
			file.append(new MipsLoadWord(usedCalleeRegs.get(i), memoryProfile.getSRegPos(i)));
		}
	}

	public void saveCallerRegs(MipsFile file) {
		for (int i = 0; i < savedArgCount; i++) {
			file.append(new MipsStoreWord(MipsArgReg.get(i), memoryProfile.getArgPos(i)));
		}
		List<MipsReg> usedCallerRegs = mipsRegProfile.getUsedCallerRegs();
		for (int i = 0, n = usedCallerRegs.size(); i < n; i++) {
			file.append(new MipsStoreWord(usedCallerRegs.get(i), memoryProfile.getTRegPos(i)));
		}
	}

	public void loadCallerRegs(MipsFile file) {
		for (int i = 0; i < savedArgCount; i++) {
			file.append(new MipsLoadWord(MipsArgReg.get(i), memoryProfile.getArgPos(i)));
		}
		List<MipsReg> usedCallerRegs = mipsRegProfile.getUsedCallerRegs();
		for (int i = 0, n = usedCallerRegs.size(); i < n; i++) {
			file.append(new MipsLoadWord(usedCallerRegs.get(i), memoryProfile.getTRegPos(i)));
		}
	}

	public void pushStack(MipsFile file) {
		file.append(new MipsAddition(MipsSp.instance, MipsSp.instance, new MipsImm(4, -memoryProfile.getTotal())));
		saveCalleeRegs(file);
		syncAllToReg(file);
	}

	public void popStack(MipsFile file) {
		syncStaticToMem(file);
		loadCalleeRegs(file);
		file.append(new MipsAddition(MipsSp.instance, MipsSp.instance, new MipsImm(4, memoryProfile.getTotal())));
	}

	// For RegAlloc

	public MipsReg getAllocatedReg(VirtualRegister register) {
		return mipsRegProfile.getAllocatedReg(register);
	}

	public MipsRegAddr getAllocatedMem(VirtualRegister register) {
		Integer offset = mipsRegProfile.getAllocatedMem(register);
		if (offset != null) {
			return memoryProfile.getLocalMemPos(offset);
		} else {
			return null;
		}
	}
}
