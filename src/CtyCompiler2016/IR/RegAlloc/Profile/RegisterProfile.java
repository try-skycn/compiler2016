package CtyCompiler2016.IR.RegAlloc.Profile;

import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SkytimChen on 5/2/16.
 */
public class RegisterProfile {
	private final List<VirtualRegister> universeList;
	private final List<VirtualRegister> coloringList = new ArrayList<>();
	private final List<VirtualRegister> preDefinedList = new ArrayList<>();
	private final List<VirtualRegister> globalOccupiedList = new ArrayList<>();

	public RegisterProfile(List<VirtualRegister> universeList) {
		this.universeList = universeList;
		for (VirtualRegister register : universeList) {
			if (register.preDefined()) {
				preDefinedList.add(register);
			}
			if (register.needReg()) {
				coloringList.add(register);
			}
			if (register.globalOccupied()) {
				globalOccupiedList.add(register);
			}
		}
	}

	public List<VirtualRegister> getUniverseList() {
		return universeList;
	}

	public List<VirtualRegister> getColoringList() {
		return coloringList;
	}

	public List<VirtualRegister> getPreDefinedList() {
		return preDefinedList;
	}

	public List<VirtualRegister> getGlobalOccupiedList() {
		return globalOccupiedList;
	}
}
