package CtyCompiler2016.IR.RegAlloc.Profile;

import CtyCompiler2016.Mips.MipsAddr.MipsRegAddr;
import CtyCompiler2016.Mips.MipsOperand.MipsImm;
import CtyCompiler2016.Mips.MipsReg.MipsArgReg;
import CtyCompiler2016.Mips.MipsReg.MipsSp;

/**
 * Created by SkytimChen on 5/2/16.
 */
public class MemoryProfile {
	private final int total;
	private final int saveRaPos;
	private final int saveSRegPos;
	private final int localMemPos;
	private final int saveTRegPos;
	private final int saveArgPos;
	private final int paramArgPos;

	public MemoryProfile(int saveSRegCnt, int localMemCnt, int saveTRegCnt, int saveArgCnt, int paramArgCnt) {
		this.paramArgPos = 0;
		this.saveArgPos = this.paramArgPos + paramArgCnt;
		this.saveTRegPos = this.saveArgPos + saveArgCnt;
		this.localMemPos = this.saveTRegPos + saveTRegCnt;
		this.saveSRegPos = this.localMemPos + localMemCnt;
		this.saveRaPos = this.saveSRegPos + saveSRegCnt;
		this.total = this.saveRaPos + 1;
	}

	public MipsRegAddr getParamArgPos(int index) {
		index = index - MipsArgReg.argRegLimit;
		return new MipsRegAddr(MipsSp.instance, new MipsImm(4, paramArgPos + index));
	}

	public MipsRegAddr getArgPos(int index) {
		return new MipsRegAddr(MipsSp.instance, new MipsImm(4, saveArgPos + index));
	}

	public MipsRegAddr getTRegPos(int index) {
		return new MipsRegAddr(MipsSp.instance, new MipsImm(4, saveTRegPos + index));
	}

	public MipsRegAddr getLocalMemPos(int index) {
		return new MipsRegAddr(MipsSp.instance, new MipsImm(4, localMemPos + index));
	}

	public MipsRegAddr getSRegPos(int index) {
		return new MipsRegAddr(MipsSp.instance, new MipsImm(4, saveSRegPos + index));
	}

	public MipsRegAddr getRaPos() {
		return new MipsRegAddr(MipsSp.instance, new MipsImm(4, saveRaPos));
	}

	public MipsRegAddr getExceedArgPos(int index) {
		index = index - MipsArgReg.argRegLimit;
		return new MipsRegAddr(MipsSp.instance, new MipsImm(4, total + index));
	}

	public int getTotal() {
		return total;
	}
}
