package CtyCompiler2016.IR.RegAlloc.Profile;

import CtyCompiler2016.IR.Register.VirtualRegister.VirtualRegister;
import CtyCompiler2016.Mips.MipsReg.MipsReg;
import CtyCompiler2016.Mips.MipsReg.MipsSReg;
import CtyCompiler2016.Mips.MipsReg.MipsTReg;

import java.util.*;

public class MipsRegProfile {
	private final Map<VirtualRegister, MipsReg> regMap = new HashMap<>();
	private final Map<VirtualRegister, Integer> memMap = new HashMap<>();
	private final List<MipsReg> usedCallerRegs = new ArrayList<>();
	private final List<MipsReg> usedCalleeRegs = new ArrayList<>();

	MipsRegProfile(Map<VirtualRegister, Integer> colorMap) {
		regAllocation(colorMap);
	}

	private List<MipsReg> waitingList(int maxColor) {
		List<MipsReg> regList = new ArrayList<>();

		Stack<MipsReg> availableCalleeRegs = new Stack<>();
		MipsSReg.getRegs(availableCalleeRegs);
		MipsTReg.getRegs(availableCalleeRegs);

		Stack<MipsReg> availableCallerRegs = new Stack<>();

		for (int i = 0; i <= maxColor; i++) {
			if (!availableCallerRegs.isEmpty()) {
				MipsReg reg = availableCallerRegs.pop();
				regList.add(reg);
				usedCallerRegs.add(reg);
			} else if (!availableCalleeRegs.isEmpty()) {
				MipsReg reg = availableCalleeRegs.pop();
				regList.add(reg);
				usedCalleeRegs.add(reg);
			}
		}
		return regList;
	}

	private void regAllocation(Map<VirtualRegister, Integer> colorMap) {
		int maxColor = colorMap.isEmpty() ? -1 : Collections.max(colorMap.values());
		List<MipsReg> regList = waitingList(maxColor);
		for (Map.Entry<VirtualRegister, Integer> entry : colorMap.entrySet()) {
			Integer color = entry.getValue();
			if (color < regList.size()) {
				regMap.put(entry.getKey(), regList.get(color));
			} else if(entry.getKey().needMem()) {
				memMap.put(entry.getKey(), memMap.size());
			}
		}
	}

	MipsReg getAllocatedReg(VirtualRegister register) {
		return regMap.get(register);
	}

	Integer getAllocatedMem(VirtualRegister register) {
		return memMap.get(register);
	}

	List<MipsReg> getUsedCalleeRegs() {
		return usedCalleeRegs;
	}

	List<MipsReg> getUsedCallerRegs() {
		return usedCallerRegs;
	}

	int getMemSize() {
		return memMap.size();
	}

	public static int totalRegisters() {
		return MipsTReg.tRegLimit + MipsSReg.sRegLimit;
	}
}
