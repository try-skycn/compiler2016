package CtyCompiler2016.IR.RegAlloc.Graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class InterferenceGraph<K> {
	private final Map<K, Set<K>> graph = new HashMap<>();

	public void makeKey(K key) {
		graph.put(key, new HashSet<>());
	}

	public void addEdge(K first, K second) {
		if (graph.containsKey(first) && graph.containsKey(second) && !first.equals(second)) {
			graph.get(first).add(second);
			graph.get(second).add(first);
		}
	}

	public void addEdgeAll(K key) {
		graph.keySet().forEach(aim -> addEdge(key, aim));
	}

	// For Coloring

	public Map<K, Integer> coloring(int total) {
		ColoringGraph<K> coloringGraph = new ColoringGraph<>(graph);
		return coloringGraph.coloring(total);
	}
}
