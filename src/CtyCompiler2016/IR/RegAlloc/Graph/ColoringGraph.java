package CtyCompiler2016.IR.RegAlloc.Graph;

import java.util.*;

class ColoringGraph<K> {
	private class Vertex {
		final K key;
		final Set<K> aimSet;

		Vertex(K key, Set<K> aimSet) {
			this.key = key;
			this.aimSet = aimSet;
		}
	}

	private final Map<K, Set<K>> graph = new HashMap<>();

	ColoringGraph(Map<K, Set<K>> graph) {
		for (Map.Entry<K, Set<K>> entry : graph.entrySet()) {
			K key = entry.getKey();
			Set<K> aimSet = new HashSet<>();
			aimSet.addAll(entry.getValue());
			this.graph.put(key, aimSet);
		}
	}

	private Vertex removeKey(K key) {
		Set<K> aimSet = graph.get(key);
		for (K aim : aimSet) {
			graph.get(aim).remove(key);
		}
		graph.remove(key);
		return new Vertex(key, aimSet);
	}

	private void recoverKey(Vertex vertex) {
		for (K aim : vertex.aimSet) {
			graph.get(aim).add(vertex.key);
		}
		graph.put(vertex.key, vertex.aimSet);
	}

	private Integer coloring(Vertex vertex, Map<K, Integer> colorMap) {
		Set<Integer> neighborColorSet = new HashSet<>();
		for (K aim : vertex.aimSet) {
			Integer color = colorMap.get(aim);
			if (color != null) {
				neighborColorSet.add(color);
			}
		}
		for (int i = 0; ; i++) {
			if (!neighborColorSet.contains(i)) {
				return i;
			}
		}
	}

	Map<K, Integer> coloring(int total) {
		Random random = new Random();
		Map<K, Integer> colorMap = new HashMap<>();

		Stack<Vertex> vertexStack = new Stack<>();

		while (!graph.isEmpty()) {
			List<K> potentialList = new ArrayList<>();
			List<K> spillList = new ArrayList<>();
			for (Map.Entry<K, Set<K>> entry : graph.entrySet()) {
				if (entry.getValue().size() < total) {
					potentialList.add(entry.getKey());
				} else {
					spillList.add(entry.getKey());
				}
			}

			if (!potentialList.isEmpty()) {
				potentialList.forEach(key -> vertexStack.push(removeKey(key)));
			} else {
				K key = spillList.get(random.nextInt(spillList.size()));
				vertexStack.push(removeKey(key));
			}
		}

		while (!vertexStack.isEmpty()) {
			Vertex vertex = vertexStack.pop();
			colorMap.put(vertex.key, coloring(vertex, colorMap));
			recoverKey(vertex);
		}

		return colorMap;
	}
}
