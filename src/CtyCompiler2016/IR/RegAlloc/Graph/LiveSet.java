package CtyCompiler2016.IR.RegAlloc.Graph;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by SkytimChen on 5/2/16.
 */
public class LiveSet<K> {
	private final InterferenceGraph<K> graph;
	private final Set<K> liveNow = new HashSet<>();

	public LiveSet(InterferenceGraph<K> graph, Collection<K> liveOut) {
		this.graph = graph;
		liveNow.addAll(liveOut);
	}

	public void add(K key) {
		liveNow.add(key);
	}

	public void kill(K key) {
		liveNow.remove(key);
		liveNow.forEach(aim -> graph.addEdge(key, aim));
	}

	public Set<K> getLiveNow() {
		return liveNow;
	}
}
