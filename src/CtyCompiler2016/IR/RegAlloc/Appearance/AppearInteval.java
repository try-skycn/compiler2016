package CtyCompiler2016.IR.RegAlloc.Appearance;

/**
 * Created by SkytimChen on 4/30/16.
 */
public class AppearInteval {
	private final AppearSpot start;
	private AppearSpot end;

	public AppearInteval(AppearSpot start, AppearSpot end) {
		this.start = start;
		this.end = end;
	}

	public void setEnd(AppearSpot end) {
		this.end = end;
	}

	public AppearSpot getStart() {
		return start;
	}

	public AppearSpot getEnd() {
		return end;
	}

	public boolean collidesWith(AppearInteval inteval) {
		return start.beforeEqual(inteval.end) && inteval.start.beforeEqual(end);
	}

	public boolean contains(AppearSpot spot) {
		return start.beforeEqual(spot) && spot.beforeEqual(end);
	}
}
