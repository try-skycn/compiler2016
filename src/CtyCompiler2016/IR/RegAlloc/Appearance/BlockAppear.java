package CtyCompiler2016.IR.RegAlloc.Appearance;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SkytimChen on 4/30/16.
 */
public class BlockAppear {
	private final List<AppearInteval> intevalList = new ArrayList<>();
	private AppearInteval lastInteval = null;

	public BlockAppear() {
	}

	public void addWrite(int instIndex) {
		lastInteval = new AppearInteval(AppearSpot.cycleWrite(instIndex), AppearSpot.cycleRead(instIndex));
	}

	public void addRead(int instIndex) {
		lastInteval.setEnd(AppearSpot.cycleRead(instIndex));
	}
}
