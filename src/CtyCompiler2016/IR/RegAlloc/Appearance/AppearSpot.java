package CtyCompiler2016.IR.RegAlloc.Appearance;

/**
 * Created by SkytimChen on 4/30/16.
 */
public class AppearSpot {
	private final int instIndex;
	private final boolean isWrite;

	private AppearSpot(int instIndex, boolean isWrite) {
		this.instIndex = instIndex;
		this.isWrite = isWrite;
	}

	public static AppearSpot cycleRead(int instIndex) {
		return new AppearSpot(instIndex, false);
	}

	public static AppearSpot cycleWrite(int instIndex) {
		return new AppearSpot(instIndex, true);
	}

	private int modifiedIndex() {
		return instIndex + (isWrite ? 1 : 0);
	}

	public boolean beforeEqual(AppearSpot spot) {
		return modifiedIndex() <= spot.modifiedIndex();
	}
}
