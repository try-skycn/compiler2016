package CtyCompiler2016.Tool;

/**
 * Created by SkytimChen on 4/25/16.
 */
public interface StringTransform {
	static String withEscape(String str) {
		StringBuilder builder = new StringBuilder();
		builder.append('"');
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			switch (ch) {
				case '\n':
					builder.append("\\n");
					break;
				case '"':
					builder.append("\\\"");
					break;
				default:
					builder.append(ch);
			}
		}
		builder.append('"');
		return builder.toString();
	}

	static String removeEscape(String text) {
		StringBuilder builder = new StringBuilder();
		for (int i = 1; i < text.length() - 1; i++) {
			if (text.charAt(i) == '\\') {
				char ch = text.charAt(i + 1);
				switch (ch) {
					case 'n':
						builder.append('\n');
						break;
					default:
						builder.append(ch);
				}
				i++;
			} else {
				builder.append(text.charAt(i));
			}
		}
		return builder.toString();
	}

	static int getLength(String text) {
		return removeEscape(text).length();
	}
}
