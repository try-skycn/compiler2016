package CtyCompiler2016.Tool;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SkytimChen on 4/13/16.
 */
public class UIDManager<V> {
	private final List<V> list = new ArrayList<>();

	public UIDManager() {}

	public int add(V v) {
		list.add(v);
		return list.size() - 1;
	}

	public V get(int uID) {
		return list.get(uID);
	}

	public List<V> getList() {
		return list;
	}

	public int size() {
		return list.size();
	}
}
