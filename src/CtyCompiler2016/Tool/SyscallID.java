package CtyCompiler2016.Tool;

import CtyCompiler2016.CompilerError.CompilerError;
import CtyCompiler2016.CompilerError.ErrorType;

/**
 * Created by SkytimChen on 4/25/16.
 */
public enum SyscallID {
	PrintInt,
	PrintString,
	ReadInt,
	ReadString,
	Malloc,
	Exit,
	RetExit;

	public int toInt() {
		switch (this) {
			case PrintInt:
				return 1;
			case PrintString:
				return 4;
			case ReadInt:
				return 5;
			case ReadString:
				return 8;
			case Malloc:
				return 9;
			case Exit:
				return 10;
			case RetExit:
				return 17;
			default:
				CompilerError.errorHandle(ErrorType.UnknownError);
				return 0;
		}
	}
}
